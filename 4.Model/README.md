# 外壳制作说明

### 外壳
- 3D打印文件Sensor_NRF24L01_LCD.stl

![](Sensor_NRF24L01_LCD.png)

### 不干胶
- Sensor_NRF24L01采用BMD60012外壳，不干胶裁切尺寸35mmX25mm，R2mm圆角即可

![](Sensor_NRF24L01(35mmX25mm).png)

- Sensor_NRF24L01_LCD采用3D打印外壳，不干胶裁切尺寸34mmX34mm，R2mm圆角即可

![](Sensor_NRF24L01_LCD(34mmX34mm).png)

![](../5.Docs/2.image/Model.jpg)
