//============================================================================
//File    : st7567.c                                                         =
//Date    : 2015.03.05                                                       =
//Author  : zty                                                              =
//Diary   : 1、ST7567液晶驱动程序;                                           =
//          2、SCL/SDA通信速率不能太快;                                      =
//          3、I2C接口的通信速率不能大于400KHz;                              =
//============================================================================
#include "st7567.h"
#include "pixel_bmp.h"
#include "pixel_english_8x6.h"
#include "pixel_english_16x8.h"
#include "pixel_chinese_12x12.h"
#include "pixel_chinese_16x16.h"
#include "pixel_chinese_24x24.h"
#include "pixel_chinese_32x32.h"

//=====================================================================
//= Private functions---自---定---义---函---数----------------------- =
//=====================================================================
void Delay_us(u16 us)
{//大概0.629us / 16M;
	  while(us--);
}

void Delay_ms(u16 ms)
{
	  while(ms--)Delay_us(1590);
}

//=====================================================================
//= Public functions------公---共---函---数-------------------------- =
//=====================================================================
//=====================================================================
//Function : LCD_Init_ST7567();                                       =
//Author   : zty                                                      =
//WriteData: 2015.03.07                                               =
//EditData : 2015.03.07                                               =
//Diary    : 1、12864液晶初始化;                                      =
//=====================================================================
void LCD_Init_ST7567()
{
	  ST7567_CSB_OUT;//IO初始化
		ST7567_RST_OUT;
		ST7567_RS_OUT;
		ST7567_SCL_OUT;
		ST7567_SDA_OUT;
		ST7567_BL_OUT;
		
		ST7567_RST_L;//硬件复位,低电平复位
	  Delay_us(1590);//1ms
	  ST7567_RST_H;
	  Delay_us(1590);//1ms 
		LCD_ST7567_WriteCmd(0xe2);//软件复位
		Delay_us(1590);//1ms 
		LCD_ST7567_WriteCmd(0x2c);//升压步骤1:打开内部升压
		Delay_us(1590);//1ms 
		LCD_ST7567_WriteCmd(0x2e);//升压步骤2:打开内部调整电路
		Delay_us(1590);//1ms 
		LCD_ST7567_WriteCmd(0x2f);//升压步骤3:打开电压跟随器
		Delay_us(1590);//1ms 
		LCD_ST7567_WriteCmd(0x24);//粗调对比度,可设范围:0x20~0x27,值越大显示越浓
		LCD_ST7567_WriteCmd(0x81);//微调对比度
		LCD_ST7567_WriteCmd(0x22);//微调对比度值:可设范围:0x00~0x3f
		LCD_ST7567_WriteCmd(0xa2);//1/9偏压比(bias)
		LCD_ST7567_WriteCmd(0xc8);//行扫描顺序:从上到下
		LCD_ST7567_WriteCmd(0xa0);//列扫描顺序:从左到右
		LCD_ST7567_WriteCmd(0x40);//起始行:第一行开始
		LCD_ST7567_WriteCmd(0xaf);//打开显示
}

//=====================================================================
//Function : LCD_Addr_xy();                                           =
//Author   : zty                                                      =
//WriteData: 2015.05.04                                               =
//EditData : 2015.05.04                                               =
//Diary    : 1、设置液晶显示坐标;                                     =
//           2、参数: page: 0~7 column: 0~127;                        =
//=====================================================================
void LCD_Addr_xy(unsigned char page, unsigned char column)
{
		LCD_ST7567_WriteCmd(0xb0+page);//设置页地址
		LCD_ST7567_WriteCmd(((column>>4)&0x0f) | 0x10);//设置列地址高4位
		LCD_ST7567_WriteCmd(column&0x0f);//设置列地址低4位
}

//=====================================================================
//Function : LCD_Clear();                                             =
//Author   : zty                                                      =
//WriteData: 2015.03.07                                               =
//EditData : 2015.03.07                                               =
//Diary    : 1、液晶清屏;                                             =
//=====================================================================
void LCD_Clear(unsigned char xx)
{
    unsigned char i,j;
    for(j=0;j<8;j++)//8个页
	  {
		    LCD_Addr_xy(j, 0);
		    for(i=0;i<128;i++)//128列
		    {
			    LCD_ST7567_WriteDat(xx);
		    }
	  }
}

//=====================================================================
//Function : LCD_DrawBmp();                                           =
//Author   : zty                                                      =
//WriteData: 2015.03.07                                               =
//EditData : 2015.03.07                                               =
//Diary    : 1、液晶显示一幅图片;                                     =
//=====================================================================
void LCD_DrawBmp(unsigned char *dp)
{
	  unsigned char i,j;
     
	  for(j=0;j<8;j++)
	  {
		    LCD_Addr_xy(j, 0);
		    for(i=0;i<128;i++)
		    {
			    LCD_ST7567_WriteDat(*dp);
			    dp++;
		    }
	  }
} 

//=====================================================================
//Function : LCD_write_chinese32x32();                                =
//Author   : zty                                                      =
//WriteData: 2015.05.05                                               =
//EditData : 2015.05.05                                               =
//Diary    : 1、显示32x32点汉字;                                      =
//           2、参数: page: 1~8 column: 1~128;                        =
//           3、---------c: 字符在汉字数组中的位置;                   = 
//=====================================================================
void LCD_write_chinese32x32(unsigned char page, unsigned char column, unsigned char c)
{
    unsigned char i;
		
		LCD_Addr_xy(page, column);
		for(i=0; i<32; i++)
		{
				LCD_ST7567_WriteDat(chinese32x32[c][i]);
		}
		
		LCD_Addr_xy(page+1, column);
		for(i=32; i<64; i++)
		{
				LCD_ST7567_WriteDat(chinese32x32[c][i]);
		}

    LCD_Addr_xy(page+2, column);
		for(i=64; i<96; i++)
		{
				LCD_ST7567_WriteDat(chinese32x32[c][i]);
		}
		
    LCD_Addr_xy(page+3, column);
		for(i=96; i<128; i++)
		{
				LCD_ST7567_WriteDat(chinese32x32[c][i]);
		}
}

//=====================================================================
//Function : LCD_write_chinese24x24();                                =
//Author   : zty                                                      =
//WriteData: 2015.05.05                                               =
//EditData : 2015.05.05                                               =
//Diary    : 1、显示24x24点汉字;                                      =
//           2、参数: page: 1~8 column: 1~128;                        =
//           3、---------c: 字符在汉字数组中的位置;                   = 
//=====================================================================
void LCD_write_chinese24x24(unsigned char page, unsigned char column, unsigned char c)
{
    unsigned char i;
		
		LCD_Addr_xy(page, column);
		for(i=0; i<24; i++)
		{
				LCD_ST7567_WriteDat(chinese24x24[c][i]);
		}
		
		LCD_Addr_xy(page+1, column);
		for(i=24; i<48; i++)
		{
				LCD_ST7567_WriteDat(chinese24x24[c][i]);
		}

    LCD_Addr_xy(page+2, column);
		for(i=48; i<72; i++)
		{
				LCD_ST7567_WriteDat(chinese24x24[c][i]);
		}
}

//=====================================================================
//Function : LCD_write_chinese16x16();                                =
//Author   : zty                                                      =
//WriteData: 2015.05.04                                               =
//EditData : 2015.05.04                                               =
//Diary    : 1、显示16x16点汉字;                                      =
//           2、参数: page: 1~8 column: 1~128;                        =
//           3、---------c: 字符在汉字数组中的位置;                   = 
//=====================================================================
void LCD_write_chinese16x16(unsigned char page, unsigned char column, unsigned char c)
{
    unsigned char i;
		
		LCD_Addr_xy(page, column);
		for(i=0; i<16; i++)
		{
				LCD_ST7567_WriteDat(chinese16x16[c][i]);
		}
		
		LCD_Addr_xy(page+1, column);
		for(i=16; i<32; i++)
		{
				LCD_ST7567_WriteDat(chinese16x16[c][i]);
		}
}

//=====================================================================
//Function : LCD_write_chinese12x12();                                =
//Author   : zty                                                      =
//WriteData: 2015.05.04                                               =
//EditData : 2015.05.04                                               =
//Diary    : 1、显示12x12点汉字;                                      =
//           2、参数: page: 1~8 column: 1~128;                        =
//           3、---------c: 字符在汉字数组中的位置;                   = 
//=====================================================================
void LCD_write_chinese12x12(unsigned char page, unsigned char column, unsigned char c)
{
    unsigned char i;
		
		LCD_Addr_xy(page, column);
		for(i=0; i<12; i++)
		{
				LCD_ST7567_WriteDat(chinese12x12[c][i]);
		}
		
		LCD_Addr_xy(page+1, column);
		for(i=12; i<24; i++)
		{
				LCD_ST7567_WriteDat(chinese12x12[c][i]);
		}
}

//=====================================================================
//Function : LCD_write_char16x8();                                    =
//Author   : zty                                                      =
//WriteData: 2015.05.06                                               =
//EditData : 2015.05.06                                               =
//Diary    : 1、显示16x8点字符;                                       =
//           2、参数: page: 1~8 column: 1~128;                        =
//           3、---------c: 字符在字符数组中的位置;                   = 
//=====================================================================
void LCD_write_char16x8(unsigned char page, unsigned char column, unsigned char c)
{
		unsigned char i;
		
		LCD_Addr_xy(page, column);
		for(i=0; i<8; i++)
		{
				LCD_ST7567_WriteDat(char16x8[c][i]);
		}
		
		LCD_Addr_xy(page+1, column);
		for(i=8; i<16; i++)
		{
				LCD_ST7567_WriteDat(char16x8[c][i]);
		}
}

//=====================================================================
//Function : LCD_write_char16x8_string();                             =
//Author   : zty                                                      =
//WriteData: 2015.05.06                                               =
//EditData : 2015.05.06                                               =
//Diary    : 1、显示16x8点字符串;                                     =
//           2、参数: page: 1~8 column: 1~128;                        =
//           3、--------*s: 要显示的字符串;                           = 
//=====================================================================
void LCD_write_char16x8_string(unsigned char page, unsigned char column, unsigned char *s)
{	
		unsigned char i, j;
		unsigned char *ss;
		ss = s;
		
		LCD_Addr_xy(page, column);
		while(*s) 
    {
			  j = *s - 32;
				for(i=0; i<8; i++)
		    {
				    LCD_ST7567_WriteDat(char16x8[j][i]);
		    }
        s++;
    }
		
		s = ss;
		LCD_Addr_xy(page+1, column);
		while(*s) 
    {
			  j = *s - 32;
				for(i=8; i<16; i++)
		    {
				    LCD_ST7567_WriteDat(char16x8[j][i]);
		    }
        s++;
    }
}

//=====================================================================
//Function : LCD_write_char8x6();                                     =
//Author   : zty                                                      =
//WriteData: 2015.05.06                                               =
//EditData : 2015.05.06                                               =
//Diary    : 1、显示8x6点字符;                                        =
//           2、参数: page: 1~8 column: 1~128;                        =
//           3、---------c: 字符在字符数组中的位置;                   = 
//=====================================================================
void LCD_write_char8x6(unsigned char c)
{
		unsigned char i;
		
		c -= 32;//把字符转换成字符数组指针
   
		for(i=0; i<6; i++)
		{
				LCD_ST7567_WriteDat(font6x8[c][i]);
		}
}

//=====================================================================
//Function : LCD_write_char8x6_string();                              =
//Author   : zty                                                      =
//WriteData: 2015.05.06                                               =
//EditData : 2015.05.06                                               =
//Diary    : 1、显示8x6点字符串;                                      =
//           2、参数: page: 1~8 column: 1~128;                        =
//           3、--------*s: 要显示的字符串;                           = 
//=====================================================================
void LCD_write_char8x6_string(unsigned char page, unsigned char column, unsigned char *s)
{
		LCD_Addr_xy(page, column);	
		
		while(*s) 
    {
        LCD_write_char8x6(*s);
        s++;
    }
}

//=====================================================================
//Function : LCD_Sleep();                                             =
//Author   : zty                                                      =
//WriteData: 2015.05.06                                               =
//EditData : 2015.05.06                                               =
//Diary    : 1、进入睡眠模式;                                         =
//=====================================================================
void LCD_Sleep(void)
{
    LCD_ST7567_WriteCmd(0xac);//静态图标:关闭
		LCD_ST7567_WriteCmd(0x00);//静态图标寄存器设置:关闭,此指令与上述指令一起完成静态图标关闭;
		LCD_ST7567_WriteCmd(0xae);//显示:关
		LCD_ST7567_WriteCmd(0xa5);//全屏显示:开
}

//=====================================================================
//Function : LCD_Wake();                                              =
//Author   : zty                                                      =
//WriteData: 2015.05.06                                               =
//EditData : 2015.05.06                                               =
//Diary    : 1、退出睡眠模式;                                         =
//=====================================================================
void LCD_Wake(void)
{
    LCD_ST7567_WriteCmd(0xa4);//全屏显示:关,进入正常模式
		LCD_ST7567_WriteCmd(0xad);//静态图标开启
		LCD_ST7567_WriteCmd(0x03);//静态图标寄存器设置:开启,此指令与上述指令一起完成静态图标开启;
		LCD_ST7567_WriteCmd(0xaf);//显示:开
}

//=====================================================================
//Function : LCD_ST7567_WriteCmd();                                   =
//Author   : zty                                                      =
//WriteData: 2015.03.07                                               =
//EditData : 2015.03.07                                               =
//Diary    : 1、写命令到液晶;                                         =
//=====================================================================
void LCD_ST7567_WriteCmd(unsigned char Comd)
{
	  unsigned char bit8;
		
	  ST7567_RS_L; //命令
	  ST7567_CSB_L;//片选
		
	  for(bit8=0;bit8<8;bit8++)
	  {
		  ST7567_SCL_L;
			Delay_us(1);
		  if((Comd & 0x80) == 0x80)ST7567_SDA_H;
		  else		ST7567_SDA_L;
		  ST7567_SCL_H;
			Delay_us(1);
		  Comd = (Comd << 1);
	  }
     
    ST7567_CSB_H;//片选失效
}

//=====================================================================
//Function : LCD_ST7567_WriteDat();                                   =
//Author   : zty                                                      =
//WriteData: 2015.03.07                                               =
//EditData : 2015.03.07                                               =
//Diary    : 1、写数据到液晶;                                         =
//=====================================================================
void LCD_ST7567_WriteDat(unsigned char Data)
{   
	  unsigned char bit8;
		
	  ST7567_RS_H; //数据
	  ST7567_CSB_L;//片选
		
	  for(bit8=0;bit8<8;bit8++)
	  {
		  ST7567_SCL_L;
			Delay_us(1);
		  if((Data & 0x80) == 0x80)ST7567_SDA_H;
		  else ST7567_SDA_L;
		  ST7567_SCL_H;
			Delay_us(1);
		  Data = (Data << 1);
		}
     
    ST7567_CSB_H;//片选失效
}

//读数据在串行模式下是无效的
//=====================================================================
//Function : Start_Read_Modify_Write();                               =
//Author   : zty                                                      =
//WriteData: 2015.05.06                                               =
//EditData : 2015.05.06                                               =
//Diary    : 1、开始"读取-修改-写入"模式;                             =
//=====================================================================
/*void Start_Read_Modify_Write(void)
{
    LCD_ST7567_WriteCmd(0xe0);  
}*/

//=====================================================================
//Function : End_Read_Modify_Write();                                 =
//Author   : zty                                                      =
//WriteData: 2015.05.06                                               =
//EditData : 2015.05.06                                               =
//Diary    : 1、结束"读取-修改-写入"模式;                             =
//=====================================================================
/*void End_Read_Modify_Write(void)
{
    LCD_ST7567_WriteCmd(0xee);  
}*/

//=====================================================================
//Function : LCD_Init_Display( );                                     =
//Author   : zty                                                      =
//WriteData: 2015.05.06                                               =
//EditData : 2015.05.06                                               =
//Diary    : 1、开机画面初始化;                                       =
//=====================================================================
void LCD_Init_Display(void)
{
	  LCD_Clear(0x00);//清屏,显示第一画面
		Delay_ms(1000);//延时
	  LCD_DrawBmp(graphic1);//开机logo
		Delay_ms(1500);//延时
		
		LCD_Clear(0x00);//清屏,显示第二画面
		LCD_write_chinese24x24(0,  4, 0);//"物联网项目"
		LCD_write_chinese24x24(0, 28, 1);
		LCD_write_chinese24x24(0, 52, 2);
		LCD_write_chinese24x24(0, 76, 3);
		LCD_write_chinese24x24(0, 100,4);
		Delay_ms(200);//延时
		LCD_write_chinese12x12(4, 34, 0);//"低成本设计与改造工作室"
		LCD_write_chinese12x12(4, 46, 1);
		LCD_write_chinese12x12(4, 58, 2);
		LCD_write_chinese12x12(4, 70, 3);
		LCD_write_chinese12x12(4, 82, 4);
		Delay_ms(200);//延时
		LCD_write_chinese12x12(6, 28, 5);
		LCD_write_chinese12x12(6, 40, 6);
		LCD_write_chinese12x12(6, 52, 7);
		LCD_write_chinese12x12(6, 64, 8);
		LCD_write_chinese12x12(6, 76, 9);
		LCD_write_chinese12x12(6, 88,10);
		Delay_ms(600);//延时
		
		LCD_Clear(0x00);//清屏,显示第三画面
		Delay_ms(100);//延时
		LCD_write_chinese16x16(1, 32, 9);//"智能物联"
		LCD_write_chinese16x16(1, 48,10);
		LCD_write_chinese16x16(1, 64, 0);
		LCD_write_chinese16x16(1, 80, 1);
		LCD_write_char8x6_string(4, 31, "todo Studio");//"todo Studio"
		LCD_write_chinese16x16(5, 16, 3);//"与你一起分享"
		LCD_write_chinese16x16(5, 32, 4);
		LCD_write_chinese16x16(5, 48, 5);
		LCD_write_chinese16x16(5, 64, 6);
		LCD_write_chinese16x16(5, 80, 7);
		LCD_write_chinese16x16(5, 96, 8);
		Delay_ms(900);//延时
		
		LCD_Clear(0x00);//清屏,显示工作画面
		Delay_ms(100);//延时
		LCD_write_chinese16x16(0, 16, 11);//"温度终端数据"
		LCD_write_chinese16x16(0, 32, 12);
		LCD_write_chinese16x16(0, 48, 13);
		LCD_write_chinese16x16(0, 64, 14);
		LCD_write_chinese16x16(0, 80, 15);
		LCD_write_chinese16x16(0, 96, 16);
		
		LCD_write_char16x8_string(2, 0, "T1:--.-~T4:--.-~");
		LCD_write_char16x8_string(4, 0, "T2:--.-~T5:--.-~");
		LCD_write_char16x8_string(6, 0, "T3:--.-~T6:--.-~");
		
}

/*调试显示的示例,不能删！
		LCD_Clear(0x00);//清屏
		LCD_write_chinese24x24(0, 0, 0);//显示24x24:"物联网"
		LCD_write_chinese24x24(0, 24, 1);
		LCD_write_chinese24x24(0, 48, 2);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		LCD_write_chinese16x16(0, 72, 0);//显示16x16:"物联网"
		LCD_write_chinese16x16(0, 88, 1);
		LCD_write_chinese16x16(0, 104, 2);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		LCD_write_english_string(2, 72, "Aoptek 06");//显示8x6点字符串;
		LCD_write_chinese24x24(3, 0, 0);//显示24x24:"物联网"
		LCD_write_chinese24x24(3, 24, 1);
		LCD_write_chinese24x24(3, 48, 2);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		
		LCD_Clear(0x00);//清屏
		LCD_write_chinese32x32(0, 0, 0);//显示32x32:"物联网"
		LCD_write_chinese32x32(0, 32, 1);
		LCD_write_chinese32x32(0, 64, 2);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		LCD_write_chinese16x16(0, 96, 0);//显示16x16:"物联网"
		LCD_write_chinese16x16(0, 112, 1);
		LCD_write_chinese16x16(2, 96, 2);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		LCD_write_chinese32x32(4, 0, 0);//显示32x32:"物联网"
		LCD_write_chinese32x32(4, 32, 1);
		LCD_write_chinese32x32(4, 64, 2);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
   
		LCD_DrawBmp(graphic1);//显示图片
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
		Delay(63600);Delay(63600);Delay(63600);Delay(63600);Delay(63600);
*/