//============================================================================
//Project : Sensor Nodes Nano24L                                             =
//Version : V1.0                                                             =
//Date    : 2015.03.05                                                       =
//Author  : zty                                                              =
//Company : todo studio                                                      =
//                                                                           =
//Chip type           : STM8L151K4T6                                         =
//Clock frequency     : SYS=16/1=16MHz ,RTC=32.768K/Hz                       =
//Diary               :                                                      =
//============================================================================

//=====================================================================
//= Includes------------头---文---件---包---含----------------------- =
//=====================================================================
#include "stm8l15x.h"
#include "stm8l15x_clk.h"
#include "stm8l15x_rtc.h"
#include "stm8l15x_gpio.h"
#include "nrf24l01.h"
#include "st7567.h"

//=====================================================================
//= Private variables------变---量---定---义------------------------- =
//=====================================================================
bool ButtonPressed = FALSE;

u16 Temp01 = 0;//无线接收到的温度值
u16 Temp02 = 0;
u16 Temp03 = 0;
u16 Temp04 = 0;
u16 Temp05 = 0;
u16 Temp06 = 0;

u16 Connect_Timer0 = 0;//通道0无线接收检查计时器
u16 Connect_Timer1 = 0;//通道1无线接收检查计时器
u16 Connect_Timer2 = 0;//通道2无线接收检查计时器
u16 Connect_Timer3 = 0;//通道3无线接收检查计时器
u16 Connect_Timer4 = 0;//通道4无线接收检查计时器
u16 Connect_Timer5 = 0;//通道5无线接收检查计时器

u8 rxData[7] = {0x00};//NRF24L01接收区

//=====================================================================
//= function prototypes---函---数---声---明-------------------------- =
//=====================================================================
void Delay(u16 nCount);
static void CLK_Config(void);
static void RTC_Config(void);
static void TIM4_Config(void);
static void IO_Config(void);

//=====================================================================
//= -------------------------主---函---数---------------------------- =
//=====================================================================
main()
{
	  //======================变量定义=========================//
	  u8 NRF24L01_status = 0;//NRF24L01状态变量
		
    //=====================初始化配置========================//
    CLK_Config();//系统时钟源配置
		Delay(1600);//1ms
		RTC_Config();//RTC配置
		Delay(1600);//1ms
		TIM4_Config();//TIM4配置
		Delay(1600);//1ms
	  IO_Config();//IO口初始化
		Delay(1600);//1ms
		Init_NRF24L01();//初始化NRF24L01
		Delay(1600);//1ms
		LCD_Init_ST7567();//初始化ST7567液晶
		Delay(1600);//1ms
		
		SwitchToRxMode( );//24L01切换到接收模式 
    Delay(16000);//10ms
		ST7567_BL_H;//打开背光
		Delay(16000);//10ms
		LCD_Init_Display();//开机画面初始化;
		Delay(16000);//10ms
		
		enableInterrupts();//开启全局中断
	
while(1)
{
    NRF24L01_status = NRF24L01_ReceivePacket(rxData);//监视接收状态
    
    if(NRF24L01_status)
    {   
        NRF24L01_status &= 0x0E;//取接收数据通道号!!!
        NRF24L01_status = NRF24L01_status >> 1;
        
        if (NRF24L01_status == 0)//rxData[0] == 0x01;//------------接收通道0处理
        {  
            Connect_Timer0 = 0;//通道0无线接收检查计时器清零
             
            Temp01 = rxData[1];//数值转化
            Temp01 = Temp01 << 8;
            Temp01 = Temp01 + rxData[2];
            
						//-----------------------------------------------------温度1显示 
            LCD_write_char16x8(2,48,(Temp01 % 10)+16);//小数部分
            Temp01 = Temp01 / 10;
            LCD_write_char16x8(2,32,(Temp01 % 10)+16);//整数个位
            LCD_write_char16x8(2,24,(Temp01 / 10)+16);//整数十位 
        }
        else if(NRF24L01_status == 1)//rxData[0] == 0x02;//--------接收通道1处理
        {   
            Connect_Timer1 = 0;//通道1无线接收检查计时器清零
             
            Temp02 = rxData[1];//数值转化
            Temp02 = Temp02 << 8;
            Temp02 = Temp02 + rxData[2];
            
						//-----------------------------------------------------温度2显示
            LCD_write_char16x8(4,48,(Temp02 % 10)+16);//小数部分
            Temp02 = Temp02 / 10;
            LCD_write_char16x8(4,32,(Temp02 % 10)+16);//整数个位
            LCD_write_char16x8(4,24,(Temp02 / 10)+16);//整数十位 
            
        }
        else if(NRF24L01_status == 2)//rxData[0] == 0x03;//--------接收通道2处理
        {   
            Connect_Timer2 = 0;//通道2无线接收检查计时器清零
						
            Temp03 = rxData[1];//数值转化
            Temp03 = Temp03 << 8;
            Temp03 = Temp03 + rxData[2]; 
            
						//-----------------------------------------------------温度3显示 
            LCD_write_char16x8(6,48,(Temp03 % 10)+16);//小数部分
            Temp03 = Temp03 / 10;
            LCD_write_char16x8(6,32,(Temp03 % 10)+16);//整数个位
            LCD_write_char16x8(6,24,(Temp03 / 10)+16);//整数十位  
						
        }
				else if(NRF24L01_status == 3)//rxData[0] == 0x04;//--------接收通道3处理
        {   
            Connect_Timer3 = 0;//通道3无线接收检查计时器清零
						
            Temp04 = rxData[1];//数值转化
            Temp04 = Temp04 << 8;
            Temp04 = Temp04 + rxData[2]; 
            
						//-----------------------------------------------------温度4显示 
            LCD_write_char16x8(2,112,(Temp04 % 10)+16);//小数部分
            Temp04 = Temp04 / 10;
            LCD_write_char16x8(2,96,(Temp04 % 10)+16);//整数个位
            LCD_write_char16x8(2,88,(Temp04 / 10)+16);//整数十位  
						
        }
				else if(NRF24L01_status == 4)//rxData[0] == 0x05;//--------接收通道4处理
        {   
            Connect_Timer4 = 0;//通道4无线接收检查计时器清零
						
            Temp05 = rxData[1];//数值转化
            Temp05 = Temp05 << 8;
            Temp05 = Temp05 + rxData[2]; 
            
						//-----------------------------------------------------温度5显示 
            LCD_write_char16x8(4,112,(Temp05 % 10)+16);//小数部分
            Temp05 = Temp05 / 10;
            LCD_write_char16x8(4,96,(Temp05 % 10)+16);//整数个位
            LCD_write_char16x8(4,88,(Temp05 / 10)+16);//整数十位  
						
        }
				else if(NRF24L01_status == 5)//rxData[0] == 0x06;//--------接收通道5处理
        {   
            Connect_Timer5 = 0;//通道5无线接收检查计时器清零
						
            Temp06 = rxData[1];//数值转化
            Temp06 = Temp06 << 8;
            Temp06 = Temp06 + rxData[2]; 
            
						//-----------------------------------------------------温度6显示 
            LCD_write_char16x8(6,112,(Temp06 % 10)+16);//小数部分
            Temp06 = Temp06 / 10;
            LCD_write_char16x8(6,96,(Temp06 % 10)+16);//整数个位
            LCD_write_char16x8(6,88,(Temp06 / 10)+16);//整数十位  
						
        }
	  }
		//GPIO_SetBits(GPIOB, GPIO_Pin_3);
		//Delay(50000);Delay(50000);
		//GPIO_ResetBits(GPIOB, GPIO_Pin_3);
    //Delay(50000);Delay(50000);
		
		//halt();/*CPU in Active Halt mode */
};
}

//=====================================================================
//= Private functions---自---定---义---函---数----------------------- =
//=====================================================================
//=====================================================================
//Function : Delay( );                                                =
//Author   : zty                                                      =
//WriteData: 2014.08.04                                               =
//EditData : 2014.08.04                                               =
//Diary    : 1、系统延时;                                             =
//=====================================================================
void Delay(u16 nCount)
{//延时0.629us -- 16MHz
    while(nCount--);
}

//=====================================================================
//Function : CLK_Config( );                                           =
//Author   : zty                                                      =
//WriteData: 2014.08.04                                               =
//EditData : 2014.08.04                                               =
//Diary    : 1、系统时钟源选择HSI(16M/hz);                            =
//           2、RTC时钟选择LSI(38K/hz);                               =
//=====================================================================
static void CLK_Config(void)
{
    /* Initialization of the clock */
		CLK_SYSCLKSourceSwitchCmd(ENABLE);
		/* Select HSI as system clock source */
		CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);//HSI = 16M/Hz
    /* Clock divider to HSI/1 */
    CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);//分频只会影响系统时钟源
		/* Enable RTC clock *///LSI = 38K/Hz
    CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_64);
		/* Enable RTC clock */
    CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
		/* Enable TIM4 CLK */
    CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
}

//=====================================================================
//Function : RTC_Config( );                                           =
//Author   : zty                                                      =
//WriteData: 2014.08.16                                               =
//EditData : 2014.08.16                                               =
//Diary    : 1、配置RTC外围;                                          =
//=====================================================================
static void RTC_Config(void)
{	
	/* Disable WakeUp */
  RTC_WakeUpCmd(DISABLE);//配置唤醒单位前,必须先禁用才能设置!!!
		
  /* RTC will wake-up from halt every 2s
  RTCClk = 38K/Hz
  Wakeup tick = 38000Hz / 64 / 16 = 37Hz = 0.027s
  0.027 x 74 = 2s */

  /* Configures the RTC */
  RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);	
  /* RTC will wake-up from halt every 4s */	
	RTC_SetWakeUpCounter(37);
	/* Enable wake up unit Interrupt */
  RTC_ITConfig(RTC_IT_WUT, ENABLE);
	
	/* Enable WakeUp */
	RTC_WakeUpCmd(ENABLE);
}

//=====================================================================
//Function : TIM4_Config( );                                          =
//Author   : zty                                                      =
//WriteData: 2014.08.16                                               =
//EditData : 2014.08.16                                               =
//Diary    : 1、配置TIM4外设;                                         =
//=====================================================================
static void TIM4_Config(void)
{
  /* TIM4 configuration:
   - TIM4CLK is set to 16 MHz, the TIM4 Prescaler is equal to 128 so the TIM1 counter
   clock used is 16 MHz / 128 = 125 000 Hz
  - With 125 000 Hz we can generate time base:
      max time base is 2.048 ms if TIM4_PERIOD = 255 --> (255 + 1) / 125000 = 2.048 ms
      min time base is 0.016 ms if TIM4_PERIOD = 1   --> (  1 + 1) / 125000 = 0.016 ms
  - In this example we need to generate a time base equal to 1 ms
   so TIM4_PERIOD = (0.001 * 125000 - 1) = 124 */

  /* Time base configuration */
  TIM4_TimeBaseInit(TIM4_Prescaler_32768, 244);//时基0.5s,0.5s溢出中断
  /* Clear TIM4 update flag */
  TIM4_ClearFlag(TIM4_FLAG_Update);
  /* Enable update interrupt */
  TIM4_ITConfig(TIM4_IT_Update, ENABLE);

  /* Enable TIM4 */
  TIM4_Cmd(ENABLE);
}

//=====================================================================
//Function : IO_Config( );                                            =
//Author   : zty                                                      =
//WriteData: 2014.08.09                                               =
//EditData : 2014.08.09                                               =
//Diary    : 1、配置IO口模式;                                         =
//=====================================================================
static void IO_Config(void)
{
    GPIO_DeInit(GPIOB);//重新初始化GPIO外设寄存器为复位默认值，慎用！
	  GPIO_Init(GPIOB, GPIO_Pin_3, GPIO_Mode_Out_PP_Low_Fast);//LED,PB.3
		GPIO_DeInit(GPIOA);//重新初始化GPIO外设寄存器为复位默认值，慎用！
		GPIO_DeInit(GPIOD);//重新初始化GPIO外设寄存器为复位默认值，慎用！
}
