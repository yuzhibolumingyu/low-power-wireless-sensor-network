//============================================================================
//File    : st7567.h                                                         =
//Date    : 2015.03.05                                                       =
//Author  : zty                                                              =
//Diary   : 1、ST7567接口定义;                                               =
//============================================================================
#ifndef _st7567_H_
#define _st7567_H_ 

//=====================================================================
//= Includes------------头---文---件---包---含----------------------- =
//=====================================================================
#include "stm8l15x_gpio.h"

//=====================================================================
//= Private define-----------宏---定---义---------------Private macro =
//=====================================================================

#define ST7567_CSB_OUT  GPIO_Init(GPIOD, GPIO_Pin_7, GPIO_Mode_Out_PP_Low_Slow)
#define ST7567_CSB_H    GPIO_SetBits(GPIOD, GPIO_Pin_7)
#define ST7567_CSB_L    GPIO_ResetBits(GPIOD, GPIO_Pin_7)//片选

#define ST7567_RST_OUT  GPIO_Init(GPIOD, GPIO_Pin_6, GPIO_Mode_Out_PP_Low_Slow)
#define ST7567_RST_H    GPIO_SetBits(GPIOD, GPIO_Pin_6)
#define ST7567_RST_L    GPIO_ResetBits(GPIOD, GPIO_Pin_6)//复位

#define ST7567_RS_OUT   GPIO_Init(GPIOD, GPIO_Pin_5, GPIO_Mode_Out_PP_Low_Slow)
#define ST7567_RS_H     GPIO_SetBits(GPIOD, GPIO_Pin_5)
#define ST7567_RS_L     GPIO_ResetBits(GPIOD, GPIO_Pin_5)//数据or命令

#define ST7567_SCL_OUT  GPIO_Init(GPIOA, GPIO_Pin_3, GPIO_Mode_Out_PP_Low_Slow)
#define ST7567_SCL_H    GPIO_SetBits(GPIOA, GPIO_Pin_3)
#define ST7567_SCL_L    GPIO_ResetBits(GPIOA, GPIO_Pin_3)//时钟

#define ST7567_SDA_OUT  GPIO_Init(GPIOA, GPIO_Pin_2, GPIO_Mode_Out_PP_Low_Slow)
#define ST7567_SDA_H    GPIO_SetBits(GPIOA, GPIO_Pin_2)
#define ST7567_SDA_L    GPIO_ResetBits(GPIOA, GPIO_Pin_2)//数据输入

#define ST7567_BL_OUT   GPIO_Init(GPIOA, GPIO_Pin_6, GPIO_Mode_Out_PP_Low_Slow)
#define ST7567_BL_H     GPIO_SetBits(GPIOA, GPIO_Pin_6)
#define ST7567_BL_L     GPIO_ResetBits(GPIOA, GPIO_Pin_6)//背光

//=====================================================================
//= function prototypes---函---数---声---明-------------------------- =
//=====================================================================
void LCD_Init_ST7567(void);//液晶初始化;
void LCD_Addr_xy(unsigned char page, unsigned char column);//设置液晶显示坐标;
void LCD_Clear(unsigned char xx);//液晶清屏;
void LCD_DrawBmp(unsigned char *dp);//液晶显示一幅图片;
void LCD_write_chinese32x32(unsigned char page, unsigned char column, unsigned char c);//显示32x32点汉字;
void LCD_write_chinese24x24(unsigned char page, unsigned char column, unsigned char c);//显示24x24点汉字;
void LCD_write_chinese16x16(unsigned char page, unsigned char column, unsigned char c);//显示16x16点汉字;
void LCD_write_chinese12x12(unsigned char page, unsigned char column, unsigned char c);//显示12x12点汉字;
void LCD_write_char16x8(unsigned char page, unsigned char column, unsigned char c);//显示16x8点字符;
void LCD_write_char16x8_string(unsigned char page, unsigned char column, unsigned char *s);//显示16x8点字符串;
void LCD_write_char8x6(unsigned char c);//显示8x6点字符;
void LCD_write_char8x6_string(unsigned char page, unsigned char column, unsigned char *s);//显示8x6点字符串;
void LCD_Sleep(void);//进入睡眠模式;
void LCD_Wake(void);//退出睡眠模式;
//void Start_Read_Modify_Write(void);//开始"读取-修改-写入"模式;
//void End_Read_Modify_Write(void);//结束"读取-修改-写入"模式;
void LCD_ST7567_WriteCmd(unsigned char Comd);//写命令到液晶;
void LCD_ST7567_WriteDat(unsigned char Data);//写数据到液晶;
void LCD_Init_Display(void);//开机画面初始化;

#endif