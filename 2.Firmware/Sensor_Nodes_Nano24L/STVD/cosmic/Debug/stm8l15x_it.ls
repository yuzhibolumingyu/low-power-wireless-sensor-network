   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  44                     ; 64 INTERRUPT_HANDLER(NonHandledInterrupt, 0)
  44                     ; 65 {
  45                     	switch	.text
  46  0000               f_NonHandledInterrupt:
  50                     ; 69 }
  53  0000 80            	iret
  75                     ; 77 INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
  75                     ; 78 {
  76                     	switch	.text
  77  0001               f_TRAP_IRQHandler:
  81                     ; 82 }
  84  0001 80            	iret
 106                     ; 88 INTERRUPT_HANDLER(FLASH_IRQHandler, 1)
 106                     ; 89 {
 107                     	switch	.text
 108  0002               f_FLASH_IRQHandler:
 112                     ; 93 }
 115  0002 80            	iret
 138                     ; 99 INTERRUPT_HANDLER(DMA1_CHANNEL0_1_IRQHandler, 2)
 138                     ; 100 {
 139                     	switch	.text
 140  0003               f_DMA1_CHANNEL0_1_IRQHandler:
 144                     ; 104 }
 147  0003 80            	iret
 170                     ; 110 INTERRUPT_HANDLER(DMA1_CHANNEL2_3_IRQHandler, 3)
 170                     ; 111 {
 171                     	switch	.text
 172  0004               f_DMA1_CHANNEL2_3_IRQHandler:
 176                     ; 115 }
 179  0004 80            	iret
 203                     ; 121 INTERRUPT_HANDLER(RTC_CSSLSE_IRQHandler, 4)
 203                     ; 122 {	
 204                     	switch	.text
 205  0005               f_RTC_CSSLSE_IRQHandler:
 207  0005 3b0002        	push	c_x+2
 208  0008 be00          	ldw	x,c_x
 209  000a 89            	pushw	x
 210  000b 3b0002        	push	c_y+2
 211  000e be00          	ldw	x,c_y
 212  0010 89            	pushw	x
 215                     ; 124   RTC_ClearITPendingBit(RTC_IT_WUT);
 217  0011 ae0040        	ldw	x,#64
 218  0014 cd0000        	call	_RTC_ClearITPendingBit
 220                     ; 125 }
 223  0017 85            	popw	x
 224  0018 bf00          	ldw	c_y,x
 225  001a 320002        	pop	c_y+2
 226  001d 85            	popw	x
 227  001e bf00          	ldw	c_x,x
 228  0020 320002        	pop	c_x+2
 229  0023 80            	iret
 252                     ; 131 INTERRUPT_HANDLER(EXTIE_F_PVD_IRQHandler, 5)
 252                     ; 132 {
 253                     	switch	.text
 254  0024               f_EXTIE_F_PVD_IRQHandler:
 258                     ; 136 }
 261  0024 80            	iret
 283                     ; 143 INTERRUPT_HANDLER(EXTIB_G_IRQHandler, 6)
 283                     ; 144 {
 284                     	switch	.text
 285  0025               f_EXTIB_G_IRQHandler:
 289                     ; 148 }
 292  0025 80            	iret
 314                     ; 155 INTERRUPT_HANDLER(EXTID_H_IRQHandler, 7)
 314                     ; 156 {
 315                     	switch	.text
 316  0026               f_EXTID_H_IRQHandler:
 320                     ; 160 }
 323  0026 80            	iret
 345                     ; 167 INTERRUPT_HANDLER(EXTI0_IRQHandler, 8)
 345                     ; 168 {
 346                     	switch	.text
 347  0027               f_EXTI0_IRQHandler:
 351                     ; 172 }
 354  0027 80            	iret
 376                     ; 179 INTERRUPT_HANDLER(EXTI1_IRQHandler, 9)
 376                     ; 180 {
 377                     	switch	.text
 378  0028               f_EXTI1_IRQHandler:
 382                     ; 184 }
 385  0028 80            	iret
 407                     ; 191 INTERRUPT_HANDLER(EXTI2_IRQHandler, 10)
 407                     ; 192 {
 408                     	switch	.text
 409  0029               f_EXTI2_IRQHandler:
 413                     ; 196 }
 416  0029 80            	iret
 438                     ; 203 INTERRUPT_HANDLER(EXTI3_IRQHandler, 11)
 438                     ; 204 {
 439                     	switch	.text
 440  002a               f_EXTI3_IRQHandler:
 444                     ; 208 }
 447  002a 80            	iret
 469                     ; 215 INTERRUPT_HANDLER(EXTI4_IRQHandler, 12)
 469                     ; 216 {
 470                     	switch	.text
 471  002b               f_EXTI4_IRQHandler:
 475                     ; 220 }
 478  002b 80            	iret
 500                     ; 227 INTERRUPT_HANDLER(EXTI5_IRQHandler, 13)
 500                     ; 228 {
 501                     	switch	.text
 502  002c               f_EXTI5_IRQHandler:
 506                     ; 232 }
 509  002c 80            	iret
 531                     ; 239 INTERRUPT_HANDLER(EXTI6_IRQHandler, 14)
 531                     ; 240 {
 532                     	switch	.text
 533  002d               f_EXTI6_IRQHandler:
 537                     ; 244 }
 540  002d 80            	iret
 562                     ; 251 INTERRUPT_HANDLER(EXTI7_IRQHandler, 15)
 562                     ; 252 {
 563                     	switch	.text
 564  002e               f_EXTI7_IRQHandler:
 568                     ; 256 }
 571  002e 80            	iret
 593                     ; 262 INTERRUPT_HANDLER(LCD_AES_IRQHandler, 16)
 593                     ; 263 {
 594                     	switch	.text
 595  002f               f_LCD_AES_IRQHandler:
 599                     ; 267 }
 602  002f 80            	iret
 625                     ; 273 INTERRUPT_HANDLER(SWITCH_CSS_BREAK_DAC_IRQHandler, 17)
 625                     ; 274 {
 626                     	switch	.text
 627  0030               f_SWITCH_CSS_BREAK_DAC_IRQHandler:
 631                     ; 278 }
 634  0030 80            	iret
 657                     ; 285 INTERRUPT_HANDLER(ADC1_COMP_IRQHandler, 18)
 657                     ; 286 {
 658                     	switch	.text
 659  0031               f_ADC1_COMP_IRQHandler:
 663                     ; 290 }
 666  0031 80            	iret
 690                     ; 297 INTERRUPT_HANDLER(TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler, 19)
 690                     ; 298 {
 691                     	switch	.text
 692  0032               f_TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler:
 696                     ; 302 }
 699  0032 80            	iret
 722                     ; 309 INTERRUPT_HANDLER(TIM2_CC_USART2_RX_IRQHandler, 20)
 722                     ; 310 {
 723                     	switch	.text
 724  0033               f_TIM2_CC_USART2_RX_IRQHandler:
 728                     ; 314 }
 731  0033 80            	iret
 755                     ; 322 INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler, 21)
 755                     ; 323 {
 756                     	switch	.text
 757  0034               f_TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler:
 761                     ; 327 }
 764  0034 80            	iret
 787                     ; 333 INTERRUPT_HANDLER(TIM3_CC_USART3_RX_IRQHandler, 22)
 787                     ; 334 {
 788                     	switch	.text
 789  0035               f_TIM3_CC_USART3_RX_IRQHandler:
 793                     ; 338 }
 796  0035 80            	iret
 819                     ; 344 INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_COM_IRQHandler, 23)
 819                     ; 345 {
 820                     	switch	.text
 821  0036               f_TIM1_UPD_OVF_TRG_COM_IRQHandler:
 825                     ; 349 }
 828  0036 80            	iret
 850                     ; 355 INTERRUPT_HANDLER(TIM1_CC_IRQHandler, 24)
 850                     ; 356 {
 851                     	switch	.text
 852  0037               f_TIM1_CC_IRQHandler:
 856                     ; 360 }
 859  0037 80            	iret
 891                     ; 367 INTERRUPT_HANDLER(TIM4_UPD_OVF_TRG_IRQHandler, 25)
 891                     ; 368 {
 892                     	switch	.text
 893  0038               f_TIM4_UPD_OVF_TRG_IRQHandler:
 895  0038 3b0002        	push	c_x+2
 896  003b be00          	ldw	x,c_x
 897  003d 89            	pushw	x
 898  003e 3b0002        	push	c_y+2
 899  0041 be00          	ldw	x,c_y
 900  0043 89            	pushw	x
 903                     ; 369 	  GPIO_ToggleBits(GPIOB, GPIO_Pin_3);
 905  0044 4b08          	push	#8
 906  0046 ae5005        	ldw	x,#20485
 907  0049 cd0000        	call	_GPIO_ToggleBits
 909  004c 84            	pop	a
 910                     ; 372     Connect_Timer0++;
 912  004d be00          	ldw	x,_Connect_Timer0
 913  004f 1c0001        	addw	x,#1
 914  0052 bf00          	ldw	_Connect_Timer0,x
 915                     ; 373     if(Connect_Timer0 > 240)//超过2分钟便会清除
 917  0054 be00          	ldw	x,_Connect_Timer0
 918  0056 a300f1        	cpw	x,#241
 919  0059 250f          	jrult	L143
 920                     ; 375         Connect_Timer0 = 0;//Clear,在下一个时段再做判断
 922  005b 5f            	clrw	x
 923  005c bf00          	ldw	_Connect_Timer0,x
 924                     ; 376         LCD_write_char16x8_string(2, 0, "T1:--.-~");//--------通道0,清除温度1显示
 926  005e ae002d        	ldw	x,#L343
 927  0061 89            	pushw	x
 928  0062 5f            	clrw	x
 929  0063 a602          	ld	a,#2
 930  0065 95            	ld	xh,a
 931  0066 cd0000        	call	_LCD_write_char16x8_string
 933  0069 85            	popw	x
 934  006a               L143:
 935                     ; 378     Connect_Timer1++;
 937  006a be00          	ldw	x,_Connect_Timer1
 938  006c 1c0001        	addw	x,#1
 939  006f bf00          	ldw	_Connect_Timer1,x
 940                     ; 379     if(Connect_Timer1 > 240)//超过2分钟便会清除
 942  0071 be00          	ldw	x,_Connect_Timer1
 943  0073 a300f1        	cpw	x,#241
 944  0076 250f          	jrult	L543
 945                     ; 381         Connect_Timer1 = 0;//Clear,在下一个时段再做判断
 947  0078 5f            	clrw	x
 948  0079 bf00          	ldw	_Connect_Timer1,x
 949                     ; 382         LCD_write_char16x8_string(4, 0, "T2:--.-~");//--------通道1,清除温度2显示
 951  007b ae0024        	ldw	x,#L743
 952  007e 89            	pushw	x
 953  007f 5f            	clrw	x
 954  0080 a604          	ld	a,#4
 955  0082 95            	ld	xh,a
 956  0083 cd0000        	call	_LCD_write_char16x8_string
 958  0086 85            	popw	x
 959  0087               L543:
 960                     ; 384     Connect_Timer2++;
 962  0087 be00          	ldw	x,_Connect_Timer2
 963  0089 1c0001        	addw	x,#1
 964  008c bf00          	ldw	_Connect_Timer2,x
 965                     ; 385     if(Connect_Timer2 > 240)//超过2分钟便会清除
 967  008e be00          	ldw	x,_Connect_Timer2
 968  0090 a300f1        	cpw	x,#241
 969  0093 250f          	jrult	L153
 970                     ; 387         Connect_Timer2 = 0;//Clear,在下一个时段再做判断
 972  0095 5f            	clrw	x
 973  0096 bf00          	ldw	_Connect_Timer2,x
 974                     ; 388         LCD_write_char16x8_string(6, 0, "T3:--.-~");//--------通道2,清除温度3显示
 976  0098 ae001b        	ldw	x,#L353
 977  009b 89            	pushw	x
 978  009c 5f            	clrw	x
 979  009d a606          	ld	a,#6
 980  009f 95            	ld	xh,a
 981  00a0 cd0000        	call	_LCD_write_char16x8_string
 983  00a3 85            	popw	x
 984  00a4               L153:
 985                     ; 390 		Connect_Timer3++;
 987  00a4 be00          	ldw	x,_Connect_Timer3
 988  00a6 1c0001        	addw	x,#1
 989  00a9 bf00          	ldw	_Connect_Timer3,x
 990                     ; 391     if(Connect_Timer3 > 240)//超过2分钟便会清除
 992  00ab be00          	ldw	x,_Connect_Timer3
 993  00ad a300f1        	cpw	x,#241
 994  00b0 2511          	jrult	L553
 995                     ; 393         Connect_Timer3 = 0;//Clear,在下一个时段再做判断
 997  00b2 5f            	clrw	x
 998  00b3 bf00          	ldw	_Connect_Timer3,x
 999                     ; 394         LCD_write_char16x8_string(2, 64, "T4:--.-~");//--------通道3,清除温度4显示
1001  00b5 ae0012        	ldw	x,#L753
1002  00b8 89            	pushw	x
1003  00b9 ae0040        	ldw	x,#64
1004  00bc a602          	ld	a,#2
1005  00be 95            	ld	xh,a
1006  00bf cd0000        	call	_LCD_write_char16x8_string
1008  00c2 85            	popw	x
1009  00c3               L553:
1010                     ; 396 		Connect_Timer4++;
1012  00c3 be00          	ldw	x,_Connect_Timer4
1013  00c5 1c0001        	addw	x,#1
1014  00c8 bf00          	ldw	_Connect_Timer4,x
1015                     ; 397     if(Connect_Timer4 > 240)//超过2分钟便会清除
1017  00ca be00          	ldw	x,_Connect_Timer4
1018  00cc a300f1        	cpw	x,#241
1019  00cf 2511          	jrult	L163
1020                     ; 399         Connect_Timer4 = 0;//Clear,在下一个时段再做判断
1022  00d1 5f            	clrw	x
1023  00d2 bf00          	ldw	_Connect_Timer4,x
1024                     ; 400         LCD_write_char16x8_string(4, 64, "T5:--.-~");//--------通道4,清除温度5显示
1026  00d4 ae0009        	ldw	x,#L363
1027  00d7 89            	pushw	x
1028  00d8 ae0040        	ldw	x,#64
1029  00db a604          	ld	a,#4
1030  00dd 95            	ld	xh,a
1031  00de cd0000        	call	_LCD_write_char16x8_string
1033  00e1 85            	popw	x
1034  00e2               L163:
1035                     ; 402 		Connect_Timer5++;
1037  00e2 be00          	ldw	x,_Connect_Timer5
1038  00e4 1c0001        	addw	x,#1
1039  00e7 bf00          	ldw	_Connect_Timer5,x
1040                     ; 403     if(Connect_Timer5 > 240)//超过2分钟便会清除
1042  00e9 be00          	ldw	x,_Connect_Timer5
1043  00eb a300f1        	cpw	x,#241
1044  00ee 2511          	jrult	L563
1045                     ; 405         Connect_Timer5 = 0;//Clear,在下一个时段再做判断
1047  00f0 5f            	clrw	x
1048  00f1 bf00          	ldw	_Connect_Timer5,x
1049                     ; 406         LCD_write_char16x8_string(6, 64, "T6:--.-~");//--------通道5,清除温度6显示
1051  00f3 ae0000        	ldw	x,#L763
1052  00f6 89            	pushw	x
1053  00f7 ae0040        	ldw	x,#64
1054  00fa a606          	ld	a,#6
1055  00fc 95            	ld	xh,a
1056  00fd cd0000        	call	_LCD_write_char16x8_string
1058  0100 85            	popw	x
1059  0101               L563:
1060                     ; 410     TIM4_ClearITPendingBit(TIM4_IT_Update);
1062  0101 a601          	ld	a,#1
1063  0103 cd0000        	call	_TIM4_ClearITPendingBit
1065                     ; 411 }
1068  0106 85            	popw	x
1069  0107 bf00          	ldw	c_y,x
1070  0109 320002        	pop	c_y+2
1071  010c 85            	popw	x
1072  010d bf00          	ldw	c_x,x
1073  010f 320002        	pop	c_x+2
1074  0112 80            	iret
1096                     ; 417 INTERRUPT_HANDLER(SPI1_IRQHandler, 26)
1096                     ; 418 {
1097                     	switch	.text
1098  0113               f_SPI1_IRQHandler:
1102                     ; 422 }
1105  0113 80            	iret
1129                     ; 429 INTERRUPT_HANDLER(USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler, 27)
1129                     ; 430 {
1130                     	switch	.text
1131  0114               f_USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler:
1135                     ; 434 }
1138  0114 80            	iret
1161                     ; 441 INTERRUPT_HANDLER(USART1_RX_TIM5_CC_IRQHandler, 28)
1161                     ; 442 {
1162                     	switch	.text
1163  0115               f_USART1_RX_TIM5_CC_IRQHandler:
1167                     ; 446 }
1170  0115 80            	iret
1193                     ; 453 INTERRUPT_HANDLER(I2C1_SPI2_IRQHandler, 29)
1193                     ; 454 {
1194                     	switch	.text
1195  0116               f_I2C1_SPI2_IRQHandler:
1199                     ; 458 }
1202  0116 80            	iret
1214                     	xref	_LCD_write_char16x8_string
1215                     	xref.b	_Connect_Timer5
1216                     	xref.b	_Connect_Timer4
1217                     	xref.b	_Connect_Timer3
1218                     	xref.b	_Connect_Timer2
1219                     	xref.b	_Connect_Timer1
1220                     	xref.b	_Connect_Timer0
1221                     	xdef	f_I2C1_SPI2_IRQHandler
1222                     	xdef	f_USART1_RX_TIM5_CC_IRQHandler
1223                     	xdef	f_USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler
1224                     	xdef	f_SPI1_IRQHandler
1225                     	xdef	f_TIM4_UPD_OVF_TRG_IRQHandler
1226                     	xdef	f_TIM1_CC_IRQHandler
1227                     	xdef	f_TIM1_UPD_OVF_TRG_COM_IRQHandler
1228                     	xdef	f_TIM3_CC_USART3_RX_IRQHandler
1229                     	xdef	f_TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler
1230                     	xdef	f_TIM2_CC_USART2_RX_IRQHandler
1231                     	xdef	f_TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler
1232                     	xdef	f_ADC1_COMP_IRQHandler
1233                     	xdef	f_SWITCH_CSS_BREAK_DAC_IRQHandler
1234                     	xdef	f_LCD_AES_IRQHandler
1235                     	xdef	f_EXTI7_IRQHandler
1236                     	xdef	f_EXTI6_IRQHandler
1237                     	xdef	f_EXTI5_IRQHandler
1238                     	xdef	f_EXTI4_IRQHandler
1239                     	xdef	f_EXTI3_IRQHandler
1240                     	xdef	f_EXTI2_IRQHandler
1241                     	xdef	f_EXTI1_IRQHandler
1242                     	xdef	f_EXTI0_IRQHandler
1243                     	xdef	f_EXTID_H_IRQHandler
1244                     	xdef	f_EXTIB_G_IRQHandler
1245                     	xdef	f_EXTIE_F_PVD_IRQHandler
1246                     	xdef	f_RTC_CSSLSE_IRQHandler
1247                     	xdef	f_DMA1_CHANNEL2_3_IRQHandler
1248                     	xdef	f_DMA1_CHANNEL0_1_IRQHandler
1249                     	xdef	f_FLASH_IRQHandler
1250                     	xdef	f_TRAP_IRQHandler
1251                     	xdef	f_NonHandledInterrupt
1252                     	xref	_TIM4_ClearITPendingBit
1253                     	xref	_RTC_ClearITPendingBit
1254                     	xref	_GPIO_ToggleBits
1255                     .const:	section	.text
1256  0000               L763:
1257  0000 54363a2d2d2e  	dc.b	"T6:--.-~",0
1258  0009               L363:
1259  0009 54353a2d2d2e  	dc.b	"T5:--.-~",0
1260  0012               L753:
1261  0012 54343a2d2d2e  	dc.b	"T4:--.-~",0
1262  001b               L353:
1263  001b 54333a2d2d2e  	dc.b	"T3:--.-~",0
1264  0024               L743:
1265  0024 54323a2d2d2e  	dc.b	"T2:--.-~",0
1266  002d               L343:
1267  002d 54313a2d2d2e  	dc.b	"T1:--.-~",0
1268                     	xref.b	c_x
1269                     	xref.b	c_y
1289                     	end
