   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  15                     	bsct
  16  0000               _TX_ADDRESS:
  17  0000 34            	dc.b	52
  18  0001 43            	dc.b	67
  19  0002 10            	dc.b	16
  20  0003 10            	dc.b	16
  21  0004 01            	dc.b	1
  22  0005               _RX_ADDRESS:
  23  0005 34            	dc.b	52
  24  0006 43            	dc.b	67
  25  0007 10            	dc.b	16
  26  0008 10            	dc.b	16
  27  0009 01            	dc.b	1
  28  000a               _RX_ADDRESS1:
  29  000a b0            	dc.b	176
  30  000b 43            	dc.b	67
  31  000c 10            	dc.b	16
  32  000d 10            	dc.b	16
  33  000e 01            	dc.b	1
  34  000f               _RX_ADDRESS2:
  35  000f b1            	dc.b	177
  36  0010               _RX_ADDRESS3:
  37  0010 b2            	dc.b	178
  38  0011               _RX_ADDRESS4:
  39  0011 b3            	dc.b	179
  40  0012               _RX_ADDRESS5:
  41  0012 b4            	dc.b	180
  81                     ; 33 void delay_ms(u16 nCount)
  81                     ; 34  {//延时0.629us -- 16MHz
  83                     	switch	.text
  84  0000               _delay_ms:
  86  0000 89            	pushw	x
  87       00000000      OFST:	set	0
  90                     ; 35     nCount = (nCount*1600);
  92  0001 1e01          	ldw	x,(OFST+1,sp)
  93  0003 90ae0640      	ldw	y,#1600
  94  0007 cd0000        	call	c_imul
  96  000a 1f01          	ldw	(OFST+1,sp),x
  98  000c               L13:
  99                     ; 36     while(nCount--);
 101  000c 1e01          	ldw	x,(OFST+1,sp)
 102  000e 1d0001        	subw	x,#1
 103  0011 1f01          	ldw	(OFST+1,sp),x
 104  0013 1c0001        	addw	x,#1
 105  0016 a30000        	cpw	x,#0
 106  0019 26f1          	jrne	L13
 107                     ; 37  }
 110  001b 85            	popw	x
 111  001c 81            	ret
 147                     ; 51 void Init_NRF24L01(void)
 147                     ; 52 { 
 148                     	switch	.text
 149  001d               _Init_NRF24L01:
 153                     ; 53     NRF24L01_CE_DDR;//输出
 155  001d 4bf0          	push	#240
 156  001f 4b02          	push	#2
 157  0021 ae500f        	ldw	x,#20495
 158  0024 cd0000        	call	_GPIO_Init
 160  0027 85            	popw	x
 161                     ; 54     NRF24L01_CE_SET;//置'1'
 164  0028 4b02          	push	#2
 165  002a ae500f        	ldw	x,#20495
 166  002d cd0000        	call	_GPIO_SetBits
 168  0030 84            	pop	a
 169                     ; 56     NRF24L01_CSN_DDR;//输出	
 172  0031 4bf0          	push	#240
 173  0033 4b01          	push	#1
 174  0035 ae500f        	ldw	x,#20495
 175  0038 cd0000        	call	_GPIO_Init
 177  003b 85            	popw	x
 178                     ; 57     NRF24L01_CSN_SET;//置'1'	
 181  003c 4b01          	push	#1
 182  003e ae500f        	ldw	x,#20495
 183  0041 cd0000        	call	_GPIO_SetBits
 185  0044 84            	pop	a
 186                     ; 59     NRF24L01_MOSI_DDR;  //输出
 189  0045 4bf0          	push	#240
 190  0047 4b04          	push	#4
 191  0049 ae500f        	ldw	x,#20495
 192  004c cd0000        	call	_GPIO_Init
 194  004f 85            	popw	x
 195                     ; 60     NRF24L01_MOSI_RESET;//置'0'	
 198  0050 4b04          	push	#4
 199  0052 ae500f        	ldw	x,#20495
 200  0055 cd0000        	call	_GPIO_ResetBits
 202  0058 84            	pop	a
 203                     ; 62     NRF24L01_IRQ_DDR;//输入
 206  0059 4b40          	push	#64
 207  005b 4b01          	push	#1
 208  005d ae5005        	ldw	x,#20485
 209  0060 cd0000        	call	_GPIO_Init
 211  0063 85            	popw	x
 212                     ; 63     NRF24L01_IRQ_SET;//置'1'
 215  0064 4b01          	push	#1
 216  0066 ae5005        	ldw	x,#20485
 217  0069 cd0000        	call	_GPIO_SetBits
 219  006c 84            	pop	a
 220                     ; 65     NRF24L01_MISO_DDR;//输入
 223  006d 4b40          	push	#64
 224  006f 4b02          	push	#2
 225  0071 ae5005        	ldw	x,#20485
 226  0074 cd0000        	call	_GPIO_Init
 228  0077 85            	popw	x
 229                     ; 66     NRF24L01_MISO_SET;//置'1'
 232  0078 4b02          	push	#2
 233  007a ae5005        	ldw	x,#20485
 234  007d cd0000        	call	_GPIO_SetBits
 236  0080 84            	pop	a
 237                     ; 68     NRF24L01_SCK_DDR;  //输出	    
 240  0081 4bf0          	push	#240
 241  0083 4b08          	push	#8
 242  0085 ae500f        	ldw	x,#20495
 243  0088 cd0000        	call	_GPIO_Init
 245  008b 85            	popw	x
 246                     ; 69     NRF24L01_SCK_RESET;//置'0'
 249  008c 4b08          	push	#8
 250  008e ae500f        	ldw	x,#20495
 251  0091 cd0000        	call	_GPIO_ResetBits
 253  0094 84            	pop	a
 254                     ; 71     NRF24L01_CE_RESET;//置'0'  
 257  0095 4b02          	push	#2
 258  0097 ae500f        	ldw	x,#20495
 259  009a cd0000        	call	_GPIO_ResetBits
 261  009d 84            	pop	a
 262                     ; 72 	  SPI_Write_Reg(WRITE_REG + CONFIG, 0x0d);//掉电
 265  009e ae000d        	ldw	x,#13
 266  00a1 a620          	ld	a,#32
 267  00a3 95            	ld	xh,a
 268  00a4 cd0000        	call	_SPI_Write_Reg
 270                     ; 73 		delay_ms(1);
 272  00a7 ae0001        	ldw	x,#1
 273  00aa cd0000        	call	_delay_ms
 275                     ; 74     SPI_Write_Reg(WRITE_REG + EN_AA, 0x3F);      //频道0、1、2、3、4、5，自动ACK应答允许	
 277  00ad ae003f        	ldw	x,#63
 278  00b0 a621          	ld	a,#33
 279  00b2 95            	ld	xh,a
 280  00b3 cd0000        	call	_SPI_Write_Reg
 282                     ; 75     SPI_Write_Reg(WRITE_REG + EN_RXADDR, 0x3F);  //频道0、1、2、3、4、5，接收地址允许
 284  00b6 ae003f        	ldw	x,#63
 285  00b9 a622          	ld	a,#34
 286  00bb 95            	ld	xh,a
 287  00bc cd0000        	call	_SPI_Write_Reg
 289                     ; 76 		SPI_Write_Reg(WRITE_REG + SETUP_AW, 0x03);   //设置地址宽度为5字节
 291  00bf ae0003        	ldw	x,#3
 292  00c2 a623          	ld	a,#35
 293  00c4 95            	ld	xh,a
 294  00c5 cd0000        	call	_SPI_Write_Reg
 296                     ; 77     SPI_Write_Reg(WRITE_REG + SETUP_RETR, 0x13); //自动重发延时500us + 86us,自动重发3次
 298  00c8 ae0013        	ldw	x,#19
 299  00cb a624          	ld	a,#36
 300  00cd 95            	ld	xh,a
 301  00ce cd0000        	call	_SPI_Write_Reg
 303                     ; 78     SPI_Write_Reg(WRITE_REG + RF_CH, 0x6F);      //设置信道工作为2.4GHZ，收发必须一致，频道未知!!!!!!
 305  00d1 ae006f        	ldw	x,#111
 306  00d4 a625          	ld	a,#37
 307  00d6 95            	ld	xh,a
 308  00d7 cd0000        	call	_SPI_Write_Reg
 310                     ; 79     SPI_Write_Reg(WRITE_REG + RF_SETUP, 0x07);   //设置发射速率为1MHZ，发射功率为最大值0dB	
 312  00da ae0007        	ldw	x,#7
 313  00dd a626          	ld	a,#38
 314  00df 95            	ld	xh,a
 315  00e0 cd0000        	call	_SPI_Write_Reg
 317                     ; 81     SPI_Write_Reg(WRITE_REG + RX_PW_P0, RX_PLOAD_WIDTH);//设置接收数据长度，本次设置为32字节
 319  00e3 ae0007        	ldw	x,#7
 320  00e6 a631          	ld	a,#49
 321  00e8 95            	ld	xh,a
 322  00e9 cd0000        	call	_SPI_Write_Reg
 324                     ; 82 		SPI_Write_Reg(WRITE_REG + RX_PW_P1, RX_PLOAD_WIDTH);//设置接收数据长度，本次设置为32字节
 326  00ec ae0007        	ldw	x,#7
 327  00ef a632          	ld	a,#50
 328  00f1 95            	ld	xh,a
 329  00f2 cd0000        	call	_SPI_Write_Reg
 331                     ; 83     SPI_Write_Reg(WRITE_REG + RX_PW_P2, RX_PLOAD_WIDTH);//设置接收数据长度，本次设置为32字节
 333  00f5 ae0007        	ldw	x,#7
 334  00f8 a633          	ld	a,#51
 335  00fa 95            	ld	xh,a
 336  00fb cd0000        	call	_SPI_Write_Reg
 338                     ; 84     SPI_Write_Reg(WRITE_REG + RX_PW_P3, RX_PLOAD_WIDTH);//设置接收数据长度，本次设置为32字节
 340  00fe ae0007        	ldw	x,#7
 341  0101 a634          	ld	a,#52
 342  0103 95            	ld	xh,a
 343  0104 cd0000        	call	_SPI_Write_Reg
 345                     ; 85     SPI_Write_Reg(WRITE_REG + RX_PW_P4, RX_PLOAD_WIDTH);//设置接收数据长度，本次设置为32字节
 347  0107 ae0007        	ldw	x,#7
 348  010a a635          	ld	a,#53
 349  010c 95            	ld	xh,a
 350  010d cd0000        	call	_SPI_Write_Reg
 352                     ; 86     SPI_Write_Reg(WRITE_REG + RX_PW_P5, RX_PLOAD_WIDTH);//设置接收数据长度，本次设置为32字节
 354  0110 ae0007        	ldw	x,#7
 355  0113 a636          	ld	a,#54
 356  0115 95            	ld	xh,a
 357  0116 cd0000        	call	_SPI_Write_Reg
 359                     ; 88     SPI_Write_Buf(WRITE_REG + TX_ADDR, TX_ADDRESS, TX_ADR_WIDTH);   //写发送地址
 361  0119 4b05          	push	#5
 362  011b ae0000        	ldw	x,#_TX_ADDRESS
 363  011e 89            	pushw	x
 364  011f a630          	ld	a,#48
 365  0121 cd0000        	call	_SPI_Write_Buf
 367  0124 5b03          	addw	sp,#3
 368                     ; 89     SPI_Write_Buf(WRITE_REG + RX_ADDR_P0, RX_ADDRESS, RX_ADR_WIDTH);//写接收端地址0(本地)
 370  0126 4b05          	push	#5
 371  0128 ae0005        	ldw	x,#_RX_ADDRESS
 372  012b 89            	pushw	x
 373  012c a62a          	ld	a,#42
 374  012e cd0000        	call	_SPI_Write_Buf
 376  0131 5b03          	addw	sp,#3
 377                     ; 90 		SPI_Write_Buf(WRITE_REG + RX_ADDR_P1, RX_ADDRESS1, RX_ADR_WIDTH);//写接收端地址1
 379  0133 4b05          	push	#5
 380  0135 ae000a        	ldw	x,#_RX_ADDRESS1
 381  0138 89            	pushw	x
 382  0139 a62b          	ld	a,#43
 383  013b cd0000        	call	_SPI_Write_Buf
 385  013e 5b03          	addw	sp,#3
 386                     ; 91     SPI_Write_Buf(WRITE_REG + RX_ADDR_P2, RX_ADDRESS2, 1);//写接收端地址2
 388  0140 4b01          	push	#1
 389  0142 ae000f        	ldw	x,#_RX_ADDRESS2
 390  0145 89            	pushw	x
 391  0146 a62c          	ld	a,#44
 392  0148 cd0000        	call	_SPI_Write_Buf
 394  014b 5b03          	addw	sp,#3
 395                     ; 92     SPI_Write_Buf(WRITE_REG + RX_ADDR_P3, RX_ADDRESS3, 1);//写接收端地址3
 397  014d 4b01          	push	#1
 398  014f ae0010        	ldw	x,#_RX_ADDRESS3
 399  0152 89            	pushw	x
 400  0153 a62d          	ld	a,#45
 401  0155 cd0000        	call	_SPI_Write_Buf
 403  0158 5b03          	addw	sp,#3
 404                     ; 93     SPI_Write_Buf(WRITE_REG + RX_ADDR_P4, RX_ADDRESS4, 1);//写接收端地址4
 406  015a 4b01          	push	#1
 407  015c ae0011        	ldw	x,#_RX_ADDRESS4
 408  015f 89            	pushw	x
 409  0160 a62e          	ld	a,#46
 410  0162 cd0000        	call	_SPI_Write_Buf
 412  0165 5b03          	addw	sp,#3
 413                     ; 94     SPI_Write_Buf(WRITE_REG + RX_ADDR_P5, RX_ADDRESS5, 1);//写接收端地址5
 415  0167 4b01          	push	#1
 416  0169 ae0012        	ldw	x,#_RX_ADDRESS5
 417  016c 89            	pushw	x
 418  016d a62f          	ld	a,#47
 419  016f cd0000        	call	_SPI_Write_Buf
 421  0172 5b03          	addw	sp,#3
 422                     ; 96     SPI_Write_Reg(WRITE_REG + CONFIG, 0x0e);   	 //IRQ收发完成中断响应，16位CRC，上电，发送模式
 424  0174 ae000e        	ldw	x,#14
 425  0177 a620          	ld	a,#32
 426  0179 95            	ld	xh,a
 427  017a cd0000        	call	_SPI_Write_Reg
 429                     ; 98     delay_ms(1);
 431  017d ae0001        	ldw	x,#1
 432  0180 cd0000        	call	_delay_ms
 434                     ; 99     NRF24L01_CE_SET;//置'1'
 436  0183 4b02          	push	#2
 437  0185 ae500f        	ldw	x,#20495
 438  0188 cd0000        	call	_GPIO_SetBits
 440  018b 84            	pop	a
 441                     ; 102     delay_ms(1);
 444  018c ae0001        	ldw	x,#1
 445  018f cd0000        	call	_delay_ms
 447                     ; 103 }
 450  0192 81            	ret
 476                     ; 112 void SwitchToRxMode(void)
 476                     ; 113 {
 477                     	switch	.text
 478  0193               _SwitchToRxMode:
 482                     ; 114     NRF24L01_CE_RESET;//置'0'
 484  0193 4b02          	push	#2
 485  0195 ae500f        	ldw	x,#20495
 486  0198 cd0000        	call	_GPIO_ResetBits
 488  019b 84            	pop	a
 489                     ; 116     SPI_Write_Reg(WRITE_REG + CONFIG, 0x0f);//上电，16位CRC，接收模式
 492  019c ae000f        	ldw	x,#15
 493  019f a620          	ld	a,#32
 494  01a1 95            	ld	xh,a
 495  01a2 cd0000        	call	_SPI_Write_Reg
 497                     ; 117     SPI_Write_Reg(WRITE_REG+STATUS, 0xff);  //清除所有中断标志
 499  01a5 ae00ff        	ldw	x,#255
 500  01a8 a627          	ld	a,#39
 501  01aa 95            	ld	xh,a
 502  01ab cd0000        	call	_SPI_Write_Reg
 504                     ; 119     NRF24L01_CE_SET;//置'1'
 506  01ae 4b02          	push	#2
 507  01b0 ae500f        	ldw	x,#20495
 508  01b3 cd0000        	call	_GPIO_SetBits
 510  01b6 84            	pop	a
 511                     ; 120 }
 515  01b7 81            	ret
 541                     ; 129 void SwitchToTxMode(void)
 541                     ; 130 {  
 542                     	switch	.text
 543  01b8               _SwitchToTxMode:
 547                     ; 131     NRF24L01_CE_RESET;//置'0'
 549  01b8 4b02          	push	#2
 550  01ba ae500f        	ldw	x,#20495
 551  01bd cd0000        	call	_GPIO_ResetBits
 553  01c0 84            	pop	a
 554                     ; 133     SPI_Write_Reg(WRITE_REG + CONFIG, 0x0e);//IRQ收发完成中断响应，16位CRC，上电，发送模式 
 557  01c1 ae000e        	ldw	x,#14
 558  01c4 a620          	ld	a,#32
 559  01c6 95            	ld	xh,a
 560  01c7 cd0000        	call	_SPI_Write_Reg
 562                     ; 134     SPI_Write_Reg(WRITE_REG+STATUS, 0xff);  //清除所有中断标志
 564  01ca ae00ff        	ldw	x,#255
 565  01cd a627          	ld	a,#39
 566  01cf 95            	ld	xh,a
 567  01d0 cd0000        	call	_SPI_Write_Reg
 569                     ; 136     NRF24L01_CE_SET;//置'1'
 571  01d3 4b02          	push	#2
 572  01d5 ae500f        	ldw	x,#20495
 573  01d8 cd0000        	call	_GPIO_SetBits
 575  01db 84            	pop	a
 576                     ; 137 }
 580  01dc 81            	ret
 637                     ; 146 unsigned char NRF24L01_SendPacket(unsigned char* buf,unsigned char len)
 637                     ; 147 {
 638                     	switch	.text
 639  01dd               _NRF24L01_SendPacket:
 641  01dd 89            	pushw	x
 642  01de 88            	push	a
 643       00000001      OFST:	set	1
 646                     ; 150     SPI_Write_Buf(WR_TX_PLOAD, buf, len);//写32字节数据
 648  01df 7b06          	ld	a,(OFST+5,sp)
 649  01e1 88            	push	a
 650  01e2 89            	pushw	x
 651  01e3 a6a0          	ld	a,#160
 652  01e5 cd0000        	call	_SPI_Write_Buf
 654  01e8 5b03          	addw	sp,#3
 655  01ea               L311:
 656                     ; 153         temp = SPI_Read_Reg(STATUS);//读状态寄存器
 658  01ea a607          	ld	a,#7
 659  01ec cd0000        	call	_SPI_Read_Reg
 661  01ef 6b01          	ld	(OFST+0,sp),a
 662                     ; 155     }while(!(temp & 0x70));//等待数据发送完成(检查接收数据中断、发送完成中断、重发次数溢出中断)
 664  01f1 7b01          	ld	a,(OFST+0,sp)
 665  01f3 a570          	bcp	a,#112
 666  01f5 27f3          	jreq	L311
 667                     ; 157     if(SPI_Read_Reg(STATUS) & STATUS_TX_DS)//检查发送完成中断是否为'1'
 669  01f7 a607          	ld	a,#7
 670  01f9 cd0000        	call	_SPI_Read_Reg
 672  01fc a520          	bcp	a,#32
 673  01fe 2714          	jreq	L121
 674                     ; 159         SPI_Write_Reg(WRITE_REG + STATUS, 0xff);//清除所有中断标志
 676  0200 ae00ff        	ldw	x,#255
 677  0203 a627          	ld	a,#39
 678  0205 95            	ld	xh,a
 679  0206 cd0000        	call	_SPI_Write_Reg
 681                     ; 160         SPI_Write_Reg(FLUSH_TX, 0x00);          //冲洗TX FIFO寄存器
 683  0209 5f            	clrw	x
 684  020a a6e1          	ld	a,#225
 685  020c 95            	ld	xh,a
 686  020d cd0000        	call	_SPI_Write_Reg
 688                     ; 161         return 1;
 690  0210 a601          	ld	a,#1
 692  0212 2011          	jra	L61
 693  0214               L121:
 694                     ; 165         SPI_Write_Reg(WRITE_REG + STATUS, 0xff);//清除所有中断标志
 696  0214 ae00ff        	ldw	x,#255
 697  0217 a627          	ld	a,#39
 698  0219 95            	ld	xh,a
 699  021a cd0000        	call	_SPI_Write_Reg
 701                     ; 166         SPI_Write_Reg(FLUSH_TX, 0x00);          //冲洗TX FIFO寄存器
 703  021d 5f            	clrw	x
 704  021e a6e1          	ld	a,#225
 705  0220 95            	ld	xh,a
 706  0221 cd0000        	call	_SPI_Write_Reg
 708                     ; 167         return 0;
 710  0224 4f            	clr	a
 712  0225               L61:
 714  0225 5b03          	addw	sp,#3
 715  0227 81            	ret
 763                     ; 178 unsigned char NRF24L01_ReceivePacket(unsigned char* buf)
 763                     ; 179 {   
 764                     	switch	.text
 765  0228               _NRF24L01_ReceivePacket:
 767  0228 89            	pushw	x
 768  0229 88            	push	a
 769       00000001      OFST:	set	1
 772                     ; 186 		temp = SPI_Read_Reg(STATUS);//读取状态寄存器
 774  022a a607          	ld	a,#7
 775  022c cd0000        	call	_SPI_Read_Reg
 777  022f 6b01          	ld	(OFST+0,sp),a
 778                     ; 188     if(temp & 0x40)//判断是否为接收数据中断
 780  0231 7b01          	ld	a,(OFST+0,sp)
 781  0233 a540          	bcp	a,#64
 782  0235 2720          	jreq	L741
 783                     ; 190         SPI_Read_Buf(RD_RX_PLOAD,buf, TX_PLOAD_WIDTH);//读32字节数据
 785  0237 4b07          	push	#7
 786  0239 1e03          	ldw	x,(OFST+2,sp)
 787  023b 89            	pushw	x
 788  023c a661          	ld	a,#97
 789  023e cd0000        	call	_SPI_Read_Buf
 791  0241 5b03          	addw	sp,#3
 792                     ; 191         SPI_Write_Reg(WRITE_REG + STATUS, 0xff);      //清除所有中断标志
 794  0243 ae00ff        	ldw	x,#255
 795  0246 a627          	ld	a,#39
 796  0248 95            	ld	xh,a
 797  0249 cd0000        	call	_SPI_Write_Reg
 799                     ; 192 				SPI_Write_Reg(FLUSH_RX, 0x00);                //冲洗RX FIFO寄存器
 801  024c 5f            	clrw	x
 802  024d a6e2          	ld	a,#226
 803  024f 95            	ld	xh,a
 804  0250 cd0000        	call	_SPI_Write_Reg
 806                     ; 193         return temp;
 808  0253 7b01          	ld	a,(OFST+0,sp)
 810  0255 2001          	jra	L22
 811  0257               L741:
 812                     ; 195     else return 0;
 814  0257 4f            	clr	a
 816  0258               L22:
 818  0258 5b03          	addw	sp,#3
 819  025a 81            	ret
 846                     ; 206 void Power_off(void)
 846                     ; 207 {
 847                     	switch	.text
 848  025b               _Power_off:
 852                     ; 208 		NRF24L01_CE_RESET;//置'0'
 854  025b 4b02          	push	#2
 855  025d ae500f        	ldw	x,#20495
 856  0260 cd0000        	call	_GPIO_ResetBits
 858  0263 84            	pop	a
 859                     ; 210     SPI_Write_Reg(WRITE_REG + CONFIG, 0x0d);//掉电
 862  0264 ae000d        	ldw	x,#13
 863  0267 a620          	ld	a,#32
 864  0269 95            	ld	xh,a
 865  026a cd0000        	call	_SPI_Write_Reg
 867                     ; 212 		NRF24L01_CE_SET;//置'1'
 869  026d 4b02          	push	#2
 870  026f ae500f        	ldw	x,#20495
 871  0272 cd0000        	call	_GPIO_SetBits
 873  0275 84            	pop	a
 874                     ; 214 		delay_ms(1);
 877  0276 ae0001        	ldw	x,#1
 878  0279 cd0000        	call	_delay_ms
 880                     ; 215 }
 883  027c 81            	ret
 968                     	xdef	_delay_ms
 969                     	xdef	_RX_ADDRESS5
 970                     	xdef	_RX_ADDRESS4
 971                     	xdef	_RX_ADDRESS3
 972                     	xdef	_RX_ADDRESS2
 973                     	xdef	_RX_ADDRESS1
 974                     	xdef	_RX_ADDRESS
 975                     	xdef	_TX_ADDRESS
 976                     	xref	_SPI_Read_Buf
 977                     	xref	_SPI_Write_Buf
 978                     	xref	_SPI_Read_Reg
 979                     	xref	_SPI_Write_Reg
 980                     	xref	_GPIO_ResetBits
 981                     	xref	_GPIO_SetBits
 982                     	xref	_GPIO_Init
 983                     	xdef	_SwitchToTxMode
 984                     	xdef	_SwitchToRxMode
 985                     	xdef	_Power_off
 986                     	xdef	_NRF24L01_ReceivePacket
 987                     	xdef	_NRF24L01_SendPacket
 988                     	xdef	_Init_NRF24L01
 989                     	xref.b	c_x
1008                     	xref	c_imul
1009                     	end
