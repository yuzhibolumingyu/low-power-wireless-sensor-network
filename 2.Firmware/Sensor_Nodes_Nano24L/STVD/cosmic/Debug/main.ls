   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  15                     	bsct
  16  0000               _ButtonPressed:
  17  0000 00            	dc.b	0
  18  0001               _Temp01:
  19  0001 0000          	dc.w	0
  20  0003               _Temp02:
  21  0003 0000          	dc.w	0
  22  0005               _Temp03:
  23  0005 0000          	dc.w	0
  24  0007               _Temp04:
  25  0007 0000          	dc.w	0
  26  0009               _Temp05:
  27  0009 0000          	dc.w	0
  28  000b               _Temp06:
  29  000b 0000          	dc.w	0
  30  000d               _Connect_Timer0:
  31  000d 0000          	dc.w	0
  32  000f               _Connect_Timer1:
  33  000f 0000          	dc.w	0
  34  0011               _Connect_Timer2:
  35  0011 0000          	dc.w	0
  36  0013               _Connect_Timer3:
  37  0013 0000          	dc.w	0
  38  0015               _Connect_Timer4:
  39  0015 0000          	dc.w	0
  40  0017               _Connect_Timer5:
  41  0017 0000          	dc.w	0
  42  0019               _rxData:
  43  0019 00            	dc.b	0
  44  001a 000000000000  	ds.b	6
 110                     ; 56 main()
 110                     ; 57 {
 112                     	switch	.text
 113  0000               _main:
 115  0000 5203          	subw	sp,#3
 116       00000003      OFST:	set	3
 119                     ; 59 	  u8 NRF24L01_status = 0;//NRF24L01状态变量
 121                     ; 62     CLK_Config();//系统时钟源配置
 123  0002 cd0332        	call	L3_CLK_Config
 125                     ; 63 		Delay(1600);//1ms
 127  0005 ae0640        	ldw	x,#1600
 128  0008 cd0320        	call	_Delay
 130                     ; 64 		RTC_Config();//RTC配置
 132  000b cd035c        	call	L5_RTC_Config
 134                     ; 65 		Delay(1600);//1ms
 136  000e ae0640        	ldw	x,#1600
 137  0011 cd0320        	call	_Delay
 139                     ; 66 		TIM4_Config();//TIM4配置
 141  0014 cd0379        	call	L7_TIM4_Config
 143                     ; 67 		Delay(1600);//1ms
 145  0017 ae0640        	ldw	x,#1600
 146  001a cd0320        	call	_Delay
 148                     ; 68 	  IO_Config();//IO口初始化
 150  001d cd0396        	call	L11_IO_Config
 152                     ; 69 		Delay(1600);//1ms
 154  0020 ae0640        	ldw	x,#1600
 155  0023 cd0320        	call	_Delay
 157                     ; 70 		Init_NRF24L01();//初始化NRF24L01
 159  0026 cd0000        	call	_Init_NRF24L01
 161                     ; 71 		Delay(1600);//1ms
 163  0029 ae0640        	ldw	x,#1600
 164  002c cd0320        	call	_Delay
 166                     ; 72 		LCD_Init_ST7567();//初始化ST7567液晶
 168  002f cd0000        	call	_LCD_Init_ST7567
 170                     ; 73 		Delay(1600);//1ms
 172  0032 ae0640        	ldw	x,#1600
 173  0035 cd0320        	call	_Delay
 175                     ; 75 		SwitchToRxMode( );//24L01切换到接收模式 
 177  0038 cd0000        	call	_SwitchToRxMode
 179                     ; 76     Delay(16000);//10ms
 181  003b ae3e80        	ldw	x,#16000
 182  003e cd0320        	call	_Delay
 184                     ; 77 		ST7567_BL_H;//打开背光
 186  0041 4b40          	push	#64
 187  0043 ae5000        	ldw	x,#20480
 188  0046 cd0000        	call	_GPIO_SetBits
 190  0049 84            	pop	a
 191                     ; 78 		Delay(16000);//10ms
 193  004a ae3e80        	ldw	x,#16000
 194  004d cd0320        	call	_Delay
 196                     ; 79 		LCD_Init_Display();//开机画面初始化;
 198  0050 cd0000        	call	_LCD_Init_Display
 200                     ; 80 		Delay(16000);//10ms
 202  0053 ae3e80        	ldw	x,#16000
 203  0056 cd0320        	call	_Delay
 205                     ; 82 		enableInterrupts();//开启全局中断
 208  0059 9a            rim
 210  005a               L73:
 211                     ; 86     NRF24L01_status = NRF24L01_ReceivePacket(rxData);//监视接收状态
 213  005a ae0019        	ldw	x,#_rxData
 214  005d cd0000        	call	_NRF24L01_ReceivePacket
 216  0060 6b03          	ld	(OFST+0,sp),a
 217                     ; 88     if(NRF24L01_status)
 219  0062 0d03          	tnz	(OFST+0,sp)
 220  0064 27f4          	jreq	L73
 221                     ; 90         NRF24L01_status &= 0x0E;//取接收数据通道号!!!
 223  0066 7b03          	ld	a,(OFST+0,sp)
 224  0068 a40e          	and	a,#14
 225  006a 6b03          	ld	(OFST+0,sp),a
 226                     ; 91         NRF24L01_status = NRF24L01_status >> 1;
 228  006c 0403          	srl	(OFST+0,sp)
 229                     ; 93         if (NRF24L01_status == 0)//rxData[0] == 0x01;//------------接收通道0处理
 231  006e 0d03          	tnz	(OFST+0,sp)
 232  0070 266c          	jrne	L54
 233                     ; 95             Connect_Timer0 = 0;//通道0无线接收检查计时器清零
 235  0072 5f            	clrw	x
 236  0073 bf0d          	ldw	_Connect_Timer0,x
 237                     ; 97             Temp01 = rxData[1];//数值转化
 239  0075 b61a          	ld	a,_rxData+1
 240  0077 5f            	clrw	x
 241  0078 97            	ld	xl,a
 242  0079 bf01          	ldw	_Temp01,x
 243                     ; 98             Temp01 = Temp01 << 8;
 245  007b b602          	ld	a,_Temp01+1
 246  007d b701          	ld	_Temp01,a
 247  007f 3f02          	clr	_Temp01+1
 248                     ; 99             Temp01 = Temp01 + rxData[2];
 250  0081 b61b          	ld	a,_rxData+2
 251  0083 5f            	clrw	x
 252  0084 97            	ld	xl,a
 253  0085 1f01          	ldw	(OFST-2,sp),x
 254  0087 be01          	ldw	x,_Temp01
 255  0089 72fb01        	addw	x,(OFST-2,sp)
 256  008c bf01          	ldw	_Temp01,x
 257                     ; 102             LCD_write_char16x8(2,48,(Temp01 % 10)+16);//小数部分
 259  008e be01          	ldw	x,_Temp01
 260  0090 90ae000a      	ldw	y,#10
 261  0094 65            	divw	x,y
 262  0095 51            	exgw	x,y
 263  0096 1c0010        	addw	x,#16
 264  0099 9f            	ld	a,xl
 265  009a 88            	push	a
 266  009b ae0030        	ldw	x,#48
 267  009e a602          	ld	a,#2
 268  00a0 95            	ld	xh,a
 269  00a1 cd0000        	call	_LCD_write_char16x8
 271  00a4 84            	pop	a
 272                     ; 103             Temp01 = Temp01 / 10;
 274  00a5 be01          	ldw	x,_Temp01
 275  00a7 90ae000a      	ldw	y,#10
 276  00ab 65            	divw	x,y
 277  00ac bf01          	ldw	_Temp01,x
 278                     ; 104             LCD_write_char16x8(2,32,(Temp01 % 10)+16);//整数个位
 280  00ae be01          	ldw	x,_Temp01
 281  00b0 90ae000a      	ldw	y,#10
 282  00b4 65            	divw	x,y
 283  00b5 51            	exgw	x,y
 284  00b6 1c0010        	addw	x,#16
 285  00b9 9f            	ld	a,xl
 286  00ba 88            	push	a
 287  00bb ae0020        	ldw	x,#32
 288  00be a602          	ld	a,#2
 289  00c0 95            	ld	xh,a
 290  00c1 cd0000        	call	_LCD_write_char16x8
 292  00c4 84            	pop	a
 293                     ; 105             LCD_write_char16x8(2,24,(Temp01 / 10)+16);//整数十位 
 295  00c5 be01          	ldw	x,_Temp01
 296  00c7 90ae000a      	ldw	y,#10
 297  00cb 65            	divw	x,y
 298  00cc 1c0010        	addw	x,#16
 299  00cf 9f            	ld	a,xl
 300  00d0 88            	push	a
 301  00d1 ae0018        	ldw	x,#24
 302  00d4 a602          	ld	a,#2
 303  00d6 95            	ld	xh,a
 304  00d7 cd0000        	call	_LCD_write_char16x8
 306  00da 84            	pop	a
 308  00db cc005a        	jra	L73
 309  00de               L54:
 310                     ; 107         else if(NRF24L01_status == 1)//rxData[0] == 0x02;//--------接收通道1处理
 312  00de 7b03          	ld	a,(OFST+0,sp)
 313  00e0 a101          	cp	a,#1
 314  00e2 266d          	jrne	L15
 315                     ; 109             Connect_Timer1 = 0;//通道1无线接收检查计时器清零
 317  00e4 5f            	clrw	x
 318  00e5 bf0f          	ldw	_Connect_Timer1,x
 319                     ; 111             Temp02 = rxData[1];//数值转化
 321  00e7 b61a          	ld	a,_rxData+1
 322  00e9 5f            	clrw	x
 323  00ea 97            	ld	xl,a
 324  00eb bf03          	ldw	_Temp02,x
 325                     ; 112             Temp02 = Temp02 << 8;
 327  00ed b604          	ld	a,_Temp02+1
 328  00ef b703          	ld	_Temp02,a
 329  00f1 3f04          	clr	_Temp02+1
 330                     ; 113             Temp02 = Temp02 + rxData[2];
 332  00f3 b61b          	ld	a,_rxData+2
 333  00f5 5f            	clrw	x
 334  00f6 97            	ld	xl,a
 335  00f7 1f01          	ldw	(OFST-2,sp),x
 336  00f9 be03          	ldw	x,_Temp02
 337  00fb 72fb01        	addw	x,(OFST-2,sp)
 338  00fe bf03          	ldw	_Temp02,x
 339                     ; 116             LCD_write_char16x8(4,48,(Temp02 % 10)+16);//小数部分
 341  0100 be03          	ldw	x,_Temp02
 342  0102 90ae000a      	ldw	y,#10
 343  0106 65            	divw	x,y
 344  0107 51            	exgw	x,y
 345  0108 1c0010        	addw	x,#16
 346  010b 9f            	ld	a,xl
 347  010c 88            	push	a
 348  010d ae0030        	ldw	x,#48
 349  0110 a604          	ld	a,#4
 350  0112 95            	ld	xh,a
 351  0113 cd0000        	call	_LCD_write_char16x8
 353  0116 84            	pop	a
 354                     ; 117             Temp02 = Temp02 / 10;
 356  0117 be03          	ldw	x,_Temp02
 357  0119 90ae000a      	ldw	y,#10
 358  011d 65            	divw	x,y
 359  011e bf03          	ldw	_Temp02,x
 360                     ; 118             LCD_write_char16x8(4,32,(Temp02 % 10)+16);//整数个位
 362  0120 be03          	ldw	x,_Temp02
 363  0122 90ae000a      	ldw	y,#10
 364  0126 65            	divw	x,y
 365  0127 51            	exgw	x,y
 366  0128 1c0010        	addw	x,#16
 367  012b 9f            	ld	a,xl
 368  012c 88            	push	a
 369  012d ae0020        	ldw	x,#32
 370  0130 a604          	ld	a,#4
 371  0132 95            	ld	xh,a
 372  0133 cd0000        	call	_LCD_write_char16x8
 374  0136 84            	pop	a
 375                     ; 119             LCD_write_char16x8(4,24,(Temp02 / 10)+16);//整数十位 
 377  0137 be03          	ldw	x,_Temp02
 378  0139 90ae000a      	ldw	y,#10
 379  013d 65            	divw	x,y
 380  013e 1c0010        	addw	x,#16
 381  0141 9f            	ld	a,xl
 382  0142 88            	push	a
 383  0143 ae0018        	ldw	x,#24
 384  0146 a604          	ld	a,#4
 385  0148 95            	ld	xh,a
 386  0149 cd0000        	call	_LCD_write_char16x8
 388  014c 84            	pop	a
 390  014d ac5a005a      	jpf	L73
 391  0151               L15:
 392                     ; 122         else if(NRF24L01_status == 2)//rxData[0] == 0x03;//--------接收通道2处理
 394  0151 7b03          	ld	a,(OFST+0,sp)
 395  0153 a102          	cp	a,#2
 396  0155 266d          	jrne	L55
 397                     ; 124             Connect_Timer2 = 0;//通道2无线接收检查计时器清零
 399  0157 5f            	clrw	x
 400  0158 bf11          	ldw	_Connect_Timer2,x
 401                     ; 126             Temp03 = rxData[1];//数值转化
 403  015a b61a          	ld	a,_rxData+1
 404  015c 5f            	clrw	x
 405  015d 97            	ld	xl,a
 406  015e bf05          	ldw	_Temp03,x
 407                     ; 127             Temp03 = Temp03 << 8;
 409  0160 b606          	ld	a,_Temp03+1
 410  0162 b705          	ld	_Temp03,a
 411  0164 3f06          	clr	_Temp03+1
 412                     ; 128             Temp03 = Temp03 + rxData[2]; 
 414  0166 b61b          	ld	a,_rxData+2
 415  0168 5f            	clrw	x
 416  0169 97            	ld	xl,a
 417  016a 1f01          	ldw	(OFST-2,sp),x
 418  016c be05          	ldw	x,_Temp03
 419  016e 72fb01        	addw	x,(OFST-2,sp)
 420  0171 bf05          	ldw	_Temp03,x
 421                     ; 131             LCD_write_char16x8(6,48,(Temp03 % 10)+16);//小数部分
 423  0173 be05          	ldw	x,_Temp03
 424  0175 90ae000a      	ldw	y,#10
 425  0179 65            	divw	x,y
 426  017a 51            	exgw	x,y
 427  017b 1c0010        	addw	x,#16
 428  017e 9f            	ld	a,xl
 429  017f 88            	push	a
 430  0180 ae0030        	ldw	x,#48
 431  0183 a606          	ld	a,#6
 432  0185 95            	ld	xh,a
 433  0186 cd0000        	call	_LCD_write_char16x8
 435  0189 84            	pop	a
 436                     ; 132             Temp03 = Temp03 / 10;
 438  018a be05          	ldw	x,_Temp03
 439  018c 90ae000a      	ldw	y,#10
 440  0190 65            	divw	x,y
 441  0191 bf05          	ldw	_Temp03,x
 442                     ; 133             LCD_write_char16x8(6,32,(Temp03 % 10)+16);//整数个位
 444  0193 be05          	ldw	x,_Temp03
 445  0195 90ae000a      	ldw	y,#10
 446  0199 65            	divw	x,y
 447  019a 51            	exgw	x,y
 448  019b 1c0010        	addw	x,#16
 449  019e 9f            	ld	a,xl
 450  019f 88            	push	a
 451  01a0 ae0020        	ldw	x,#32
 452  01a3 a606          	ld	a,#6
 453  01a5 95            	ld	xh,a
 454  01a6 cd0000        	call	_LCD_write_char16x8
 456  01a9 84            	pop	a
 457                     ; 134             LCD_write_char16x8(6,24,(Temp03 / 10)+16);//整数十位  
 459  01aa be05          	ldw	x,_Temp03
 460  01ac 90ae000a      	ldw	y,#10
 461  01b0 65            	divw	x,y
 462  01b1 1c0010        	addw	x,#16
 463  01b4 9f            	ld	a,xl
 464  01b5 88            	push	a
 465  01b6 ae0018        	ldw	x,#24
 466  01b9 a606          	ld	a,#6
 467  01bb 95            	ld	xh,a
 468  01bc cd0000        	call	_LCD_write_char16x8
 470  01bf 84            	pop	a
 472  01c0 ac5a005a      	jpf	L73
 473  01c4               L55:
 474                     ; 137 				else if(NRF24L01_status == 3)//rxData[0] == 0x04;//--------接收通道3处理
 476  01c4 7b03          	ld	a,(OFST+0,sp)
 477  01c6 a103          	cp	a,#3
 478  01c8 266d          	jrne	L16
 479                     ; 139             Connect_Timer3 = 0;//通道3无线接收检查计时器清零
 481  01ca 5f            	clrw	x
 482  01cb bf13          	ldw	_Connect_Timer3,x
 483                     ; 141             Temp04 = rxData[1];//数值转化
 485  01cd b61a          	ld	a,_rxData+1
 486  01cf 5f            	clrw	x
 487  01d0 97            	ld	xl,a
 488  01d1 bf07          	ldw	_Temp04,x
 489                     ; 142             Temp04 = Temp04 << 8;
 491  01d3 b608          	ld	a,_Temp04+1
 492  01d5 b707          	ld	_Temp04,a
 493  01d7 3f08          	clr	_Temp04+1
 494                     ; 143             Temp04 = Temp04 + rxData[2]; 
 496  01d9 b61b          	ld	a,_rxData+2
 497  01db 5f            	clrw	x
 498  01dc 97            	ld	xl,a
 499  01dd 1f01          	ldw	(OFST-2,sp),x
 500  01df be07          	ldw	x,_Temp04
 501  01e1 72fb01        	addw	x,(OFST-2,sp)
 502  01e4 bf07          	ldw	_Temp04,x
 503                     ; 146             LCD_write_char16x8(2,112,(Temp04 % 10)+16);//小数部分
 505  01e6 be07          	ldw	x,_Temp04
 506  01e8 90ae000a      	ldw	y,#10
 507  01ec 65            	divw	x,y
 508  01ed 51            	exgw	x,y
 509  01ee 1c0010        	addw	x,#16
 510  01f1 9f            	ld	a,xl
 511  01f2 88            	push	a
 512  01f3 ae0070        	ldw	x,#112
 513  01f6 a602          	ld	a,#2
 514  01f8 95            	ld	xh,a
 515  01f9 cd0000        	call	_LCD_write_char16x8
 517  01fc 84            	pop	a
 518                     ; 147             Temp04 = Temp04 / 10;
 520  01fd be07          	ldw	x,_Temp04
 521  01ff 90ae000a      	ldw	y,#10
 522  0203 65            	divw	x,y
 523  0204 bf07          	ldw	_Temp04,x
 524                     ; 148             LCD_write_char16x8(2,96,(Temp04 % 10)+16);//整数个位
 526  0206 be07          	ldw	x,_Temp04
 527  0208 90ae000a      	ldw	y,#10
 528  020c 65            	divw	x,y
 529  020d 51            	exgw	x,y
 530  020e 1c0010        	addw	x,#16
 531  0211 9f            	ld	a,xl
 532  0212 88            	push	a
 533  0213 ae0060        	ldw	x,#96
 534  0216 a602          	ld	a,#2
 535  0218 95            	ld	xh,a
 536  0219 cd0000        	call	_LCD_write_char16x8
 538  021c 84            	pop	a
 539                     ; 149             LCD_write_char16x8(2,88,(Temp04 / 10)+16);//整数十位  
 541  021d be07          	ldw	x,_Temp04
 542  021f 90ae000a      	ldw	y,#10
 543  0223 65            	divw	x,y
 544  0224 1c0010        	addw	x,#16
 545  0227 9f            	ld	a,xl
 546  0228 88            	push	a
 547  0229 ae0058        	ldw	x,#88
 548  022c a602          	ld	a,#2
 549  022e 95            	ld	xh,a
 550  022f cd0000        	call	_LCD_write_char16x8
 552  0232 84            	pop	a
 554  0233 ac5a005a      	jpf	L73
 555  0237               L16:
 556                     ; 152 				else if(NRF24L01_status == 4)//rxData[0] == 0x05;//--------接收通道4处理
 558  0237 7b03          	ld	a,(OFST+0,sp)
 559  0239 a104          	cp	a,#4
 560  023b 266d          	jrne	L56
 561                     ; 154             Connect_Timer4 = 0;//通道4无线接收检查计时器清零
 563  023d 5f            	clrw	x
 564  023e bf15          	ldw	_Connect_Timer4,x
 565                     ; 156             Temp05 = rxData[1];//数值转化
 567  0240 b61a          	ld	a,_rxData+1
 568  0242 5f            	clrw	x
 569  0243 97            	ld	xl,a
 570  0244 bf09          	ldw	_Temp05,x
 571                     ; 157             Temp05 = Temp05 << 8;
 573  0246 b60a          	ld	a,_Temp05+1
 574  0248 b709          	ld	_Temp05,a
 575  024a 3f0a          	clr	_Temp05+1
 576                     ; 158             Temp05 = Temp05 + rxData[2]; 
 578  024c b61b          	ld	a,_rxData+2
 579  024e 5f            	clrw	x
 580  024f 97            	ld	xl,a
 581  0250 1f01          	ldw	(OFST-2,sp),x
 582  0252 be09          	ldw	x,_Temp05
 583  0254 72fb01        	addw	x,(OFST-2,sp)
 584  0257 bf09          	ldw	_Temp05,x
 585                     ; 161             LCD_write_char16x8(4,112,(Temp05 % 10)+16);//小数部分
 587  0259 be09          	ldw	x,_Temp05
 588  025b 90ae000a      	ldw	y,#10
 589  025f 65            	divw	x,y
 590  0260 51            	exgw	x,y
 591  0261 1c0010        	addw	x,#16
 592  0264 9f            	ld	a,xl
 593  0265 88            	push	a
 594  0266 ae0070        	ldw	x,#112
 595  0269 a604          	ld	a,#4
 596  026b 95            	ld	xh,a
 597  026c cd0000        	call	_LCD_write_char16x8
 599  026f 84            	pop	a
 600                     ; 162             Temp05 = Temp05 / 10;
 602  0270 be09          	ldw	x,_Temp05
 603  0272 90ae000a      	ldw	y,#10
 604  0276 65            	divw	x,y
 605  0277 bf09          	ldw	_Temp05,x
 606                     ; 163             LCD_write_char16x8(4,96,(Temp05 % 10)+16);//整数个位
 608  0279 be09          	ldw	x,_Temp05
 609  027b 90ae000a      	ldw	y,#10
 610  027f 65            	divw	x,y
 611  0280 51            	exgw	x,y
 612  0281 1c0010        	addw	x,#16
 613  0284 9f            	ld	a,xl
 614  0285 88            	push	a
 615  0286 ae0060        	ldw	x,#96
 616  0289 a604          	ld	a,#4
 617  028b 95            	ld	xh,a
 618  028c cd0000        	call	_LCD_write_char16x8
 620  028f 84            	pop	a
 621                     ; 164             LCD_write_char16x8(4,88,(Temp05 / 10)+16);//整数十位  
 623  0290 be09          	ldw	x,_Temp05
 624  0292 90ae000a      	ldw	y,#10
 625  0296 65            	divw	x,y
 626  0297 1c0010        	addw	x,#16
 627  029a 9f            	ld	a,xl
 628  029b 88            	push	a
 629  029c ae0058        	ldw	x,#88
 630  029f a604          	ld	a,#4
 631  02a1 95            	ld	xh,a
 632  02a2 cd0000        	call	_LCD_write_char16x8
 634  02a5 84            	pop	a
 636  02a6 ac5a005a      	jpf	L73
 637  02aa               L56:
 638                     ; 167 				else if(NRF24L01_status == 5)//rxData[0] == 0x06;//--------接收通道5处理
 640  02aa 7b03          	ld	a,(OFST+0,sp)
 641  02ac a105          	cp	a,#5
 642  02ae 2703          	jreq	L6
 643  02b0 cc005a        	jp	L73
 644  02b3               L6:
 645                     ; 169             Connect_Timer5 = 0;//通道5无线接收检查计时器清零
 647  02b3 5f            	clrw	x
 648  02b4 bf17          	ldw	_Connect_Timer5,x
 649                     ; 171             Temp06 = rxData[1];//数值转化
 651  02b6 b61a          	ld	a,_rxData+1
 652  02b8 5f            	clrw	x
 653  02b9 97            	ld	xl,a
 654  02ba bf0b          	ldw	_Temp06,x
 655                     ; 172             Temp06 = Temp06 << 8;
 657  02bc b60c          	ld	a,_Temp06+1
 658  02be b70b          	ld	_Temp06,a
 659  02c0 3f0c          	clr	_Temp06+1
 660                     ; 173             Temp06 = Temp06 + rxData[2]; 
 662  02c2 b61b          	ld	a,_rxData+2
 663  02c4 5f            	clrw	x
 664  02c5 97            	ld	xl,a
 665  02c6 1f01          	ldw	(OFST-2,sp),x
 666  02c8 be0b          	ldw	x,_Temp06
 667  02ca 72fb01        	addw	x,(OFST-2,sp)
 668  02cd bf0b          	ldw	_Temp06,x
 669                     ; 176             LCD_write_char16x8(6,112,(Temp06 % 10)+16);//小数部分
 671  02cf be0b          	ldw	x,_Temp06
 672  02d1 90ae000a      	ldw	y,#10
 673  02d5 65            	divw	x,y
 674  02d6 51            	exgw	x,y
 675  02d7 1c0010        	addw	x,#16
 676  02da 9f            	ld	a,xl
 677  02db 88            	push	a
 678  02dc ae0070        	ldw	x,#112
 679  02df a606          	ld	a,#6
 680  02e1 95            	ld	xh,a
 681  02e2 cd0000        	call	_LCD_write_char16x8
 683  02e5 84            	pop	a
 684                     ; 177             Temp06 = Temp06 / 10;
 686  02e6 be0b          	ldw	x,_Temp06
 687  02e8 90ae000a      	ldw	y,#10
 688  02ec 65            	divw	x,y
 689  02ed bf0b          	ldw	_Temp06,x
 690                     ; 178             LCD_write_char16x8(6,96,(Temp06 % 10)+16);//整数个位
 692  02ef be0b          	ldw	x,_Temp06
 693  02f1 90ae000a      	ldw	y,#10
 694  02f5 65            	divw	x,y
 695  02f6 51            	exgw	x,y
 696  02f7 1c0010        	addw	x,#16
 697  02fa 9f            	ld	a,xl
 698  02fb 88            	push	a
 699  02fc ae0060        	ldw	x,#96
 700  02ff a606          	ld	a,#6
 701  0301 95            	ld	xh,a
 702  0302 cd0000        	call	_LCD_write_char16x8
 704  0305 84            	pop	a
 705                     ; 179             LCD_write_char16x8(6,88,(Temp06 / 10)+16);//整数十位  
 707  0306 be0b          	ldw	x,_Temp06
 708  0308 90ae000a      	ldw	y,#10
 709  030c 65            	divw	x,y
 710  030d 1c0010        	addw	x,#16
 711  0310 9f            	ld	a,xl
 712  0311 88            	push	a
 713  0312 ae0058        	ldw	x,#88
 714  0315 a606          	ld	a,#6
 715  0317 95            	ld	xh,a
 716  0318 cd0000        	call	_LCD_write_char16x8
 718  031b 84            	pop	a
 719  031c ac5a005a      	jpf	L73
 753                     ; 202 void Delay(u16 nCount)
 753                     ; 203 {//延时0.629us -- 16MHz
 754                     	switch	.text
 755  0320               _Delay:
 757  0320 89            	pushw	x
 758       00000000      OFST:	set	0
 761  0321               L311:
 762                     ; 204     while(nCount--);
 764  0321 1e01          	ldw	x,(OFST+1,sp)
 765  0323 1d0001        	subw	x,#1
 766  0326 1f01          	ldw	(OFST+1,sp),x
 767  0328 1c0001        	addw	x,#1
 768  032b a30000        	cpw	x,#0
 769  032e 26f1          	jrne	L311
 770                     ; 205 }
 773  0330 85            	popw	x
 774  0331 81            	ret
 802                     ; 215 static void CLK_Config(void)
 802                     ; 216 {
 803                     	switch	.text
 804  0332               L3_CLK_Config:
 808                     ; 218 		CLK_SYSCLKSourceSwitchCmd(ENABLE);
 810  0332 a601          	ld	a,#1
 811  0334 cd0000        	call	_CLK_SYSCLKSourceSwitchCmd
 813                     ; 220 		CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);//HSI = 16M/Hz
 815  0337 a601          	ld	a,#1
 816  0339 cd0000        	call	_CLK_SYSCLKSourceConfig
 818                     ; 222     CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);//分频只会影响系统时钟源
 820  033c 4f            	clr	a
 821  033d cd0000        	call	_CLK_SYSCLKDivConfig
 823                     ; 224     CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_64);
 825  0340 ae00c0        	ldw	x,#192
 826  0343 a604          	ld	a,#4
 827  0345 95            	ld	xh,a
 828  0346 cd0000        	call	_CLK_RTCClockConfig
 830                     ; 226     CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
 832  0349 ae0001        	ldw	x,#1
 833  034c a612          	ld	a,#18
 834  034e 95            	ld	xh,a
 835  034f cd0000        	call	_CLK_PeripheralClockConfig
 837                     ; 228     CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
 839  0352 ae0001        	ldw	x,#1
 840  0355 a602          	ld	a,#2
 841  0357 95            	ld	xh,a
 842  0358 cd0000        	call	_CLK_PeripheralClockConfig
 844                     ; 229 }
 847  035b 81            	ret
 874                     ; 238 static void RTC_Config(void)
 874                     ; 239 {	
 875                     	switch	.text
 876  035c               L5_RTC_Config:
 880                     ; 241   RTC_WakeUpCmd(DISABLE);//配置唤醒单位前,必须先禁用才能设置!!!
 882  035c 4f            	clr	a
 883  035d cd0000        	call	_RTC_WakeUpCmd
 885                     ; 249   RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);	
 887  0360 4f            	clr	a
 888  0361 cd0000        	call	_RTC_WakeUpClockConfig
 890                     ; 251 	RTC_SetWakeUpCounter(37);
 892  0364 ae0025        	ldw	x,#37
 893  0367 cd0000        	call	_RTC_SetWakeUpCounter
 895                     ; 253   RTC_ITConfig(RTC_IT_WUT, ENABLE);
 897  036a 4b01          	push	#1
 898  036c ae0040        	ldw	x,#64
 899  036f cd0000        	call	_RTC_ITConfig
 901  0372 84            	pop	a
 902                     ; 256 	RTC_WakeUpCmd(ENABLE);
 904  0373 a601          	ld	a,#1
 905  0375 cd0000        	call	_RTC_WakeUpCmd
 907                     ; 257 }
 910  0378 81            	ret
 937                     ; 266 static void TIM4_Config(void)
 937                     ; 267 {
 938                     	switch	.text
 939  0379               L7_TIM4_Config:
 943                     ; 278   TIM4_TimeBaseInit(TIM4_Prescaler_32768, 244);//时基0.5s,0.5s溢出中断
 945  0379 ae00f4        	ldw	x,#244
 946  037c a60f          	ld	a,#15
 947  037e 95            	ld	xh,a
 948  037f cd0000        	call	_TIM4_TimeBaseInit
 950                     ; 280   TIM4_ClearFlag(TIM4_FLAG_Update);
 952  0382 a601          	ld	a,#1
 953  0384 cd0000        	call	_TIM4_ClearFlag
 955                     ; 282   TIM4_ITConfig(TIM4_IT_Update, ENABLE);
 957  0387 ae0001        	ldw	x,#1
 958  038a a601          	ld	a,#1
 959  038c 95            	ld	xh,a
 960  038d cd0000        	call	_TIM4_ITConfig
 962                     ; 285   TIM4_Cmd(ENABLE);
 964  0390 a601          	ld	a,#1
 965  0392 cd0000        	call	_TIM4_Cmd
 967                     ; 286 }
 970  0395 81            	ret
 995                     ; 295 static void IO_Config(void)
 995                     ; 296 {
 996                     	switch	.text
 997  0396               L11_IO_Config:
1001                     ; 297     GPIO_DeInit(GPIOB);//重新初始化GPIO外设寄存器为复位默认值，慎用！
1003  0396 ae5005        	ldw	x,#20485
1004  0399 cd0000        	call	_GPIO_DeInit
1006                     ; 298 	  GPIO_Init(GPIOB, GPIO_Pin_3, GPIO_Mode_Out_PP_Low_Fast);//LED,PB.3
1008  039c 4be0          	push	#224
1009  039e 4b08          	push	#8
1010  03a0 ae5005        	ldw	x,#20485
1011  03a3 cd0000        	call	_GPIO_Init
1013  03a6 85            	popw	x
1014                     ; 299 		GPIO_DeInit(GPIOA);//重新初始化GPIO外设寄存器为复位默认值，慎用！
1016  03a7 ae5000        	ldw	x,#20480
1017  03aa cd0000        	call	_GPIO_DeInit
1019                     ; 300 		GPIO_DeInit(GPIOD);//重新初始化GPIO外设寄存器为复位默认值，慎用！
1021  03ad ae500f        	ldw	x,#20495
1022  03b0 cd0000        	call	_GPIO_DeInit
1024                     ; 301 }
1027  03b3 81            	ret
1190                     	xdef	_main
1191                     	xdef	_Delay
1192                     	xdef	_rxData
1193                     	xdef	_Connect_Timer5
1194                     	xdef	_Connect_Timer4
1195                     	xdef	_Connect_Timer3
1196                     	xdef	_Connect_Timer2
1197                     	xdef	_Connect_Timer1
1198                     	xdef	_Connect_Timer0
1199                     	xdef	_Temp06
1200                     	xdef	_Temp05
1201                     	xdef	_Temp04
1202                     	xdef	_Temp03
1203                     	xdef	_Temp02
1204                     	xdef	_Temp01
1205                     	xdef	_ButtonPressed
1206                     	xref	_LCD_Init_Display
1207                     	xref	_LCD_write_char16x8
1208                     	xref	_LCD_Init_ST7567
1209                     	xref	_SwitchToRxMode
1210                     	xref	_NRF24L01_ReceivePacket
1211                     	xref	_Init_NRF24L01
1212                     	xref	_TIM4_ClearFlag
1213                     	xref	_TIM4_ITConfig
1214                     	xref	_TIM4_Cmd
1215                     	xref	_TIM4_TimeBaseInit
1216                     	xref	_RTC_ITConfig
1217                     	xref	_RTC_WakeUpCmd
1218                     	xref	_RTC_SetWakeUpCounter
1219                     	xref	_RTC_WakeUpClockConfig
1220                     	xref	_GPIO_SetBits
1221                     	xref	_GPIO_Init
1222                     	xref	_GPIO_DeInit
1223                     	xref	_CLK_PeripheralClockConfig
1224                     	xref	_CLK_RTCClockConfig
1225                     	xref	_CLK_SYSCLKSourceSwitchCmd
1226                     	xref	_CLK_SYSCLKDivConfig
1227                     	xref	_CLK_SYSCLKSourceConfig
1246                     	end
