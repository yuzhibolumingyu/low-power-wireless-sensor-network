# 固件编译说明

### 注意修改的地方
- nRF24L01的地址设置：
```c
    //接收机地址设置：文件路径[ \2.Firmware\Sensor_Nodes_Nano24L\STVD\src\nrf24l01.c ]，发送地址用不到，所以不用改
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0x34,0x43,0x10,0x10,0x01};//发送地址
    unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0x34,0x43,0x10,0x10,0x01};//通道0接收地址
    unsigned char RX_ADDRESS1[RX_ADR_WIDTH]= {0xB0,0x43,0x10,0x10,0x01};//通道1接收地址
    unsigned char RX_ADDRESS2[1]= {0xB1};//通道2接收地址，实际地址{0xB1,0x43,0x10,0x10,0x01}
    unsigned char RX_ADDRESS3[1]= {0xB2};//通道3接收地址，实际地址{0xB2,0x43,0x10,0x10,0x01}
    unsigned char RX_ADDRESS4[1]= {0xB3};//通道4接收地址，实际地址{0xB3,0x43,0x10,0x10,0x01}
    unsigned char RX_ADDRESS5[1]= {0xB4};//通道5接收地址，实际地址{0xB4,0x43,0x10,0x10,0x01}

	//发送节点地址设置：文件路径[ \2.Firmware\Sensor_NRF24L01\STVD\src\nrf24l01.c ]
	//发送节点1，通道0是芯片Enhanced ShockBurst TM模式需要，不用计较
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0x34,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0x34,0x43,0x10,0x10,0x01};//通道0接收地址
	//发送节点2
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB0,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB0,0x43,0x10,0x10,0x01};//通道0接收地址
	//发送节点3
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB1,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB1,0x43,0x10,0x10,0x01};//通道0接收地址
	//发送节点4
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB2,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB2,0x43,0x10,0x10,0x01};//通道0接收地址
	//发送节点5
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB3,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB3,0x43,0x10,0x10,0x01};//通道0接收地址
	//发送节点6
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB4,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB4,0x43,0x10,0x10,0x01};//通道0接收地址
```
- 休眠时间修改：
```c
	//Sensor_NRF24L01设置：文件路径[ \2.Firmware\Sensor_NRF24L01\STVD\src\main.c ]
	static void AWU_Config(void)
	{
	    /* Initialization of AWU */
	    /* LSI calibration for accurate auto wake up time base*/
	    AWU_LSICalibrationConfig(LSIMeasurment());
	     
	    /* The delay corresponds to the time we will stay in Halt mode */
	    AWU_Init(AWU_Timebase_30s);
	}
	
	//Sensor_NRF24L01_LCD设置：文件路径[ \2.Firmware\Sensor_NRF24L01_LCD\STVD\src\main.c ]
	static void RTC_Config(void)
	{
		  /* Enable RTC clock */
	    CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
		 
		  /* Disable WakeUp */
	    RTC_WakeUpCmd(DISABLE);//配置唤醒单位前,必须先禁用才能设置!!!
	   
	    /* Configures the RTC */
	    //RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);
	    RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
		  
	    /* RTC will wake-up from halt every 120s */	
		  RTC_SetWakeUpCounter(60);
		  /* Enable wake up unit Interrupt */
	    RTC_ITConfig(RTC_IT_WUT, ENABLE);
		 
		  /* Enable WakeUp */
		  RTC_WakeUpCmd(ENABLE);
	}	
```

- 开机画面：文件路径[ \2.Firmware\Sensor_Nodes_Nano24L\STVD\src\pixel_bmp.h ]
```c
	//取模设置.jpg
```

### 扩展
- nRF24L01芯片支持点对多点通信地址控制，可同时设置六路接收通道地址，可有选择性的打开接收通道
- 通信原理：一方接收地址和一方发射地址设置相同，双方再分别切换成接收模式和发送模式，才能收发通信
- NRF2401的所有配置工作都是通过SPI完成，共有30字节的配置字
- 初始配置完以后，只需改变最低一个字节的内容，即可实现接收模式和发送模式之间的切换
- 当CSN为低时，SPI接口开始等待一条指令，任何一条新指令均由CSN的由高到低的转换开始
- 详细配置参考代码即可*[ \2.Firmware\Sensor_NRF24L01\STVD\src\nrf24l01.c ]*
