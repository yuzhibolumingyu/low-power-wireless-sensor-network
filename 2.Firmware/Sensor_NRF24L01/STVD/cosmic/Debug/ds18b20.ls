   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  54                     ; 12 void _delay_us(u16 us)
  54                     ; 13 {//大概1.258us
  56                     	switch	.text
  57  0000               __delay_us:
  59  0000 89            	pushw	x
  60       00000000      OFST:	set	0
  63                     ; 14 	us *= 2;//根据晶振该系数
  65  0001 0802          	sll	(OFST+2,sp)
  66  0003 0901          	rlc	(OFST+1,sp)
  68  0005               L13:
  69                     ; 15 	while(us--);
  71  0005 1e01          	ldw	x,(OFST+1,sp)
  72  0007 1d0001        	subw	x,#1
  73  000a 1f01          	ldw	(OFST+1,sp),x
  74  000c 1c0001        	addw	x,#1
  75  000f a30000        	cpw	x,#0
  76  0012 26f1          	jrne	L13
  77                     ; 16 }
  80  0014 85            	popw	x
  81  0015 81            	ret
 118                     ; 28 unsigned char DS18B20_Init(void)
 118                     ; 29 {
 119                     	switch	.text
 120  0016               _DS18B20_Init:
 122  0016 88            	push	a
 123       00000001      OFST:	set	1
 126                     ; 30 		unsigned char x = 0;
 128                     ; 32 		DS18B20_DQ_OUT_L;
 130  0017 4bc0          	push	#192
 131  0019 4b08          	push	#8
 132  001b ae5000        	ldw	x,#20480
 133  001e cd0000        	call	_GPIO_Init
 135  0021 85            	popw	x
 136                     ; 33 		_delay_us(480);//复位脉冲
 138  0022 ae01e0        	ldw	x,#480
 139  0025 add9          	call	__delay_us
 141                     ; 34 		DS18B20_DQ_IN;
 143  0027 4b40          	push	#64
 144  0029 4b08          	push	#8
 145  002b ae5000        	ldw	x,#20480
 146  002e cd0000        	call	_GPIO_Init
 148  0031 85            	popw	x
 149                     ; 35 		_delay_us(45);//等待存在脉冲出现，注意非零才可以检测
 151  0032 ae002d        	ldw	x,#45
 152  0035 adc9          	call	__delay_us
 155  0037               L55:
 156                     ; 36 		while(DS18B20_DQ_PIN != 0);//读引脚
 158  0037 4b08          	push	#8
 159  0039 ae5000        	ldw	x,#20480
 160  003c cd0000        	call	_GPIO_ReadInputDataBit
 162  003f 5b01          	addw	sp,#1
 163  0041 4d            	tnz	a
 164  0042 26f3          	jrne	L55
 165                     ; 37 		x = 1;
 167  0044 a601          	ld	a,#1
 168  0046 6b01          	ld	(OFST+0,sp),a
 169                     ; 38 		_delay_us(120);
 171  0048 ae0078        	ldw	x,#120
 172  004b adb3          	call	__delay_us
 174                     ; 40 		return x;
 176  004d 7b01          	ld	a,(OFST+0,sp)
 179  004f 5b01          	addw	sp,#1
 180  0051 81            	ret
 226                     ; 50 void DS18B20_WriteByte(unsigned char _data)
 226                     ; 51 {
 227                     	switch	.text
 228  0052               _DS18B20_WriteByte:
 230  0052 88            	push	a
 231  0053 88            	push	a
 232       00000001      OFST:	set	1
 235                     ; 52 		unsigned char i = 0;
 237                     ; 53 		for (i = 0; i < 8; i++)
 239  0054 0f01          	clr	(OFST+0,sp)
 240  0056               L301:
 241                     ; 55 				DS18B20_DQ_OUT_L;
 243  0056 4bc0          	push	#192
 244  0058 4b08          	push	#8
 245  005a ae5000        	ldw	x,#20480
 246  005d cd0000        	call	_GPIO_Init
 248  0060 85            	popw	x
 249                     ; 56 				_delay_us(10);
 251  0061 ae000a        	ldw	x,#10
 252  0064 ad9a          	call	__delay_us
 254                     ; 57 				if (_data & 0x01)
 256  0066 7b02          	ld	a,(OFST+1,sp)
 257  0068 a501          	bcp	a,#1
 258  006a 2709          	jreq	L111
 259                     ; 59 					  DS18B20_DQ_OUT_H;
 261  006c 4b08          	push	#8
 262  006e ae5000        	ldw	x,#20480
 263  0071 cd0000        	call	_GPIO_SetBits
 265  0074 84            	pop	a
 266  0075               L111:
 267                     ; 61 				_delay_us(45);
 269  0075 ae002d        	ldw	x,#45
 270  0078 ad86          	call	__delay_us
 272                     ; 62 				_data >>= 1;
 274  007a 0402          	srl	(OFST+1,sp)
 275                     ; 63 				DS18B20_DQ_IN;
 277  007c 4b40          	push	#64
 278  007e 4b08          	push	#8
 279  0080 ae5000        	ldw	x,#20480
 280  0083 cd0000        	call	_GPIO_Init
 282  0086 85            	popw	x
 283                     ; 64 				_delay_us(5);
 285  0087 ae0005        	ldw	x,#5
 286  008a cd0000        	call	__delay_us
 288                     ; 53 		for (i = 0; i < 8; i++)
 290  008d 0c01          	inc	(OFST+0,sp)
 293  008f 7b01          	ld	a,(OFST+0,sp)
 294  0091 a108          	cp	a,#8
 295  0093 25c1          	jrult	L301
 296                     ; 66 }
 299  0095 85            	popw	x
 300  0096 81            	ret
 346                     ; 75 unsigned char DS18B20_ReadByte(void)
 346                     ; 76 {
 347                     	switch	.text
 348  0097               _DS18B20_ReadByte:
 350  0097 89            	pushw	x
 351       00000002      OFST:	set	2
 354                     ; 77 	  unsigned char i = 0, _data = 0;
 358  0098 0f02          	clr	(OFST+0,sp)
 359                     ; 78 		for (i = 0; i < 8; i++)
 361  009a 0f01          	clr	(OFST-1,sp)
 362  009c               L531:
 363                     ; 80 				DS18B20_DQ_OUT_L;
 365  009c 4bc0          	push	#192
 366  009e 4b08          	push	#8
 367  00a0 ae5000        	ldw	x,#20480
 368  00a3 cd0000        	call	_GPIO_Init
 370  00a6 85            	popw	x
 371                     ; 81 				_delay_us(2);
 373  00a7 ae0002        	ldw	x,#2
 374  00aa cd0000        	call	__delay_us
 376                     ; 82 				DS18B20_DQ_IN;
 378  00ad 4b40          	push	#64
 379  00af 4b08          	push	#8
 380  00b1 ae5000        	ldw	x,#20480
 381  00b4 cd0000        	call	_GPIO_Init
 383  00b7 85            	popw	x
 384                     ; 83 				_data >>= 1;
 386  00b8 0402          	srl	(OFST+0,sp)
 387                     ; 84 				_delay_us(5);
 389  00ba ae0005        	ldw	x,#5
 390  00bd cd0000        	call	__delay_us
 392                     ; 85 				if (DS18B20_DQ_PIN != 0)
 394  00c0 4b08          	push	#8
 395  00c2 ae5000        	ldw	x,#20480
 396  00c5 cd0000        	call	_GPIO_ReadInputDataBit
 398  00c8 5b01          	addw	sp,#1
 399  00ca 4d            	tnz	a
 400  00cb 270e          	jreq	L151
 401                     ; 87 						_data |= 0x80;
 403  00cd 7b02          	ld	a,(OFST+0,sp)
 404  00cf aa80          	or	a,#128
 405  00d1 6b02          	ld	(OFST+0,sp),a
 406                     ; 88 						_delay_us(30);
 408  00d3 ae001e        	ldw	x,#30
 409  00d6 cd0000        	call	__delay_us
 412  00d9 2013          	jra	L541
 413  00db               L151:
 414                     ; 92 						while(DS18B20_DQ_PIN == 0);
 416  00db 4b08          	push	#8
 417  00dd ae5000        	ldw	x,#20480
 418  00e0 cd0000        	call	_GPIO_ReadInputDataBit
 420  00e3 5b01          	addw	sp,#1
 421  00e5 4d            	tnz	a
 422  00e6 27f3          	jreq	L151
 423                     ; 93 						_delay_us(30);
 425  00e8 ae001e        	ldw	x,#30
 426  00eb cd0000        	call	__delay_us
 428  00ee               L541:
 429                     ; 78 		for (i = 0; i < 8; i++)
 431  00ee 0c01          	inc	(OFST-1,sp)
 434  00f0 7b01          	ld	a,(OFST-1,sp)
 435  00f2 a108          	cp	a,#8
 436  00f4 25a6          	jrult	L531
 437                     ; 97 		return _data;
 439  00f6 7b02          	ld	a,(OFST+0,sp)
 442  00f8 85            	popw	x
 443  00f9 81            	ret
 508                     ; 107 unsigned int DS18B20_ReadTemperature(void)
 508                     ; 108 {
 509                     	switch	.text
 510  00fa               _DS18B20_ReadTemperature:
 512  00fa 5208          	subw	sp,#8
 513       00000008      OFST:	set	8
 516                     ; 109 		unsigned char a = 0;
 518                     ; 110 		unsigned char b = 0;
 520                     ; 111 		unsigned int  t = 0;
 522                     ; 112 		float tt = 0;
 525  00fc               L112:
 526                     ; 114 		while(DS18B20_Init() == 0);
 528  00fc cd0016        	call	_DS18B20_Init
 530  00ff 4d            	tnz	a
 531  0100 27fa          	jreq	L112
 532                     ; 115 		DS18B20_WriteByte(0xcc);
 534  0102 a6cc          	ld	a,#204
 535  0104 cd0052        	call	_DS18B20_WriteByte
 537                     ; 116 		DS18B20_WriteByte(0x44);
 539  0107 a644          	ld	a,#68
 540  0109 cd0052        	call	_DS18B20_WriteByte
 543  010c               L712:
 544                     ; 118 		while(DS18B20_Init() == 0);
 546  010c cd0016        	call	_DS18B20_Init
 548  010f 4d            	tnz	a
 549  0110 27fa          	jreq	L712
 550                     ; 119 		DS18B20_WriteByte(0xcc);
 552  0112 a6cc          	ld	a,#204
 553  0114 cd0052        	call	_DS18B20_WriteByte
 555                     ; 120 		DS18B20_WriteByte(0xbe);
 557  0117 a6be          	ld	a,#190
 558  0119 cd0052        	call	_DS18B20_WriteByte
 560                     ; 122 		a = DS18B20_ReadByte();
 562  011c cd0097        	call	_DS18B20_ReadByte
 564  011f 6b01          	ld	(OFST-7,sp),a
 565                     ; 123 		b = DS18B20_ReadByte();
 567  0121 cd0097        	call	_DS18B20_ReadByte
 569  0124 6b02          	ld	(OFST-6,sp),a
 570                     ; 125 		t=b;
 572  0126 7b02          	ld	a,(OFST-6,sp)
 573  0128 5f            	clrw	x
 574  0129 97            	ld	xl,a
 575  012a 1f07          	ldw	(OFST-1,sp),x
 576                     ; 126 		t<<=8;
 578  012c 7b08          	ld	a,(OFST+0,sp)
 579  012e 6b07          	ld	(OFST-1,sp),a
 580  0130 0f08          	clr	(OFST+0,sp)
 581                     ; 127 		t=t|a;
 583  0132 7b01          	ld	a,(OFST-7,sp)
 584  0134 5f            	clrw	x
 585  0135 97            	ld	xl,a
 586  0136 01            	rrwa	x,a
 587  0137 1a08          	or	a,(OFST+0,sp)
 588  0139 01            	rrwa	x,a
 589  013a 1a07          	or	a,(OFST-1,sp)
 590  013c 01            	rrwa	x,a
 591  013d 1f07          	ldw	(OFST-1,sp),x
 592                     ; 128 		tt=t*0.0625;
 594  013f 1e07          	ldw	x,(OFST-1,sp)
 595  0141 cd0000        	call	c_uitof
 597  0144 ae0008        	ldw	x,#L722
 598  0147 cd0000        	call	c_fmul
 600  014a 96            	ldw	x,sp
 601  014b 1c0003        	addw	x,#OFST-5
 602  014e cd0000        	call	c_rtol
 604                     ; 129 		t = tt*10+0.5;
 606  0151 96            	ldw	x,sp
 607  0152 1c0003        	addw	x,#OFST-5
 608  0155 cd0000        	call	c_ltor
 610  0158 ae0004        	ldw	x,#L732
 611  015b cd0000        	call	c_fmul
 613  015e ae0000        	ldw	x,#L742
 614  0161 cd0000        	call	c_fadd
 616  0164 cd0000        	call	c_ftoi
 618  0167 1f07          	ldw	(OFST-1,sp),x
 619                     ; 131 		return t;
 621  0169 1e07          	ldw	x,(OFST-1,sp)
 624  016b 5b08          	addw	sp,#8
 625  016d 81            	ret
 638                     	xdef	_DS18B20_ReadByte
 639                     	xdef	_DS18B20_WriteByte
 640                     	xdef	__delay_us
 641                     	xdef	_DS18B20_ReadTemperature
 642                     	xdef	_DS18B20_Init
 643                     	xref	_GPIO_ReadInputDataBit
 644                     	xref	_GPIO_SetBits
 645                     	xref	_GPIO_Init
 646                     .const:	section	.text
 647  0000               L742:
 648  0000 3f000000      	dc.w	16128,0
 649  0004               L732:
 650  0004 41200000      	dc.w	16672,0
 651  0008               L722:
 652  0008 3d800000      	dc.w	15744,0
 653                     	xref.b	c_x
 673                     	xref	c_ftoi
 674                     	xref	c_fadd
 675                     	xref	c_ltor
 676                     	xref	c_rtol
 677                     	xref	c_fmul
 678                     	xref	c_uitof
 679                     	end
