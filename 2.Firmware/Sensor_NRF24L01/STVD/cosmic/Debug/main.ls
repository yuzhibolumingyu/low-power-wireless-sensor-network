   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  15                     	bsct
  16  0000               _ButtonPressed:
  17  0000 00            	dc.b	0
  18  0001               _txData:
  19  0001 00            	dc.b	0
  20  0002 00            	dc.b	0
  21  0003 00            	dc.b	0
  22  0004 00            	dc.b	0
  23  0005 00            	dc.b	0
  24  0006 00            	dc.b	0
  25  0007 00            	dc.b	0
 105                     ; 44 main()
 105                     ; 45 {
 107                     	switch	.text
 108  0000               _main:
 110  0000 5205          	subw	sp,#5
 111       00000005      OFST:	set	5
 114                     ; 49 		u8   Count_Sleep = 3;//睡眠计数
 116  0002 a603          	ld	a,#3
 117  0004 6b05          	ld	(OFST+0,sp),a
 118                     ; 53     CLK_Config();//系统时钟源配置
 120  0006 ad74          	call	L3_CLK_Config
 122                     ; 54 		Delay(1600);//1ms
 124  0008 ae0640        	ldw	x,#1600
 125  000b ad5d          	call	_Delay
 127                     ; 55 		AWU_Config();//AWU配置
 129  000d cd0093        	call	L5_AWU_Config
 131                     ; 56 		Delay(1600);//1ms
 133  0010 ae0640        	ldw	x,#1600
 134  0013 ad55          	call	_Delay
 136                     ; 57 		GPIO_LowPower_Config();//IO口低功耗配置
 138  0015 cd00b8        	call	_GPIO_LowPower_Config
 140                     ; 58 		Delay(1600);//1ms
 142  0018 ae0640        	ldw	x,#1600
 143  001b ad4d          	call	_Delay
 145                     ; 61 		DS18B20_Init();//DS18B20初始化
 147  001d cd0000        	call	_DS18B20_Init
 149                     ; 62 		Delay(1600);//1ms
 151  0020 ae0640        	ldw	x,#1600
 152  0023 ad45          	call	_Delay
 154                     ; 63 		Init_NRF24L01();//初始化NRF24L01
 156  0025 cd0000        	call	_Init_NRF24L01
 158                     ; 64 		Delay(1600);//1ms
 160  0028 ae0640        	ldw	x,#1600
 161  002b ad3d          	call	_Delay
 163                     ; 66 		enableInterrupts();//开启全局中断
 166  002d 9a            rim
 168  002e               L74:
 169                     ; 70 	  Count_Sleep ++;
 171  002e 0c05          	inc	(OFST+0,sp)
 172                     ; 78 		if(Count_Sleep == 4)//30s * 4 = 120s = 2分钟唤醒一次
 174  0030 7b05          	ld	a,(OFST+0,sp)
 175  0032 a104          	cp	a,#4
 176  0034 2631          	jrne	L35
 177                     ; 80 			  Count_Sleep = 0;
 179  0036 0f05          	clr	(OFST+0,sp)
 180                     ; 83 		    SwitchToTxMode( );//切换到发送模式
 182  0038 cd0000        	call	_SwitchToTxMode
 184                     ; 84 		    Delay(2400);//1.5ms
 186  003b ae0960        	ldw	x,#2400
 187  003e ad2a          	call	_Delay
 189                     ; 86 		    Temperature = DS18B20_ReadTemperature();
 191  0040 cd0000        	call	_DS18B20_ReadTemperature
 193  0043 1f03          	ldw	(OFST-2,sp),x
 194                     ; 87 		    T_H = Temperature >> 8;
 196  0045 7b03          	ld	a,(OFST-2,sp)
 197  0047 6b01          	ld	(OFST-4,sp),a
 198                     ; 88 		    T_L = Temperature;
 200  0049 7b04          	ld	a,(OFST-1,sp)
 201  004b 6b02          	ld	(OFST-3,sp),a
 202                     ; 90 		    txData[0] = 0x01;
 204  004d 35010001      	mov	_txData,#1
 205                     ; 91 		    txData[1] = T_H;
 207  0051 7b01          	ld	a,(OFST-4,sp)
 208  0053 b702          	ld	_txData+1,a
 209                     ; 92 		    txData[2] = T_L;
 211  0055 7b02          	ld	a,(OFST-3,sp)
 212  0057 b703          	ld	_txData+2,a
 213                     ; 94 		    if(NRF24L01_SendPacket(txData, 7) != 0)//发送字符串
 215  0059 4b07          	push	#7
 216  005b ae0001        	ldw	x,#_txData
 217  005e cd0000        	call	_NRF24L01_SendPacket
 219  0061 5b01          	addw	sp,#1
 220  0063 4d            	tnz	a
 221                     ; 101 		    Power_off( );//进入掉电模式
 223  0064 cd0000        	call	_Power_off
 225  0067               L35:
 226                     ; 104 		halt();/* Program halted */
 229  0067 8e            halt
 233  0068 20c4          	jra	L74
 267                     ; 118 void Delay(u16 nCount)
 267                     ; 119  {//延时0.629us -- 16M/HZ
 268                     	switch	.text
 269  006a               _Delay:
 271  006a 89            	pushw	x
 272       00000000      OFST:	set	0
 275  006b               L77:
 276                     ; 120    while(nCount--);
 278  006b 1e01          	ldw	x,(OFST+1,sp)
 279  006d 1d0001        	subw	x,#1
 280  0070 1f01          	ldw	(OFST+1,sp),x
 281  0072 1c0001        	addw	x,#1
 282  0075 a30000        	cpw	x,#0
 283  0078 26f1          	jrne	L77
 284                     ; 121  }
 287  007a 85            	popw	x
 288  007b 81            	ret
 313                     ; 131 static void CLK_Config(void)
 313                     ; 132 {
 314                     	switch	.text
 315  007c               L3_CLK_Config:
 319                     ; 135     CLK_MasterPrescalerConfig(CLK_MasterPrescaler_HSIDiv1);
 321  007c 4f            	clr	a
 322  007d cd0000        	call	_CLK_MasterPrescalerConfig
 324                     ; 138     CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
 326  0080 ae0001        	ldw	x,#1
 327  0083 a601          	ld	a,#1
 328  0085 95            	ld	xh,a
 329  0086 cd0000        	call	_CLK_PeripheralClockConfig
 331                     ; 139     CLK_PeripheralClockConfig(CLK_Peripheral_AWU, ENABLE);
 333  0089 ae0001        	ldw	x,#1
 334  008c a640          	ld	a,#64
 335  008e 95            	ld	xh,a
 336  008f cd0000        	call	_CLK_PeripheralClockConfig
 338                     ; 140 }
 341  0092 81            	ret
 367                     ; 149 static void AWU_Config(void)
 367                     ; 150 {
 368                     	switch	.text
 369  0093               L5_AWU_Config:
 373                     ; 153     AWU_LSICalibrationConfig(LSIMeasurment());
 375  0093 ad50          	call	_LSIMeasurment
 377  0095 be02          	ldw	x,c_lreg+2
 378  0097 89            	pushw	x
 379  0098 be00          	ldw	x,c_lreg
 380  009a 89            	pushw	x
 381  009b cd0000        	call	_AWU_LSICalibrationConfig
 383  009e 5b04          	addw	sp,#4
 384                     ; 156     AWU_Init(AWU_Timebase_30s);
 386  00a0 a610          	ld	a,#16
 387  00a2 cd0000        	call	_AWU_Init
 389                     ; 157 }
 392  00a5 81            	ret
 417                     ; 166 void IO_Config(void)
 417                     ; 167 {
 418                     	switch	.text
 419  00a6               _IO_Config:
 423                     ; 168 		GPIO_DeInit(GPIOA);
 425  00a6 ae5000        	ldw	x,#20480
 426  00a9 cd0000        	call	_GPIO_DeInit
 428                     ; 169 	  GPIO_Init(GPIOA, GPIO_Pin_2, GPIO_Mode_Out_PP_Low_Fast);//LED,PA.2
 430  00ac 4be0          	push	#224
 431  00ae 4b04          	push	#4
 432  00b0 ae5000        	ldw	x,#20480
 433  00b3 cd0000        	call	_GPIO_Init
 435  00b6 85            	popw	x
 436                     ; 171 }
 439  00b7 81            	ret
 464                     ; 180 void GPIO_LowPower_Config(void)
 464                     ; 181 {
 465                     	switch	.text
 466  00b8               _GPIO_LowPower_Config:
 470                     ; 183     GPIO_Init(GPIOA, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
 472  00b8 4bc0          	push	#192
 473  00ba 4bff          	push	#255
 474  00bc ae5000        	ldw	x,#20480
 475  00bf cd0000        	call	_GPIO_Init
 477  00c2 85            	popw	x
 478                     ; 185     GPIO_Init(GPIOB, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
 480  00c3 4bc0          	push	#192
 481  00c5 4bff          	push	#255
 482  00c7 ae5005        	ldw	x,#20485
 483  00ca cd0000        	call	_GPIO_Init
 485  00cd 85            	popw	x
 486                     ; 187     GPIO_Init(GPIOC, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
 488  00ce 4bc0          	push	#192
 489  00d0 4bff          	push	#255
 490  00d2 ae500a        	ldw	x,#20490
 491  00d5 cd0000        	call	_GPIO_Init
 493  00d8 85            	popw	x
 494                     ; 189     GPIO_Init(GPIOD, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
 496  00d9 4bc0          	push	#192
 497  00db 4bff          	push	#255
 498  00dd ae500f        	ldw	x,#20495
 499  00e0 cd0000        	call	_GPIO_Init
 501  00e3 85            	popw	x
 502                     ; 190 }
 505  00e4 81            	ret
 571                     ; 199 uint32_t LSIMeasurment(void)
 571                     ; 200 {
 572                     	switch	.text
 573  00e5               _LSIMeasurment:
 575  00e5 520c          	subw	sp,#12
 576       0000000c      OFST:	set	12
 579                     ; 202   uint32_t lsi_freq_hz = 0x0;
 581                     ; 203   uint32_t fmaster = 0x0;
 583                     ; 204   uint16_t ICValue1 = 0x0;
 585                     ; 205   uint16_t ICValue2 = 0x0;
 587                     ; 208   fmaster = CLK_GetClockFreq();
 589  00e7 cd0000        	call	_CLK_GetClockFreq
 591  00ea 96            	ldw	x,sp
 592  00eb 1c0009        	addw	x,#OFST-3
 593  00ee cd0000        	call	c_rtol
 595                     ; 211   AWU->CSR |= AWU_CSR_MSR;
 597  00f1 721050f0      	bset	20720,#0
 598                     ; 214   TIM2_ICInit(  TIM2_Channel_1, TIM2_ICPolarity_Rising, TIM2_ICSelection_DirectTI, TIM2_ICPSC_Div8, 0x0);
 600  00f5 4b00          	push	#0
 601  00f7 4b0c          	push	#12
 602  00f9 4b01          	push	#1
 603  00fb 5f            	clrw	x
 604  00fc 4f            	clr	a
 605  00fd 95            	ld	xh,a
 606  00fe cd0000        	call	_TIM2_ICInit
 608  0101 5b03          	addw	sp,#3
 609                     ; 217   TIM2_Cmd(ENABLE);
 611  0103 a601          	ld	a,#1
 612  0105 cd0000        	call	_TIM2_Cmd
 615  0108               L771:
 616                     ; 220   while ((TIM2->SR1 & (uint8_t)TIM2_FLAG_CC1) != TIM2_FLAG_CC1);
 618  0108 c65255        	ld	a,21077
 619  010b a402          	and	a,#2
 620  010d a102          	cp	a,#2
 621  010f 26f7          	jrne	L771
 622                     ; 222   ICValue1 = TIM2_GetCapture1();
 624  0111 cd0000        	call	_TIM2_GetCapture1
 626  0114 1f05          	ldw	(OFST-7,sp),x
 627                     ; 223   TIM2_ClearFlag(TIM2_FLAG_CC1);
 629  0116 ae0002        	ldw	x,#2
 630  0119 cd0000        	call	_TIM2_ClearFlag
 633  011c               L502:
 634                     ; 226   while ((TIM2->SR1 & (uint8_t)TIM2_FLAG_CC1) != TIM2_FLAG_CC1);
 636  011c c65255        	ld	a,21077
 637  011f a402          	and	a,#2
 638  0121 a102          	cp	a,#2
 639  0123 26f7          	jrne	L502
 640                     ; 228   ICValue2 = TIM2_GetCapture1();
 642  0125 cd0000        	call	_TIM2_GetCapture1
 644  0128 1f07          	ldw	(OFST-5,sp),x
 645                     ; 229   TIM2_ClearFlag(TIM2_FLAG_CC1);
 647  012a ae0002        	ldw	x,#2
 648  012d cd0000        	call	_TIM2_ClearFlag
 650                     ; 232   TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1E);
 652  0130 7211525a      	bres	21082,#0
 653                     ; 234   TIM2_Cmd(DISABLE);
 655  0134 4f            	clr	a
 656  0135 cd0000        	call	_TIM2_Cmd
 658                     ; 237   lsi_freq_hz = (8 * fmaster) / (ICValue2 - ICValue1);
 660  0138 1e07          	ldw	x,(OFST-5,sp)
 661  013a 72f005        	subw	x,(OFST-7,sp)
 662  013d cd0000        	call	c_uitolx
 664  0140 96            	ldw	x,sp
 665  0141 1c0001        	addw	x,#OFST-11
 666  0144 cd0000        	call	c_rtol
 668  0147 96            	ldw	x,sp
 669  0148 1c0009        	addw	x,#OFST-3
 670  014b cd0000        	call	c_ltor
 672  014e a603          	ld	a,#3
 673  0150 cd0000        	call	c_llsh
 675  0153 96            	ldw	x,sp
 676  0154 1c0001        	addw	x,#OFST-11
 677  0157 cd0000        	call	c_ludv
 679  015a 96            	ldw	x,sp
 680  015b 1c0009        	addw	x,#OFST-3
 681  015e cd0000        	call	c_rtol
 683                     ; 240   AWU->CSR &= (uint8_t)(~AWU_CSR_MSR);
 685  0161 721150f0      	bres	20720,#0
 686                     ; 242   return (lsi_freq_hz);
 688  0165 96            	ldw	x,sp
 689  0166 1c0009        	addw	x,#OFST-3
 690  0169 cd0000        	call	c_ltor
 694  016c 5b0c          	addw	sp,#12
 695  016e 81            	ret
 750                     	xdef	_main
 751                     	xdef	_LSIMeasurment
 752                     	xdef	_Delay
 753                     	xdef	_GPIO_LowPower_Config
 754                     	xdef	_IO_Config
 755                     	xdef	_txData
 756                     	xdef	_ButtonPressed
 757                     	xref	_DS18B20_ReadTemperature
 758                     	xref	_DS18B20_Init
 759                     	xref	_SwitchToTxMode
 760                     	xref	_Power_off
 761                     	xref	_NRF24L01_SendPacket
 762                     	xref	_Init_NRF24L01
 763                     	xref	_TIM2_ClearFlag
 764                     	xref	_TIM2_GetCapture1
 765                     	xref	_TIM2_Cmd
 766                     	xref	_TIM2_ICInit
 767                     	xref	_GPIO_Init
 768                     	xref	_GPIO_DeInit
 769                     	xref	_CLK_GetClockFreq
 770                     	xref	_CLK_MasterPrescalerConfig
 771                     	xref	_CLK_PeripheralClockConfig
 772                     	xref	_AWU_LSICalibrationConfig
 773                     	xref	_AWU_Init
 774                     	xref.b	c_lreg
 775                     	xref.b	c_x
 794                     	xref	c_ludv
 795                     	xref	c_uitolx
 796                     	xref	c_llsh
 797                     	xref	c_ltor
 798                     	xref	c_rtol
 799                     	end
