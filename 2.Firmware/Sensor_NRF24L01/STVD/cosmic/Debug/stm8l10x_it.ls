   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  44                     ; 50 INTERRUPT_HANDLER(NonHandledInterrupt,0)
  44                     ; 51 {
  45                     	switch	.text
  46  0000               f_NonHandledInterrupt:
  50                     ; 55 }
  53  0000 80            	iret
  75                     ; 63 INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
  75                     ; 64 {
  76                     	switch	.text
  77  0001               f_TRAP_IRQHandler:
  81                     ; 68 }
  84  0001 80            	iret
 106                     ; 75 INTERRUPT_HANDLER(FLASH_IRQHandler,1)
 106                     ; 76 {
 107                     	switch	.text
 108  0002               f_FLASH_IRQHandler:
 112                     ; 80 }
 115  0002 80            	iret
 138                     ; 87 INTERRUPT_HANDLER(AWU_IRQHandler,4)
 138                     ; 88 {
 139                     	switch	.text
 140  0003               f_AWU_IRQHandler:
 142  0003 3b0002        	push	c_x+2
 143  0006 be00          	ldw	x,c_x
 144  0008 89            	pushw	x
 145  0009 3b0002        	push	c_y+2
 146  000c be00          	ldw	x,c_y
 147  000e 89            	pushw	x
 150                     ; 90   AWU_GetFlagStatus();
 152  000f cd0000        	call	_AWU_GetFlagStatus
 154                     ; 91 }
 157  0012 85            	popw	x
 158  0013 bf00          	ldw	c_y,x
 159  0015 320002        	pop	c_y+2
 160  0018 85            	popw	x
 161  0019 bf00          	ldw	c_x,x
 162  001b 320002        	pop	c_x+2
 163  001e 80            	iret
 185                     ; 98 INTERRUPT_HANDLER(EXTIB_IRQHandler, 6)
 185                     ; 99 {
 186                     	switch	.text
 187  001f               f_EXTIB_IRQHandler:
 191                     ; 103 }
 194  001f 80            	iret
 216                     ; 110 INTERRUPT_HANDLER(EXTID_IRQHandler, 7)
 216                     ; 111 {
 217                     	switch	.text
 218  0020               f_EXTID_IRQHandler:
 222                     ; 115 }
 225  0020 80            	iret
 247                     ; 122 INTERRUPT_HANDLER(EXTI0_IRQHandler, 8)
 247                     ; 123 {
 248                     	switch	.text
 249  0021               f_EXTI0_IRQHandler:
 253                     ; 127 }
 256  0021 80            	iret
 278                     ; 134 INTERRUPT_HANDLER(EXTI1_IRQHandler, 9)
 278                     ; 135 {
 279                     	switch	.text
 280  0022               f_EXTI1_IRQHandler:
 284                     ; 139 }
 287  0022 80            	iret
 309                     ; 146 INTERRUPT_HANDLER(EXTI2_IRQHandler, 10)
 309                     ; 147 {
 310                     	switch	.text
 311  0023               f_EXTI2_IRQHandler:
 315                     ; 151 }
 318  0023 80            	iret
 340                     ; 158 INTERRUPT_HANDLER(EXTI3_IRQHandler, 11)
 340                     ; 159 {
 341                     	switch	.text
 342  0024               f_EXTI3_IRQHandler:
 346                     ; 163 }
 349  0024 80            	iret
 371                     ; 170 INTERRUPT_HANDLER(EXTI4_IRQHandler, 12)
 371                     ; 171 {
 372                     	switch	.text
 373  0025               f_EXTI4_IRQHandler:
 377                     ; 175 }
 380  0025 80            	iret
 402                     ; 182 INTERRUPT_HANDLER(EXTI5_IRQHandler, 13)
 402                     ; 183 {
 403                     	switch	.text
 404  0026               f_EXTI5_IRQHandler:
 408                     ; 187 }
 411  0026 80            	iret
 433                     ; 194 INTERRUPT_HANDLER(EXTI6_IRQHandler, 14)
 433                     ; 195 {
 434                     	switch	.text
 435  0027               f_EXTI6_IRQHandler:
 439                     ; 199 }
 442  0027 80            	iret
 465                     ; 206 INTERRUPT_HANDLER(EXTI7_IRQHandler, 15)
 465                     ; 207 {
 466                     	switch	.text
 467  0028               f_EXTI7_IRQHandler:
 471                     ; 211 		ButtonPressed = TRUE;
 473  0028 35010000      	mov	_ButtonPressed,#1
 474                     ; 213 }
 477  002c 80            	iret
 499                     ; 220 INTERRUPT_HANDLER(COMP_IRQHandler, 18)
 499                     ; 221 {
 500                     	switch	.text
 501  002d               f_COMP_IRQHandler:
 505                     ; 225 }
 508  002d 80            	iret
 531                     ; 232 INTERRUPT_HANDLER(TIM2_UPD_OVF_TRG_BRK_IRQHandler, 19)
 531                     ; 233 {
 532                     	switch	.text
 533  002e               f_TIM2_UPD_OVF_TRG_BRK_IRQHandler:
 537                     ; 237 }
 540  002e 80            	iret
 563                     ; 244 INTERRUPT_HANDLER(TIM2_CAP_IRQHandler, 20)
 563                     ; 245 {
 564                     	switch	.text
 565  002f               f_TIM2_CAP_IRQHandler:
 569                     ; 249 }
 572  002f 80            	iret
 595                     ; 257 INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_IRQHandler, 21)
 595                     ; 258 {
 596                     	switch	.text
 597  0030               f_TIM3_UPD_OVF_TRG_BRK_IRQHandler:
 601                     ; 262 }
 604  0030 80            	iret
 627                     ; 268 INTERRUPT_HANDLER(TIM3_CAP_IRQHandler, 22)
 627                     ; 269 {
 628                     	switch	.text
 629  0031               f_TIM3_CAP_IRQHandler:
 633                     ; 273 }
 636  0031 80            	iret
 659                     ; 279 INTERRUPT_HANDLER(TIM4_UPD_OVF_IRQHandler, 25)
 659                     ; 280 {
 660                     	switch	.text
 661  0032               f_TIM4_UPD_OVF_IRQHandler:
 665                     ; 284 }
 668  0032 80            	iret
 690                     ; 291 INTERRUPT_HANDLER(SPI_IRQHandler, 26)
 690                     ; 292 {
 691                     	switch	.text
 692  0033               f_SPI_IRQHandler:
 696                     ; 296 }
 699  0033 80            	iret
 722                     ; 303 INTERRUPT_HANDLER(USART_TX_IRQHandler, 27)
 722                     ; 304 {
 723                     	switch	.text
 724  0034               f_USART_TX_IRQHandler:
 728                     ; 308 }
 731  0034 80            	iret
 754                     ; 315 INTERRUPT_HANDLER(USART_RX_IRQHandler, 28)
 754                     ; 316 {
 755                     	switch	.text
 756  0035               f_USART_RX_IRQHandler:
 760                     ; 320 }
 763  0035 80            	iret
 785                     ; 327 INTERRUPT_HANDLER(I2C_IRQHandler, 29)
 785                     ; 328 {
 786                     	switch	.text
 787  0036               f_I2C_IRQHandler:
 791                     ; 332 }
 794  0036 80            	iret
 806                     	xref.b	_ButtonPressed
 807                     	xdef	f_I2C_IRQHandler
 808                     	xdef	f_USART_RX_IRQHandler
 809                     	xdef	f_USART_TX_IRQHandler
 810                     	xdef	f_SPI_IRQHandler
 811                     	xdef	f_TIM4_UPD_OVF_IRQHandler
 812                     	xdef	f_TIM3_CAP_IRQHandler
 813                     	xdef	f_TIM3_UPD_OVF_TRG_BRK_IRQHandler
 814                     	xdef	f_TIM2_CAP_IRQHandler
 815                     	xdef	f_TIM2_UPD_OVF_TRG_BRK_IRQHandler
 816                     	xdef	f_COMP_IRQHandler
 817                     	xdef	f_EXTI7_IRQHandler
 818                     	xdef	f_EXTI6_IRQHandler
 819                     	xdef	f_EXTI5_IRQHandler
 820                     	xdef	f_EXTI4_IRQHandler
 821                     	xdef	f_EXTI3_IRQHandler
 822                     	xdef	f_EXTI2_IRQHandler
 823                     	xdef	f_EXTI1_IRQHandler
 824                     	xdef	f_EXTI0_IRQHandler
 825                     	xdef	f_EXTID_IRQHandler
 826                     	xdef	f_EXTIB_IRQHandler
 827                     	xdef	f_AWU_IRQHandler
 828                     	xdef	f_FLASH_IRQHandler
 829                     	xdef	f_TRAP_IRQHandler
 830                     	xdef	f_NonHandledInterrupt
 831                     	xref	_AWU_GetFlagStatus
 832                     	xref.b	c_x
 833                     	xref.b	c_y
 852                     	end
