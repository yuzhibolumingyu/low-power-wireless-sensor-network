   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  66                     ; 22 BYTE SPI_RW(BYTE _byte)
  66                     ; 23 {  
  68                     	switch	.text
  69  0000               _SPI_RW:
  71  0000 88            	push	a
  72  0001 88            	push	a
  73       00000001      OFST:	set	1
  76                     ; 25     for(bit_ctr = 0;bit_ctr < 8; bit_ctr++)// output 8-bit
  78  0002 0f01          	clr	(OFST+0,sp)
  79  0004               L33:
  80                     ; 27         if (_byte & 0x80)// output 'byte', MSB to MOSI
  82  0004 7b02          	ld	a,(OFST+1,sp)
  83  0006 a580          	bcp	a,#128
  84  0008 270b          	jreq	L14
  85                     ; 28 					  {NRF24L01_MOSI_SET;}//��'1'
  87  000a 4b20          	push	#32
  88  000c ae5005        	ldw	x,#20485
  89  000f cd0000        	call	_GPIO_SetBits
  91  0012 84            	pop	a
  94  0013 2009          	jra	L34
  95  0015               L14:
  96                     ; 30 					  {NRF24L01_MOSI_RESET;}//��'0'
  98  0015 4b20          	push	#32
  99  0017 ae5005        	ldw	x,#20485
 100  001a cd0000        	call	_GPIO_ResetBits
 102  001d 84            	pop	a
 103  001e               L34:
 104                     ; 32    	    _byte = (_byte << 1);            // shift next bit into MSB..
 106  001e 0802          	sll	(OFST+1,sp)
 107                     ; 33    	    NRF24L01_SCK_SET;                // Set SCK high..
 109  0020 4b08          	push	#8
 110  0022 ae5005        	ldw	x,#20485
 111  0025 cd0000        	call	_GPIO_SetBits
 113  0028 84            	pop	a
 114                     ; 34    	    NRF24L01_MISO_SET;
 117  0029 4b10          	push	#16
 118  002b ae5005        	ldw	x,#20485
 119  002e cd0000        	call	_GPIO_SetBits
 121  0031 84            	pop	a
 122                     ; 35 				if(NRF24L01_MISO_PIN)_byte |= 1; // capture current MISO bit                  
 125  0032 4b10          	push	#16
 126  0034 ae5005        	ldw	x,#20485
 127  0037 cd0000        	call	_GPIO_ReadInputDataBit
 129  003a 5b01          	addw	sp,#1
 130  003c 4d            	tnz	a
 131  003d 2706          	jreq	L54
 134  003f 7b02          	ld	a,(OFST+1,sp)
 135  0041 aa01          	or	a,#1
 136  0043 6b02          	ld	(OFST+1,sp),a
 137  0045               L54:
 138                     ; 36    	    NRF24L01_SCK_RESET;              // ..then set SCK low again
 140  0045 4b08          	push	#8
 141  0047 ae5005        	ldw	x,#20485
 142  004a cd0000        	call	_GPIO_ResetBits
 144  004d 84            	pop	a
 145                     ; 25     for(bit_ctr = 0;bit_ctr < 8; bit_ctr++)// output 8-bit
 148  004e 0c01          	inc	(OFST+0,sp)
 151  0050 7b01          	ld	a,(OFST+0,sp)
 152  0052 a108          	cp	a,#8
 153  0054 25ae          	jrult	L33
 154                     ; 38     return(_byte);           	           // return read byte
 156  0056 7b02          	ld	a,(OFST+1,sp)
 159  0058 85            	popw	x
 160  0059 81            	ret
 215                     ; 48 BYTE SPI_Write_Reg(BYTE reg, BYTE value)
 215                     ; 49 {       
 216                     	switch	.text
 217  005a               _SPI_Write_Reg:
 219  005a 89            	pushw	x
 220  005b 88            	push	a
 221       00000001      OFST:	set	1
 224                     ; 51   	NRF24L01_CSN_RESET;  // CSN low, init SPI transaction
 226  005c 4b04          	push	#4
 227  005e ae5005        	ldw	x,#20485
 228  0061 cd0000        	call	_GPIO_ResetBits
 230  0064 84            	pop	a
 231                     ; 52   	status = SPI_RW(reg);// select register
 234  0065 7b02          	ld	a,(OFST+1,sp)
 235  0067 ad97          	call	_SPI_RW
 237  0069 6b01          	ld	(OFST+0,sp),a
 238                     ; 53   	SPI_RW(value);       // ..and write value to it..
 240  006b 7b03          	ld	a,(OFST+2,sp)
 241  006d ad91          	call	_SPI_RW
 243                     ; 54   	NRF24L01_CSN_SET;    // CSN high again
 245  006f 4b04          	push	#4
 246  0071 ae5005        	ldw	x,#20485
 247  0074 cd0000        	call	_GPIO_SetBits
 249  0077 84            	pop	a
 250                     ; 55   	return(status);      // return nRF24L01 status byte
 253  0078 7b01          	ld	a,(OFST+0,sp)
 256  007a 5b03          	addw	sp,#3
 257  007c 81            	ret
 303                     ; 65 BYTE SPI_Read_Reg(BYTE reg)
 303                     ; 66 {
 304                     	switch	.text
 305  007d               _SPI_Read_Reg:
 307  007d 88            	push	a
 308  007e 88            	push	a
 309       00000001      OFST:	set	1
 312                     ; 68   	NRF24L01_CSN_RESET;// CSN low, initialize SPI communication...
 314  007f 4b04          	push	#4
 315  0081 ae5005        	ldw	x,#20485
 316  0084 cd0000        	call	_GPIO_ResetBits
 318  0087 84            	pop	a
 319                     ; 69   	SPI_RW(reg);       // Select register to read from..
 322  0088 7b02          	ld	a,(OFST+1,sp)
 323  008a cd0000        	call	_SPI_RW
 325                     ; 70   	value = SPI_RW(0); // ..then read registervalue
 327  008d 4f            	clr	a
 328  008e cd0000        	call	_SPI_RW
 330  0091 6b01          	ld	(OFST+0,sp),a
 331                     ; 71   	NRF24L01_CSN_SET;  // CSN high, terminate SPI communication
 333  0093 4b04          	push	#4
 334  0095 ae5005        	ldw	x,#20485
 335  0098 cd0000        	call	_GPIO_SetBits
 337  009b 84            	pop	a
 338                     ; 72   	return(value);     // return register value
 341  009c 7b01          	ld	a,(OFST+0,sp)
 344  009e 85            	popw	x
 345  009f 81            	ret
 419                     ; 82 BYTE SPI_Write_Buf(BYTE reg, BYTE *pBuf, BYTE bytes)
 419                     ; 83 {
 420                     	switch	.text
 421  00a0               _SPI_Write_Buf:
 423  00a0 88            	push	a
 424  00a1 89            	pushw	x
 425       00000002      OFST:	set	2
 428                     ; 85   	NRF24L01_CSN_RESET;      // Set CSN low, init SPI tranaction
 430  00a2 4b04          	push	#4
 431  00a4 ae5005        	ldw	x,#20485
 432  00a7 cd0000        	call	_GPIO_ResetBits
 434  00aa 84            	pop	a
 435                     ; 86   	status = SPI_RW(reg);    // Select register to write to and read status byte
 438  00ab 7b03          	ld	a,(OFST+1,sp)
 439  00ad cd0000        	call	_SPI_RW
 441  00b0 6b01          	ld	(OFST-1,sp),a
 442                     ; 87   	for(byte_ctr = 0;byte_ctr < bytes; byte_ctr++)//then write all byte in buffer(*pBuf)
 444  00b2 0f02          	clr	(OFST+0,sp)
 446  00b4 2010          	jra	L161
 447  00b6               L551:
 448                     ; 89 	    SPI_RW(*pBuf++);
 450  00b6 1e06          	ldw	x,(OFST+4,sp)
 451  00b8 1c0001        	addw	x,#1
 452  00bb 1f06          	ldw	(OFST+4,sp),x
 453  00bd 1d0001        	subw	x,#1
 454  00c0 f6            	ld	a,(x)
 455  00c1 cd0000        	call	_SPI_RW
 457                     ; 87   	for(byte_ctr = 0;byte_ctr < bytes; byte_ctr++)//then write all byte in buffer(*pBuf)
 459  00c4 0c02          	inc	(OFST+0,sp)
 460  00c6               L161:
 463  00c6 7b02          	ld	a,(OFST+0,sp)
 464  00c8 1108          	cp	a,(OFST+6,sp)
 465  00ca 25ea          	jrult	L551
 466                     ; 91    	NRF24L01_CSN_SET;        // Set CSN high again
 468  00cc 4b04          	push	#4
 469  00ce ae5005        	ldw	x,#20485
 470  00d1 cd0000        	call	_GPIO_SetBits
 472  00d4 84            	pop	a
 473                     ; 92   	return(status);          // return nRF24L01 status byte
 476  00d5 7b01          	ld	a,(OFST-1,sp)
 479  00d7 5b03          	addw	sp,#3
 480  00d9 81            	ret
 554                     ; 102 BYTE SPI_Read_Buf(unsigned char reg, unsigned char *pBuf, unsigned char bytes)     
 554                     ; 103 {                                                           
 555                     	switch	.text
 556  00da               _SPI_Read_Buf:
 558  00da 88            	push	a
 559  00db 89            	pushw	x
 560       00000002      OFST:	set	2
 563                     ; 106   	NRF24L01_CSN_RESET;              // Set CSN l
 565  00dc 4b04          	push	#4
 566  00de ae5005        	ldw	x,#20485
 567  00e1 cd0000        	call	_GPIO_ResetBits
 569  00e4 84            	pop	a
 570                     ; 107   	status = SPI_RW(reg);            // Select re
 573  00e5 7b03          	ld	a,(OFST+1,sp)
 574  00e7 cd0000        	call	_SPI_RW
 576  00ea 6b01          	ld	(OFST-1,sp),a
 577                     ; 109   	for(byte_ctr = 0;byte_ctr < bytes; byte_ctr++)
 579  00ec 0f02          	clr	(OFST+0,sp)
 581  00ee 2014          	jra	L722
 582  00f0               L322:
 583                     ; 111     	    pBuf[byte_ctr] = SPI_RW(0);// Perform SPI_RW to 
 585  00f0 7b06          	ld	a,(OFST+4,sp)
 586  00f2 97            	ld	xl,a
 587  00f3 7b07          	ld	a,(OFST+5,sp)
 588  00f5 1b02          	add	a,(OFST+0,sp)
 589  00f7 2401          	jrnc	L61
 590  00f9 5c            	incw	x
 591  00fa               L61:
 592  00fa 02            	rlwa	x,a
 593  00fb 89            	pushw	x
 594  00fc 4f            	clr	a
 595  00fd cd0000        	call	_SPI_RW
 597  0100 85            	popw	x
 598  0101 f7            	ld	(x),a
 599                     ; 109   	for(byte_ctr = 0;byte_ctr < bytes; byte_ctr++)
 601  0102 0c02          	inc	(OFST+0,sp)
 602  0104               L722:
 605  0104 7b02          	ld	a,(OFST+0,sp)
 606  0106 1108          	cp	a,(OFST+6,sp)
 607  0108 25e6          	jrult	L322
 608                     ; 114   	NRF24L01_CSN_SET;                // Set CSN high again
 610  010a 4b04          	push	#4
 611  010c ae5005        	ldw	x,#20485
 612  010f cd0000        	call	_GPIO_SetBits
 614  0112 84            	pop	a
 615                     ; 115   	return(status);                  // return BK2411 
 618  0113 7b01          	ld	a,(OFST-1,sp)
 621  0115 5b03          	addw	sp,#3
 622  0117 81            	ret
 635                     	xdef	_SPI_Read_Buf
 636                     	xdef	_SPI_Write_Buf
 637                     	xdef	_SPI_Read_Reg
 638                     	xdef	_SPI_Write_Reg
 639                     	xdef	_SPI_RW
 640                     	xref	_GPIO_ReadInputDataBit
 641                     	xref	_GPIO_ResetBits
 642                     	xref	_GPIO_SetBits
 661                     	end
