   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  15                     	bsct
  16  0000               _TX_ADDRESS:
  17  0000 34            	dc.b	52
  18  0001 43            	dc.b	67
  19  0002 10            	dc.b	16
  20  0003 10            	dc.b	16
  21  0004 01            	dc.b	1
  22  0005               _RX_ADDRESS:
  23  0005 34            	dc.b	52
  24  0006 43            	dc.b	67
  25  0007 10            	dc.b	16
  26  0008 10            	dc.b	16
  27  0009 01            	dc.b	1
  67                     ; 27 void delay_ms(u16 nCount)
  67                     ; 28  {//延时0.629us -- 16MHz
  69                     	switch	.text
  70  0000               _delay_ms:
  72  0000 89            	pushw	x
  73       00000000      OFST:	set	0
  76                     ; 29     nCount = (nCount*1600);
  78  0001 1e01          	ldw	x,(OFST+1,sp)
  79  0003 90ae0640      	ldw	y,#1600
  80  0007 cd0000        	call	c_imul
  82  000a 1f01          	ldw	(OFST+1,sp),x
  84  000c               L13:
  85                     ; 30     while(nCount--);
  87  000c 1e01          	ldw	x,(OFST+1,sp)
  88  000e 1d0001        	subw	x,#1
  89  0011 1f01          	ldw	(OFST+1,sp),x
  90  0013 1c0001        	addw	x,#1
  91  0016 a30000        	cpw	x,#0
  92  0019 26f1          	jrne	L13
  93                     ; 31  }
  96  001b 85            	popw	x
  97  001c 81            	ret
 128                     ; 44 void Init_NRF24L01(void)
 128                     ; 45 { 
 129                     	switch	.text
 130  001d               _Init_NRF24L01:
 134                     ; 46     NRF24L01_CE_DDR;//输出
 136  001d 4bf0          	push	#240
 137  001f 4b02          	push	#2
 138  0021 ae5005        	ldw	x,#20485
 139  0024 cd0000        	call	_GPIO_Init
 141  0027 85            	popw	x
 142                     ; 47     NRF24L01_CE_SET;//置'1'
 145  0028 4b02          	push	#2
 146  002a ae5005        	ldw	x,#20485
 147  002d cd0000        	call	_GPIO_SetBits
 149  0030 84            	pop	a
 150                     ; 49     NRF24L01_CSN_DDR;//输出	
 153  0031 4bf0          	push	#240
 154  0033 4b04          	push	#4
 155  0035 ae5005        	ldw	x,#20485
 156  0038 cd0000        	call	_GPIO_Init
 158  003b 85            	popw	x
 159                     ; 50     NRF24L01_CSN_SET;//置'1'	
 162  003c 4b04          	push	#4
 163  003e ae5005        	ldw	x,#20485
 164  0041 cd0000        	call	_GPIO_SetBits
 166  0044 84            	pop	a
 167                     ; 52     NRF24L01_MOSI_DDR;  //输出
 170  0045 4bf0          	push	#240
 171  0047 4b20          	push	#32
 172  0049 ae5005        	ldw	x,#20485
 173  004c cd0000        	call	_GPIO_Init
 175  004f 85            	popw	x
 176                     ; 53     NRF24L01_MOSI_RESET;//置'0'	
 179  0050 4b20          	push	#32
 180  0052 ae5005        	ldw	x,#20485
 181  0055 cd0000        	call	_GPIO_ResetBits
 183  0058 84            	pop	a
 184                     ; 55     NRF24L01_IRQ_DDR;//输入
 187  0059 4b40          	push	#64
 188  005b 4b40          	push	#64
 189  005d ae5005        	ldw	x,#20485
 190  0060 cd0000        	call	_GPIO_Init
 192  0063 85            	popw	x
 193                     ; 56     NRF24L01_IRQ_SET;//置'1'
 196  0064 4b40          	push	#64
 197  0066 ae5005        	ldw	x,#20485
 198  0069 cd0000        	call	_GPIO_SetBits
 200  006c 84            	pop	a
 201                     ; 58     NRF24L01_MISO_DDR;//输入
 204  006d 4b40          	push	#64
 205  006f 4b10          	push	#16
 206  0071 ae5005        	ldw	x,#20485
 207  0074 cd0000        	call	_GPIO_Init
 209  0077 85            	popw	x
 210                     ; 59     NRF24L01_MISO_SET;//置'1'
 213  0078 4b10          	push	#16
 214  007a ae5005        	ldw	x,#20485
 215  007d cd0000        	call	_GPIO_SetBits
 217  0080 84            	pop	a
 218                     ; 61     NRF24L01_SCK_DDR;  //输出	    
 221  0081 4bf0          	push	#240
 222  0083 4b08          	push	#8
 223  0085 ae5005        	ldw	x,#20485
 224  0088 cd0000        	call	_GPIO_Init
 226  008b 85            	popw	x
 227                     ; 62     NRF24L01_SCK_RESET;//置'0'
 230  008c 4b08          	push	#8
 231  008e ae5005        	ldw	x,#20485
 232  0091 cd0000        	call	_GPIO_ResetBits
 234  0094 84            	pop	a
 235                     ; 64     NRF24L01_CE_RESET;//置'0'  
 238  0095 4b02          	push	#2
 239  0097 ae5005        	ldw	x,#20485
 240  009a cd0000        	call	_GPIO_ResetBits
 242  009d 84            	pop	a
 243                     ; 65 	  SPI_Write_Reg(WRITE_REG + CONFIG, 0x0d);//掉电
 246  009e ae000d        	ldw	x,#13
 247  00a1 a620          	ld	a,#32
 248  00a3 95            	ld	xh,a
 249  00a4 cd0000        	call	_SPI_Write_Reg
 251                     ; 66 		delay_ms(1);
 253  00a7 ae0001        	ldw	x,#1
 254  00aa cd0000        	call	_delay_ms
 256                     ; 67     SPI_Write_Reg(WRITE_REG + EN_AA, 0x01);      //频道0自动ACK应答允许	
 258  00ad ae0001        	ldw	x,#1
 259  00b0 a621          	ld	a,#33
 260  00b2 95            	ld	xh,a
 261  00b3 cd0000        	call	_SPI_Write_Reg
 263                     ; 68     SPI_Write_Reg(WRITE_REG + EN_RXADDR, 0x01);  //允许接收地址只有频道0
 265  00b6 ae0001        	ldw	x,#1
 266  00b9 a622          	ld	a,#34
 267  00bb 95            	ld	xh,a
 268  00bc cd0000        	call	_SPI_Write_Reg
 270                     ; 69 		SPI_Write_Reg(WRITE_REG + SETUP_AW, 0x03);   //设置地址宽度为5字节
 272  00bf ae0003        	ldw	x,#3
 273  00c2 a623          	ld	a,#35
 274  00c4 95            	ld	xh,a
 275  00c5 cd0000        	call	_SPI_Write_Reg
 277                     ; 70     SPI_Write_Reg(WRITE_REG + SETUP_RETR, 0x13); //自动重发延时500us + 86us,自动重发3次
 279  00c8 ae0013        	ldw	x,#19
 280  00cb a624          	ld	a,#36
 281  00cd 95            	ld	xh,a
 282  00ce cd0000        	call	_SPI_Write_Reg
 284                     ; 71     SPI_Write_Reg(WRITE_REG + RF_CH, 0x6F);      //设置信道工作为2.4GHZ，收发必须一致，频道未知!!!!!!
 286  00d1 ae006f        	ldw	x,#111
 287  00d4 a625          	ld	a,#37
 288  00d6 95            	ld	xh,a
 289  00d7 cd0000        	call	_SPI_Write_Reg
 291                     ; 72     SPI_Write_Reg(WRITE_REG + RF_SETUP, 0x07);   //设置发射速率为1MHZ，发射功率为最大值0dB	
 293  00da ae0007        	ldw	x,#7
 294  00dd a626          	ld	a,#38
 295  00df 95            	ld	xh,a
 296  00e0 cd0000        	call	_SPI_Write_Reg
 298                     ; 74     SPI_Write_Reg(WRITE_REG + RX_PW_P0, RX_PLOAD_WIDTH);//设置接收数据长度，本次设置为32字节
 300  00e3 ae0007        	ldw	x,#7
 301  00e6 a631          	ld	a,#49
 302  00e8 95            	ld	xh,a
 303  00e9 cd0000        	call	_SPI_Write_Reg
 305                     ; 75     SPI_Write_Buf(WRITE_REG + TX_ADDR, TX_ADDRESS, TX_ADR_WIDTH);   //写发送地址
 307  00ec 4b05          	push	#5
 308  00ee ae0000        	ldw	x,#_TX_ADDRESS
 309  00f1 89            	pushw	x
 310  00f2 a630          	ld	a,#48
 311  00f4 cd0000        	call	_SPI_Write_Buf
 313  00f7 5b03          	addw	sp,#3
 314                     ; 76     SPI_Write_Buf(WRITE_REG + RX_ADDR_P0, RX_ADDRESS, RX_ADR_WIDTH);//写接收端地址0(本地)
 316  00f9 4b05          	push	#5
 317  00fb ae0005        	ldw	x,#_RX_ADDRESS
 318  00fe 89            	pushw	x
 319  00ff a62a          	ld	a,#42
 320  0101 cd0000        	call	_SPI_Write_Buf
 322  0104 5b03          	addw	sp,#3
 323                     ; 78     SPI_Write_Reg(WRITE_REG + CONFIG, 0x0e);   	 //IRQ收发完成中断响应，16位CRC，上电，发送模式
 325  0106 ae000e        	ldw	x,#14
 326  0109 a620          	ld	a,#32
 327  010b 95            	ld	xh,a
 328  010c cd0000        	call	_SPI_Write_Reg
 330                     ; 80     delay_ms(1);
 332  010f ae0001        	ldw	x,#1
 333  0112 cd0000        	call	_delay_ms
 335                     ; 81     NRF24L01_CE_SET;//置'1'
 337  0115 4b02          	push	#2
 338  0117 ae5005        	ldw	x,#20485
 339  011a cd0000        	call	_GPIO_SetBits
 341  011d 84            	pop	a
 342                     ; 84     delay_ms(1);
 345  011e ae0001        	ldw	x,#1
 346  0121 cd0000        	call	_delay_ms
 348                     ; 85 }
 351  0124 81            	ret
 377                     ; 94 void SwitchToRxMode(void)
 377                     ; 95 {
 378                     	switch	.text
 379  0125               _SwitchToRxMode:
 383                     ; 96     NRF24L01_CE_RESET;//置'0'
 385  0125 4b02          	push	#2
 386  0127 ae5005        	ldw	x,#20485
 387  012a cd0000        	call	_GPIO_ResetBits
 389  012d 84            	pop	a
 390                     ; 98     SPI_Write_Reg(WRITE_REG + CONFIG, 0x0f);//上电，16位CRC，接收模式
 393  012e ae000f        	ldw	x,#15
 394  0131 a620          	ld	a,#32
 395  0133 95            	ld	xh,a
 396  0134 cd0000        	call	_SPI_Write_Reg
 398                     ; 99     SPI_Write_Reg(WRITE_REG+STATUS, 0xff);  //清除所有中断标志
 400  0137 ae00ff        	ldw	x,#255
 401  013a a627          	ld	a,#39
 402  013c 95            	ld	xh,a
 403  013d cd0000        	call	_SPI_Write_Reg
 405                     ; 101     NRF24L01_CE_SET;//置'1'
 407  0140 4b02          	push	#2
 408  0142 ae5005        	ldw	x,#20485
 409  0145 cd0000        	call	_GPIO_SetBits
 411  0148 84            	pop	a
 412                     ; 102 }
 416  0149 81            	ret
 442                     ; 111 void SwitchToTxMode(void)
 442                     ; 112 {  
 443                     	switch	.text
 444  014a               _SwitchToTxMode:
 448                     ; 113     NRF24L01_CE_RESET;//置'0'
 450  014a 4b02          	push	#2
 451  014c ae5005        	ldw	x,#20485
 452  014f cd0000        	call	_GPIO_ResetBits
 454  0152 84            	pop	a
 455                     ; 115     SPI_Write_Reg(WRITE_REG + CONFIG, 0x0e);//IRQ收发完成中断响应，16位CRC，上电，发送模式 
 458  0153 ae000e        	ldw	x,#14
 459  0156 a620          	ld	a,#32
 460  0158 95            	ld	xh,a
 461  0159 cd0000        	call	_SPI_Write_Reg
 463                     ; 116     SPI_Write_Reg(WRITE_REG+STATUS, 0xff);  //清除所有中断标志
 465  015c ae00ff        	ldw	x,#255
 466  015f a627          	ld	a,#39
 467  0161 95            	ld	xh,a
 468  0162 cd0000        	call	_SPI_Write_Reg
 470                     ; 118     NRF24L01_CE_SET;//置'1'
 472  0165 4b02          	push	#2
 473  0167 ae5005        	ldw	x,#20485
 474  016a cd0000        	call	_GPIO_SetBits
 476  016d 84            	pop	a
 477                     ; 119 }
 481  016e 81            	ret
 538                     ; 128 unsigned char NRF24L01_SendPacket(unsigned char* buf,unsigned char len)
 538                     ; 129 {
 539                     	switch	.text
 540  016f               _NRF24L01_SendPacket:
 542  016f 89            	pushw	x
 543  0170 88            	push	a
 544       00000001      OFST:	set	1
 547                     ; 132     SPI_Write_Buf(WR_TX_PLOAD, buf, len);//写32字节数据
 549  0171 7b06          	ld	a,(OFST+5,sp)
 550  0173 88            	push	a
 551  0174 89            	pushw	x
 552  0175 a6a0          	ld	a,#160
 553  0177 cd0000        	call	_SPI_Write_Buf
 555  017a 5b03          	addw	sp,#3
 556  017c               L311:
 557                     ; 135         temp = SPI_Read_Reg(STATUS);//读状态寄存器
 559  017c a607          	ld	a,#7
 560  017e cd0000        	call	_SPI_Read_Reg
 562  0181 6b01          	ld	(OFST+0,sp),a
 563                     ; 137     }while(!(temp & 0x70));//等待数据发送完成(检查接收数据中断、发送完成中断、重发次数溢出中断)
 565  0183 7b01          	ld	a,(OFST+0,sp)
 566  0185 a570          	bcp	a,#112
 567  0187 27f3          	jreq	L311
 568                     ; 139     if(SPI_Read_Reg(STATUS) & STATUS_TX_DS)//检查发送完成中断是否为'1'
 570  0189 a607          	ld	a,#7
 571  018b cd0000        	call	_SPI_Read_Reg
 573  018e a520          	bcp	a,#32
 574  0190 2714          	jreq	L121
 575                     ; 141         SPI_Write_Reg(WRITE_REG + STATUS, 0xff);//清除所有中断标志
 577  0192 ae00ff        	ldw	x,#255
 578  0195 a627          	ld	a,#39
 579  0197 95            	ld	xh,a
 580  0198 cd0000        	call	_SPI_Write_Reg
 582                     ; 142         SPI_Write_Reg(FLUSH_TX, 0x00);          //冲洗TX FIFO寄存器
 584  019b 5f            	clrw	x
 585  019c a6e1          	ld	a,#225
 586  019e 95            	ld	xh,a
 587  019f cd0000        	call	_SPI_Write_Reg
 589                     ; 143         return 1;
 591  01a2 a601          	ld	a,#1
 593  01a4 2011          	jra	L61
 594  01a6               L121:
 595                     ; 147         SPI_Write_Reg(WRITE_REG + STATUS, 0xff);//清除所有中断标志
 597  01a6 ae00ff        	ldw	x,#255
 598  01a9 a627          	ld	a,#39
 599  01ab 95            	ld	xh,a
 600  01ac cd0000        	call	_SPI_Write_Reg
 602                     ; 148         SPI_Write_Reg(FLUSH_TX, 0x00);          //冲洗TX FIFO寄存器
 604  01af 5f            	clrw	x
 605  01b0 a6e1          	ld	a,#225
 606  01b2 95            	ld	xh,a
 607  01b3 cd0000        	call	_SPI_Write_Reg
 609                     ; 149         return 0;
 611  01b6 4f            	clr	a
 613  01b7               L61:
 615  01b7 5b03          	addw	sp,#3
 616  01b9 81            	ret
 664                     ; 160 unsigned char NRF24L01_ReceivePacket(unsigned char* buf)
 664                     ; 161 {   
 665                     	switch	.text
 666  01ba               _NRF24L01_ReceivePacket:
 668  01ba 89            	pushw	x
 669  01bb 88            	push	a
 670       00000001      OFST:	set	1
 673                     ; 168 		temp = SPI_Read_Reg(STATUS);//读取状态寄存器
 675  01bc a607          	ld	a,#7
 676  01be cd0000        	call	_SPI_Read_Reg
 678  01c1 6b01          	ld	(OFST+0,sp),a
 679                     ; 170     if(temp & 0x40)//判断是否为接收数据中断
 681  01c3 7b01          	ld	a,(OFST+0,sp)
 682  01c5 a540          	bcp	a,#64
 683  01c7 2720          	jreq	L741
 684                     ; 172         SPI_Read_Buf(RD_RX_PLOAD,buf, TX_PLOAD_WIDTH);//读32字节数据
 686  01c9 4b07          	push	#7
 687  01cb 1e03          	ldw	x,(OFST+2,sp)
 688  01cd 89            	pushw	x
 689  01ce a661          	ld	a,#97
 690  01d0 cd0000        	call	_SPI_Read_Buf
 692  01d3 5b03          	addw	sp,#3
 693                     ; 173         SPI_Write_Reg(WRITE_REG + STATUS, 0xff);      //清除所有中断标志
 695  01d5 ae00ff        	ldw	x,#255
 696  01d8 a627          	ld	a,#39
 697  01da 95            	ld	xh,a
 698  01db cd0000        	call	_SPI_Write_Reg
 700                     ; 174 				SPI_Write_Reg(FLUSH_RX, 0x00);                //冲洗RX FIFO寄存器
 702  01de 5f            	clrw	x
 703  01df a6e2          	ld	a,#226
 704  01e1 95            	ld	xh,a
 705  01e2 cd0000        	call	_SPI_Write_Reg
 707                     ; 175         return temp;
 709  01e5 7b01          	ld	a,(OFST+0,sp)
 711  01e7 2001          	jra	L22
 712  01e9               L741:
 713                     ; 177     else return 0;
 715  01e9 4f            	clr	a
 717  01ea               L22:
 719  01ea 5b03          	addw	sp,#3
 720  01ec 81            	ret
 747                     ; 188 void Power_off(void)
 747                     ; 189 {
 748                     	switch	.text
 749  01ed               _Power_off:
 753                     ; 190 		NRF24L01_CE_RESET;//置'0'
 755  01ed 4b02          	push	#2
 756  01ef ae5005        	ldw	x,#20485
 757  01f2 cd0000        	call	_GPIO_ResetBits
 759  01f5 84            	pop	a
 760                     ; 192     SPI_Write_Reg(WRITE_REG + CONFIG, 0x0d);//掉电
 763  01f6 ae000d        	ldw	x,#13
 764  01f9 a620          	ld	a,#32
 765  01fb 95            	ld	xh,a
 766  01fc cd0000        	call	_SPI_Write_Reg
 768                     ; 194 		NRF24L01_CE_SET;//置'1'
 770  01ff 4b02          	push	#2
 771  0201 ae5005        	ldw	x,#20485
 772  0204 cd0000        	call	_GPIO_SetBits
 774  0207 84            	pop	a
 775                     ; 196 		delay_ms(1);
 778  0208 ae0001        	ldw	x,#1
 779  020b cd0000        	call	_delay_ms
 781                     ; 197 }
 784  020e 81            	ret
 819                     	xdef	_delay_ms
 820                     	xdef	_RX_ADDRESS
 821                     	xdef	_TX_ADDRESS
 822                     	xref	_SPI_Read_Buf
 823                     	xref	_SPI_Write_Buf
 824                     	xref	_SPI_Read_Reg
 825                     	xref	_SPI_Write_Reg
 826                     	xref	_GPIO_ResetBits
 827                     	xref	_GPIO_SetBits
 828                     	xref	_GPIO_Init
 829                     	xdef	_SwitchToTxMode
 830                     	xdef	_SwitchToRxMode
 831                     	xdef	_Power_off
 832                     	xdef	_NRF24L01_ReceivePacket
 833                     	xdef	_NRF24L01_SendPacket
 834                     	xdef	_Init_NRF24L01
 835                     	xref.b	c_x
 854                     	xref	c_imul
 855                     	end
