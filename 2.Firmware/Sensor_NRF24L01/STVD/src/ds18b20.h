#ifndef _DS18B20_H_
#define _DS18B20_H_ 

//=====================================================================
//= Includes------------头---文---件---包---含----------------------- =
//=====================================================================
#include "stm8l10x_gpio.h"

//=====================================================================
//= Private define-----------宏---定---义---------------Private macro =
//=====================================================================
#define DS18B20_DQ_OUT_L  GPIO_Init(GPIOA, GPIO_Pin_3, GPIO_Mode_Out_PP_Low_Slow)
#define DS18B20_DQ_OUT_H  GPIO_SetBits(GPIOA, GPIO_Pin_3)


#define DS18B20_DQ_IN     GPIO_Init(GPIOA, GPIO_Pin_3, GPIO_Mode_In_PU_No_IT)
#define DS18B20_DQ_PIN    GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_3)

//=====================================================================
//= function prototypes---函---数---声---明-------------------------- =
//=====================================================================
unsigned char DS18B20_Init(void);
unsigned int  DS18B20_ReadTemperature(void);

#endif