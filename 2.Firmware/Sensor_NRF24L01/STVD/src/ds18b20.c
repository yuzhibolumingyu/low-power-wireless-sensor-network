//============================================================================
//File    : ds18b20.c                                                        =
//Date    : 2014.08.17                                                       =
//Author  : zty                                                              =
//Diary   :                                                                  =
//============================================================================
#include "ds18b20.h"

//=====================================================================
//= Private functions---自---定---义---函---数----------------------- =
//=====================================================================
void _delay_us(u16 us)
{//大概1.258us
	us *= 2;//根据晶振该系数
	while(us--);
}

//=====================================================================
//= Public functions------公---共---函---数-------------------------- =
//=====================================================================
//=====================================================================
//Function : DS18B20_Init( );                                         =
//Author   : zty                                                      =
//WriteData: 2014.08.17                                               =
//EditData : 2014.08.17                                               =
//Diary    : 1、DS18B20初始化一次;                                    =
//=====================================================================
unsigned char DS18B20_Init(void)
{
		unsigned char x = 0;
		
		DS18B20_DQ_OUT_L;
		_delay_us(480);//复位脉冲
		DS18B20_DQ_IN;
		_delay_us(45);//等待存在脉冲出现，注意非零才可以检测
		while(DS18B20_DQ_PIN != 0);//读引脚
		x = 1;
		_delay_us(120);
		
		return x;
}

//=====================================================================
//Function : DS18B20_WriteByte( );                                    =
//Author   : zty                                                      =
//WriteData: 2014.08.17                                               =
//EditData : 2014.08.17                                               =
//Diary    : 1、向DS18B20写一个字节;                                  =
//=====================================================================
void DS18B20_WriteByte(unsigned char _data)
{
		unsigned char i = 0;
		for (i = 0; i < 8; i++)
		{
				DS18B20_DQ_OUT_L;
				_delay_us(10);
				if (_data & 0x01)
				{
					  DS18B20_DQ_OUT_H;
				}
				_delay_us(45);
				_data >>= 1;
				DS18B20_DQ_IN;
				_delay_us(5);
		}
}

//=====================================================================
//Function : DS18B20_ReadByte( );                                     =
//Author   : zty                                                      =
//WriteData: 2014.08.17                                               =
//EditData : 2014.08.17                                               =
//Diary    : 1、从DS18B20读一个字节;                                  =
//=====================================================================
unsigned char DS18B20_ReadByte(void)
{
	  unsigned char i = 0, _data = 0;
		for (i = 0; i < 8; i++)
		{
				DS18B20_DQ_OUT_L;
				_delay_us(2);
				DS18B20_DQ_IN;
				_data >>= 1;
				_delay_us(5);
				if (DS18B20_DQ_PIN != 0)
				{
						_data |= 0x80;
						_delay_us(30);
				}
				else
				{	
						while(DS18B20_DQ_PIN == 0);
						_delay_us(30);
			  }
		}
		 
		return _data;
}

//=====================================================================
//Function : DS18B20_ReadTemperature( );                              =
//Author   : zty                                                      =
//WriteData: 2014.08.17                                               =
//EditData : 2014.08.17                                               =
//Diary    : 1、从DS18B20读一次温度值;                                =
//=====================================================================
unsigned int DS18B20_ReadTemperature(void)
{
		unsigned char a = 0;
		unsigned char b = 0;
		unsigned int  t = 0;
		float tt = 0;
			
		while(DS18B20_Init() == 0);
		DS18B20_WriteByte(0xcc);
		DS18B20_WriteByte(0x44);
		 
		while(DS18B20_Init() == 0);
		DS18B20_WriteByte(0xcc);
		DS18B20_WriteByte(0xbe);
     
		a = DS18B20_ReadByte();
		b = DS18B20_ReadByte();
		 
		t=b;
		t<<=8;
		t=t|a;
		tt=t*0.0625;
		t = tt*10+0.5;
			
		return t;
}
