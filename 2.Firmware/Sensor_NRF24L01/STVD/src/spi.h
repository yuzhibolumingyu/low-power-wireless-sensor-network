#ifndef _SPI_H
#define _SPI_H

//=====================================================================
//= Includes------------头---文---件---包---含----------------------- =
//=====================================================================
#include "stm8l10x.h"

//=====================================================================
//= Private typedef-------类---型---定---义-------------------------- =
//=====================================================================
typedef unsigned char BYTE;

//=====================================================================
//= Private define-----------宏---定---义---------------Private macro =
//=====================================================================
#define   NRF24L01_IRQ_DDR   {GPIO_Init(GPIOB, GPIO_Pin_6, GPIO_Mode_In_PU_No_IT);}
#define   NRF24L01_MISO_DDR  {GPIO_Init(GPIOB, GPIO_Pin_4, GPIO_Mode_In_PU_No_IT);} 
#define   NRF24L01_MOSI_DDR  {GPIO_Init(GPIOB, GPIO_Pin_5, GPIO_Mode_Out_PP_High_Fast);}
#define   NRF24L01_SCK_DDR   {GPIO_Init(GPIOB, GPIO_Pin_3, GPIO_Mode_Out_PP_High_Fast);} 
#define   NRF24L01_CSN_DDR   {GPIO_Init(GPIOB, GPIO_Pin_2, GPIO_Mode_Out_PP_High_Fast);}
#define   NRF24L01_CE_DDR    {GPIO_Init(GPIOB, GPIO_Pin_1, GPIO_Mode_Out_PP_High_Fast);} 

#define   NRF24L01_IRQ_SET   {GPIO_SetBits(GPIOB, GPIO_Pin_6);}
#define   NRF24L01_MISO_SET  {GPIO_SetBits(GPIOB, GPIO_Pin_4);}
#define   NRF24L01_MOSI_SET	 {GPIO_SetBits(GPIOB, GPIO_Pin_5);}
#define   NRF24L01_SCK_SET	 {GPIO_SetBits(GPIOB, GPIO_Pin_3);}
#define   NRF24L01_CSN_SET   {GPIO_SetBits(GPIOB, GPIO_Pin_2);}
#define   NRF24L01_CE_SET    {GPIO_SetBits(GPIOB, GPIO_Pin_1);}

#define   NRF24L01_IRQ_RESET    {GPIO_ResetBits(GPIOB, GPIO_Pin_6);}
#define   NRF24L01_MISO_RESET   {GPIO_ResetBits(GPIOB, GPIO_Pin_4);}
#define   NRF24L01_MOSI_RESET	  {GPIO_ResetBits(GPIOB, GPIO_Pin_5);}
#define   NRF24L01_SCK_RESET	  {GPIO_ResetBits(GPIOB, GPIO_Pin_3);}
#define   NRF24L01_CSN_RESET    {GPIO_ResetBits(GPIOB, GPIO_Pin_2);}
#define   NRF24L01_CE_RESET     {GPIO_ResetBits(GPIOB, GPIO_Pin_1);}
	
#define   NRF24L01_IRQ_PIN	 (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_6))
#define   NRF24L01_MISO_PIN	 (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_4))	

//=====================================================================
//= function prototypes---函---数---声---明-------------------------- =
//=====================================================================
BYTE SPI_RW(BYTE _byte);
BYTE SPI_Write_Reg(BYTE reg, BYTE value);
BYTE SPI_Read_Reg(BYTE reg);
BYTE SPI_Write_Buf(BYTE reg, BYTE *pBuf, BYTE bytes);
BYTE SPI_Read_Buf(unsigned char reg, unsigned char *pBuf, unsigned char bytes);

#endif


