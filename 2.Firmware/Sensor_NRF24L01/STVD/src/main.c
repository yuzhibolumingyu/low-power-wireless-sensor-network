//============================================================================
//Project : Sensor_NRF24L01                                                  =
//Version : V1.0                                                             =
//Date    : 2015.01.17                                                       =
//Author  : zty                                                              =
//Company : todo studio                                                      =
//                                                                           =
//Chip type           : STM8L101F3P6                                         =
//Clock frequency     : 16 MHz                                               =
//Diary               :                                                      =
//============================================================================

//=====================================================================
//= Includes------------头---文---件---包---含----------------------- =
//=====================================================================
#include "stm8l10x.h"
#include "stm8l10x_clk.h"
#include "stm8l10x_awu.h"
#include "stm8l10x_gpio.h"
#include "stm8l10x_tim2.h"
#include "nrf24l01.h"
#include "ds18b20.h"

//=====================================================================
//= Private variables------变---量---定---义------------------------- =
//=====================================================================
bool ButtonPressed = FALSE;

u8 txData[7] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00};

//=====================================================================
//= function prototypes---函---数---声---明-------------------------- =
//=====================================================================
static void CLK_Config(void);
static void AWU_Config(void);
void IO_Config(void);
void GPIO_LowPower_Config(void);
void Delay(u16 nCount);
uint32_t LSIMeasurment(void);

//=====================================================================
//= -------------------------主---函---数---------------------------- =
//=====================================================================
main()
{
	  //======================变量定义=========================//
    u16  Temperature;//温度值
		u8   T_H, T_L;
		u8   Count_Sleep = 3;//睡眠计数
		
    //=====================初始化配置========================//
		
    CLK_Config();//系统时钟源配置
		Delay(1600);//1ms
		AWU_Config();//AWU配置
		Delay(1600);//1ms
		GPIO_LowPower_Config();//IO口低功耗配置
		Delay(1600);//1ms
	  //IO_Config();//IO口初始化
		//Delay(1600);//1ms
		DS18B20_Init();//DS18B20初始化
		Delay(1600);//1ms
		Init_NRF24L01();//初始化NRF24L01
		Delay(1600);//1ms
		
		enableInterrupts();//开启全局中断
		
while (1)
{
	  Count_Sleep ++;
		
		/*休眠结束后准备发送数据,LED1闪烁*/
		//GPIO_SetBits(GPIOA, GPIO_Pin_2);
		//Delay(10000);
		//GPIO_ResetBits(GPIOA, GPIO_Pin_2);
		//Delay(10000);
		
		if(Count_Sleep == 4)//30s * 4 = 120s = 2分钟唤醒一次
		{
			  Count_Sleep = 0;
				
		    /*将NRF24L01从掉电模式唤醒*/
		    SwitchToTxMode( );//切换到发送模式
		    Delay(2400);//1.5ms
		 
		    Temperature = DS18B20_ReadTemperature();
		    T_H = Temperature >> 8;
		    T_L = Temperature;
		
		    txData[0] = 0x01;
		    txData[1] = T_H;
		    txData[2] = T_L;
  		
		    if(NRF24L01_SendPacket(txData, 7) != 0)//发送字符串
        {
			  //发送成功后,LED2闪烁
        //GPIO_SetBits(GPIOA, GPIO_Pin_2);
		    //Delay(10000);
		    //GPIO_ResetBits(GPIOA, GPIO_Pin_2);
        }
		    Power_off( );//进入掉电模式
	  }
		 
		halt();/* Program halted */
};
}

//=====================================================================
//= Private functions---自---定---义---函---数----------------------- =
//=====================================================================
//=====================================================================
//Function : Delay( );                                                =
//Author   : zty                                                      =
//WriteData: 2014.08.04                                               =
//EditData : 2014.08.04                                               =
//Diary    : 1、系统延时;                                             =
//=====================================================================
void Delay(u16 nCount)
 {//延时0.629us -- 16M/HZ
   while(nCount--);
 }

//=====================================================================
//Function : CLK_Config( );                                           =
//Author   : zty                                                      =
//WriteData: 2014.08.04                                               =
//EditData : 2014.08.04                                               =
//Diary    : 1、系统时钟源选择HSI(16M/Hz);                            =
//           2、AWU时钟源只能是LSI(38K/Hz);                           =
//=====================================================================
static void CLK_Config(void)
{
    /* Initialization of the clock */
    /* Clock divider to HSI/1 */
    CLK_MasterPrescalerConfig(CLK_MasterPrescaler_HSIDiv1);
    
    /* Enable SPI clock */
    CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
    CLK_PeripheralClockConfig(CLK_Peripheral_AWU, ENABLE);
}

//=====================================================================
//Function : AWU_Config( );                                           =
//Author   : zty                                                      =
//WriteData: 2014.08.04                                               =
//EditData : 2014.08.04                                               =
//Diary    : 1、配置自动唤醒时基为30S;                                =
//=====================================================================
static void AWU_Config(void)
{
    /* Initialization of AWU */
    /* LSI calibration for accurate auto wake up time base*/
    AWU_LSICalibrationConfig(LSIMeasurment());
     
    /* The delay corresponds to the time we will stay in Halt mode */
    AWU_Init(AWU_Timebase_30s);
}

//=====================================================================
//Function : IO_Config( );                                            =
//Author   : zty                                                      =
//WriteData: 2014.08.09                                               =
//EditData : 2014.08.09                                               =
//Diary    : 1、配置IO口的输入输出模式;                               =
//=====================================================================
void IO_Config(void)
{
		GPIO_DeInit(GPIOA);
	  GPIO_Init(GPIOA, GPIO_Pin_2, GPIO_Mode_Out_PP_Low_Fast);//LED,PA.2
		//EXTI_SetPinSensitivity(EXTI_Pin_7, EXTI_Trigger_Falling_Low);
}

//=====================================================================
//Function : GPIO_LowPower_Config( );                                 =
//Author   : zty                                                      =
//WriteData: 2015.05.10                                               =
//EditData : 2015.05.10                                               =
//Diary    : 1、IO口低功耗配置;                                       =
//=====================================================================
void GPIO_LowPower_Config(void)
{
    /* Port A in output push-pull 0 */
    GPIO_Init(GPIOA, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
    /* Port B in output push-pull 0 */
    GPIO_Init(GPIOB, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
    /* Port C in output push-pull 0 */
    GPIO_Init(GPIOC, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
    /* Port D in output push-pull 0 */
    GPIO_Init(GPIOD, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
}

//===============================================================
//Function : LSIMeasurment( );                                  =
//Author   : zty                                                =
//WriteData: 2014.08.04                                         =
//EditData : 2014.08.04                                         =
//Diary    : 1、用TIM2的输入捕捉通道1来检测LSI的时钟频率;       =
//===============================================================
uint32_t LSIMeasurment(void)
{

  uint32_t lsi_freq_hz = 0x0;
  uint32_t fmaster = 0x0;
  uint16_t ICValue1 = 0x0;
  uint16_t ICValue2 = 0x0;

  /* Get master frequency */
  fmaster = CLK_GetClockFreq();

  /* Enable the LSI measurement: LSI clock connected to timer Input Capture 1 */
  AWU->CSR |= AWU_CSR_MSR;

  /* Capture only every 8 events!!! */
  TIM2_ICInit(  TIM2_Channel_1, TIM2_ICPolarity_Rising, TIM2_ICSelection_DirectTI, TIM2_ICPSC_Div8, 0x0);

  /* Enable TIM2 */
  TIM2_Cmd(ENABLE);

  /* wait a capture on cc1 */
  while ((TIM2->SR1 & (uint8_t)TIM2_FLAG_CC1) != TIM2_FLAG_CC1);
  /* Get CCR1 value*/
  ICValue1 = TIM2_GetCapture1();
  TIM2_ClearFlag(TIM2_FLAG_CC1);

  /* wait a capture on cc1 */
  while ((TIM2->SR1 & (uint8_t)TIM2_FLAG_CC1) != TIM2_FLAG_CC1);
  /* Get CCR1 value*/
  ICValue2 = TIM2_GetCapture1();
  TIM2_ClearFlag(TIM2_FLAG_CC1);

  /* Disable IC1 input capture */
  TIM2->CCER1 &= (uint8_t)(~TIM_CCER1_CC1E);
  /* Disable TIM2 */
  TIM2_Cmd(DISABLE);

  /* Compute LSI clock frequency */
  lsi_freq_hz = (8 * fmaster) / (ICValue2 - ICValue1);

  /* Disable the LSI measurement: LSI clock disconnected from timer Input Capture 1 */
  AWU->CSR &= (uint8_t)(~AWU_CSR_MSR);
  
  return (lsi_freq_hz);
}
