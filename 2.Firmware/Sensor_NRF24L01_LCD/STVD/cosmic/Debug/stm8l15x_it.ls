   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  44                     ; 52 INTERRUPT_HANDLER(NonHandledInterrupt, 0)
  44                     ; 53 {
  45                     	switch	.text
  46  0000               f_NonHandledInterrupt:
  50                     ; 57 }
  53  0000 80            	iret
  75                     ; 65 INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
  75                     ; 66 {
  76                     	switch	.text
  77  0001               f_TRAP_IRQHandler:
  81                     ; 70 }
  84  0001 80            	iret
 106                     ; 76 INTERRUPT_HANDLER(FLASH_IRQHandler, 1)
 106                     ; 77 {
 107                     	switch	.text
 108  0002               f_FLASH_IRQHandler:
 112                     ; 81 }
 115  0002 80            	iret
 138                     ; 87 INTERRUPT_HANDLER(DMA1_CHANNEL0_1_IRQHandler, 2)
 138                     ; 88 {
 139                     	switch	.text
 140  0003               f_DMA1_CHANNEL0_1_IRQHandler:
 144                     ; 92 }
 147  0003 80            	iret
 170                     ; 98 INTERRUPT_HANDLER(DMA1_CHANNEL2_3_IRQHandler, 3)
 170                     ; 99 {
 171                     	switch	.text
 172  0004               f_DMA1_CHANNEL2_3_IRQHandler:
 176                     ; 103 }
 179  0004 80            	iret
 203                     ; 109 INTERRUPT_HANDLER(RTC_CSSLSE_IRQHandler, 4)
 203                     ; 110 {	
 204                     	switch	.text
 205  0005               f_RTC_CSSLSE_IRQHandler:
 207  0005 3b0002        	push	c_x+2
 208  0008 be00          	ldw	x,c_x
 209  000a 89            	pushw	x
 210  000b 3b0002        	push	c_y+2
 211  000e be00          	ldw	x,c_y
 212  0010 89            	pushw	x
 215                     ; 114   RTC_ClearITPendingBit(RTC_IT_WUT);
 217  0011 ae0040        	ldw	x,#64
 218  0014 cd0000        	call	_RTC_ClearITPendingBit
 220                     ; 115 }
 223  0017 85            	popw	x
 224  0018 bf00          	ldw	c_y,x
 225  001a 320002        	pop	c_y+2
 226  001d 85            	popw	x
 227  001e bf00          	ldw	c_x,x
 228  0020 320002        	pop	c_x+2
 229  0023 80            	iret
 252                     ; 121 INTERRUPT_HANDLER(EXTIE_F_PVD_IRQHandler, 5)
 252                     ; 122 {
 253                     	switch	.text
 254  0024               f_EXTIE_F_PVD_IRQHandler:
 258                     ; 126 }
 261  0024 80            	iret
 283                     ; 133 INTERRUPT_HANDLER(EXTIB_G_IRQHandler, 6)
 283                     ; 134 {
 284                     	switch	.text
 285  0025               f_EXTIB_G_IRQHandler:
 289                     ; 138 }
 292  0025 80            	iret
 314                     ; 145 INTERRUPT_HANDLER(EXTID_H_IRQHandler, 7)
 314                     ; 146 {
 315                     	switch	.text
 316  0026               f_EXTID_H_IRQHandler:
 320                     ; 150 }
 323  0026 80            	iret
 345                     ; 157 INTERRUPT_HANDLER(EXTI0_IRQHandler, 8)
 345                     ; 158 {
 346                     	switch	.text
 347  0027               f_EXTI0_IRQHandler:
 351                     ; 162 }
 354  0027 80            	iret
 376                     ; 169 INTERRUPT_HANDLER(EXTI1_IRQHandler, 9)
 376                     ; 170 {
 377                     	switch	.text
 378  0028               f_EXTI1_IRQHandler:
 382                     ; 174 }
 385  0028 80            	iret
 407                     ; 181 INTERRUPT_HANDLER(EXTI2_IRQHandler, 10)
 407                     ; 182 {
 408                     	switch	.text
 409  0029               f_EXTI2_IRQHandler:
 413                     ; 186 }
 416  0029 80            	iret
 438                     ; 193 INTERRUPT_HANDLER(EXTI3_IRQHandler, 11)
 438                     ; 194 {
 439                     	switch	.text
 440  002a               f_EXTI3_IRQHandler:
 444                     ; 198 }
 447  002a 80            	iret
 469                     ; 205 INTERRUPT_HANDLER(EXTI4_IRQHandler, 12)
 469                     ; 206 {
 470                     	switch	.text
 471  002b               f_EXTI4_IRQHandler:
 475                     ; 210 }
 478  002b 80            	iret
 500                     ; 217 INTERRUPT_HANDLER(EXTI5_IRQHandler, 13)
 500                     ; 218 {
 501                     	switch	.text
 502  002c               f_EXTI5_IRQHandler:
 506                     ; 222 }
 509  002c 80            	iret
 531                     ; 229 INTERRUPT_HANDLER(EXTI6_IRQHandler, 14)
 531                     ; 230 {
 532                     	switch	.text
 533  002d               f_EXTI6_IRQHandler:
 537                     ; 234 }
 540  002d 80            	iret
 562                     ; 241 INTERRUPT_HANDLER(EXTI7_IRQHandler, 15)
 562                     ; 242 {
 563                     	switch	.text
 564  002e               f_EXTI7_IRQHandler:
 568                     ; 246 }
 571  002e 80            	iret
 593                     ; 252 INTERRUPT_HANDLER(LCD_AES_IRQHandler, 16)
 593                     ; 253 {
 594                     	switch	.text
 595  002f               f_LCD_AES_IRQHandler:
 599                     ; 257 }
 602  002f 80            	iret
 625                     ; 263 INTERRUPT_HANDLER(SWITCH_CSS_BREAK_DAC_IRQHandler, 17)
 625                     ; 264 {
 626                     	switch	.text
 627  0030               f_SWITCH_CSS_BREAK_DAC_IRQHandler:
 631                     ; 268 }
 634  0030 80            	iret
 657                     ; 275 INTERRUPT_HANDLER(ADC1_COMP_IRQHandler, 18)
 657                     ; 276 {
 658                     	switch	.text
 659  0031               f_ADC1_COMP_IRQHandler:
 663                     ; 280 }
 666  0031 80            	iret
 690                     ; 287 INTERRUPT_HANDLER(TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler, 19)
 690                     ; 288 {
 691                     	switch	.text
 692  0032               f_TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler:
 696                     ; 292 }
 699  0032 80            	iret
 722                     ; 299 INTERRUPT_HANDLER(TIM2_CC_USART2_RX_IRQHandler, 20)
 722                     ; 300 {
 723                     	switch	.text
 724  0033               f_TIM2_CC_USART2_RX_IRQHandler:
 728                     ; 304 }
 731  0033 80            	iret
 755                     ; 312 INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler, 21)
 755                     ; 313 {
 756                     	switch	.text
 757  0034               f_TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler:
 761                     ; 317 }
 764  0034 80            	iret
 787                     ; 323 INTERRUPT_HANDLER(TIM3_CC_USART3_RX_IRQHandler, 22)
 787                     ; 324 {
 788                     	switch	.text
 789  0035               f_TIM3_CC_USART3_RX_IRQHandler:
 793                     ; 328 }
 796  0035 80            	iret
 819                     ; 334 INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_COM_IRQHandler, 23)
 819                     ; 335 {
 820                     	switch	.text
 821  0036               f_TIM1_UPD_OVF_TRG_COM_IRQHandler:
 825                     ; 339 }
 828  0036 80            	iret
 850                     ; 345 INTERRUPT_HANDLER(TIM1_CC_IRQHandler, 24)
 850                     ; 346 {
 851                     	switch	.text
 852  0037               f_TIM1_CC_IRQHandler:
 856                     ; 350 }
 859  0037 80            	iret
 882                     ; 357 INTERRUPT_HANDLER(TIM4_UPD_OVF_TRG_IRQHandler, 25)
 882                     ; 358 {
 883                     	switch	.text
 884  0038               f_TIM4_UPD_OVF_TRG_IRQHandler:
 888                     ; 362 }
 891  0038 80            	iret
 913                     ; 368 INTERRUPT_HANDLER(SPI1_IRQHandler, 26)
 913                     ; 369 {
 914                     	switch	.text
 915  0039               f_SPI1_IRQHandler:
 919                     ; 373 }
 922  0039 80            	iret
 946                     ; 380 INTERRUPT_HANDLER(USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler, 27)
 946                     ; 381 {
 947                     	switch	.text
 948  003a               f_USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler:
 952                     ; 385 }
 955  003a 80            	iret
 978                     ; 392 INTERRUPT_HANDLER(USART1_RX_TIM5_CC_IRQHandler, 28)
 978                     ; 393 {
 979                     	switch	.text
 980  003b               f_USART1_RX_TIM5_CC_IRQHandler:
 984                     ; 397 }
 987  003b 80            	iret
1010                     ; 404 INTERRUPT_HANDLER(I2C1_SPI2_IRQHandler, 29)
1010                     ; 405 {
1011                     	switch	.text
1012  003c               f_I2C1_SPI2_IRQHandler:
1016                     ; 409 }
1019  003c 80            	iret
1031                     	xdef	f_I2C1_SPI2_IRQHandler
1032                     	xdef	f_USART1_RX_TIM5_CC_IRQHandler
1033                     	xdef	f_USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler
1034                     	xdef	f_SPI1_IRQHandler
1035                     	xdef	f_TIM4_UPD_OVF_TRG_IRQHandler
1036                     	xdef	f_TIM1_CC_IRQHandler
1037                     	xdef	f_TIM1_UPD_OVF_TRG_COM_IRQHandler
1038                     	xdef	f_TIM3_CC_USART3_RX_IRQHandler
1039                     	xdef	f_TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler
1040                     	xdef	f_TIM2_CC_USART2_RX_IRQHandler
1041                     	xdef	f_TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler
1042                     	xdef	f_ADC1_COMP_IRQHandler
1043                     	xdef	f_SWITCH_CSS_BREAK_DAC_IRQHandler
1044                     	xdef	f_LCD_AES_IRQHandler
1045                     	xdef	f_EXTI7_IRQHandler
1046                     	xdef	f_EXTI6_IRQHandler
1047                     	xdef	f_EXTI5_IRQHandler
1048                     	xdef	f_EXTI4_IRQHandler
1049                     	xdef	f_EXTI3_IRQHandler
1050                     	xdef	f_EXTI2_IRQHandler
1051                     	xdef	f_EXTI1_IRQHandler
1052                     	xdef	f_EXTI0_IRQHandler
1053                     	xdef	f_EXTID_H_IRQHandler
1054                     	xdef	f_EXTIB_G_IRQHandler
1055                     	xdef	f_EXTIE_F_PVD_IRQHandler
1056                     	xdef	f_RTC_CSSLSE_IRQHandler
1057                     	xdef	f_DMA1_CHANNEL2_3_IRQHandler
1058                     	xdef	f_DMA1_CHANNEL0_1_IRQHandler
1059                     	xdef	f_FLASH_IRQHandler
1060                     	xdef	f_TRAP_IRQHandler
1061                     	xdef	f_NonHandledInterrupt
1062                     	xref	_RTC_ClearITPendingBit
1063                     	xref.b	c_x
1064                     	xref.b	c_y
1083                     	end
