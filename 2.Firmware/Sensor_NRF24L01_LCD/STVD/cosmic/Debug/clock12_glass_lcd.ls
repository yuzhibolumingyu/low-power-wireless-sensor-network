   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  15                     .const:	section	.text
  16  0000               _mask:
  17  0000 f000          	dc.w	-4096
  18  0002 0f00          	dc.w	3840
  19  0004 00f0          	dc.w	240
  20  0006 000f          	dc.w	15
  21  0008               _shift:
  22  0008 0c            	dc.b	12
  23  0009 08            	dc.b	8
  24  000a 04            	dc.b	4
  25  000b 00            	dc.b	0
  26  000c               _LetterMap:
  27  000c 4d70          	dc.w	19824
  28  000e 6469          	dc.w	25705
  29  0010 0212          	dc.w	530
  30  0012 6449          	dc.w	25673
  31  0014 4911          	dc.w	18705
  32  0016 4910          	dc.w	18704
  33  0018 4171          	dc.w	16753
  34  001a 0d70          	dc.w	3440
  35  001c 6009          	dc.w	24585
  36  001e 0451          	dc.w	1105
  37  0020 0b12          	dc.w	2834
  38  0022 0111          	dc.w	273
  39  0024 8750          	dc.w	-30896
  40  0026 8552          	dc.w	-31406
  41  0028 4551          	dc.w	17745
  42  002a 4d30          	dc.w	19760
  43  002c 4553          	dc.w	17747
  44  002e 4d32          	dc.w	19762
  45  0030 4961          	dc.w	18785
  46  0032 6008          	dc.w	24584
  47  0034 0551          	dc.w	1361
  48  0036 0390          	dc.w	912
  49  0038 05d2          	dc.w	1490
  50  003a 8282          	dc.w	-32126
  51  003c 8208          	dc.w	-32248
  52  003e 4281          	dc.w	17025
  53  0040               _NumberMap:
  54  0040 3132          	dc.w	12594
  55  0042 2020          	dc.w	8224
  56  0044 3212          	dc.w	12818
  57  0046 3222          	dc.w	12834
  58  0048 2320          	dc.w	8992
  59  004a 1322          	dc.w	4898
  60  004c 1332          	dc.w	4914
  61  004e 3020          	dc.w	12320
  62  0050 3332          	dc.w	13106
  63  0052 3322          	dc.w	13090
 100                     ; 97 void LCD_GLASS_Init(void)
 100                     ; 98 {
 102                     	switch	.text
 103  0000               _LCD_GLASS_Init:
 107                     ; 110     CLK_PeripheralClockConfig(CLK_Peripheral_LCD, ENABLE);
 109  0000 ae0001        	ldw	x,#1
 110  0003 a613          	ld	a,#19
 111  0005 95            	ld	xh,a
 112  0006 cd0000        	call	_CLK_PeripheralClockConfig
 114                     ; 113     LCD_Init(LCD_Prescaler_2, LCD_Divider_31, LCD_Duty_1_4,
 114                     ; 114              LCD_Bias_1_3, LCD_VoltageSource_Internal);
 116  0009 4b00          	push	#0
 117  000b 4b00          	push	#0
 118  000d 4b06          	push	#6
 119  000f ae000f        	ldw	x,#15
 120  0012 a610          	ld	a,#16
 121  0014 95            	ld	xh,a
 122  0015 cd0000        	call	_LCD_Init
 124  0018 5b03          	addw	sp,#3
 125                     ; 119     LCD_PortMaskConfig(LCD_PortMaskRegister_1, 0xFF);
 127  001a ae00ff        	ldw	x,#255
 128  001d a601          	ld	a,#1
 129  001f 95            	ld	xh,a
 130  0020 cd0000        	call	_LCD_PortMaskConfig
 132                     ; 124     LCD_ContrastConfig(LCD_Contrast_3V0);
 134  0023 a608          	ld	a,#8
 135  0025 cd0000        	call	_LCD_ContrastConfig
 137                     ; 125     LCD_DeadTimeConfig(LCD_DeadTime_0);
 139  0028 4f            	clr	a
 140  0029 cd0000        	call	_LCD_DeadTimeConfig
 142                     ; 126     LCD_PulseOnDurationConfig(LCD_PulseOnDuration_1);
 144  002c a620          	ld	a,#32
 145  002e cd0000        	call	_LCD_PulseOnDurationConfig
 147                     ; 129     LCD_Cmd(ENABLE);
 149  0031 a601          	ld	a,#1
 150  0033 cd0000        	call	_LCD_Cmd
 152                     ; 130 }
 155  0036 81            	ret
 262                     ; 145 void LCD_GLASS_WriteChar(uint8_t* ch, Point_Typedef point,
 262                     ; 146                          Apostrophe_Typedef apostrophe, uint8_t position)
 262                     ; 147 {
 263                     	switch	.text
 264  0037               _LCD_GLASS_WriteChar:
 266  0037 89            	pushw	x
 267       00000000      OFST:	set	0
 270                     ; 148   Convert(ch, point, apostrophe);
 272  0038 7b06          	ld	a,(OFST+6,sp)
 273  003a 88            	push	a
 274  003b 7b06          	ld	a,(OFST+6,sp)
 275  003d 88            	push	a
 276  003e cd02a8        	call	L3_Convert
 278  0041 85            	popw	x
 279                     ; 150   switch (position)
 281  0042 7b07          	ld	a,(OFST+7,sp)
 283                     ; 215     default:
 283                     ; 216       break;
 284  0044 4d            	tnz	a
 285  0045 2713          	jreq	L32
 286  0047 4a            	dec	a
 287  0048 2766          	jreq	L52
 288  004a 4a            	dec	a
 289  004b 2603          	jrne	L01
 290  004d cc010a        	jp	L72
 291  0050               L01:
 292  0050 4a            	dec	a
 293  0051 2603          	jrne	L21
 294  0053 cc015e        	jp	L13
 295  0056               L21:
 296  0056 acb401b4      	jpf	L111
 297  005a               L32:
 298                     ; 153     case 0:
 298                     ; 154       LCD->RAM[LCD_RAMRegister_1] &= 0xFC;
 300  005a c6540d        	ld	a,21517
 301  005d a4fc          	and	a,#252
 302  005f c7540d        	ld	21517,a
 303                     ; 155       LCD->RAM[LCD_RAMRegister_1] |= (uint8_t)(digit[0] & 0x03);// 1A 1B
 305  0062 b600          	ld	a,_digit
 306  0064 a403          	and	a,#3
 307  0066 ca540d        	or	a,21517
 308  0069 c7540d        	ld	21517,a
 309                     ; 157       LCD->RAM[LCD_RAMRegister_4] &= 0xCF;
 311  006c c65410        	ld	a,21520
 312  006f a4cf          	and	a,#207
 313  0071 c75410        	ld	21520,a
 314                     ; 158       LCD->RAM[LCD_RAMRegister_4] |= (uint8_t)((digit[1]<<4) & 0x30);// 1F 1G
 316  0074 b601          	ld	a,_digit+1
 317  0076 97            	ld	xl,a
 318  0077 a610          	ld	a,#16
 319  0079 42            	mul	x,a
 320  007a 9f            	ld	a,xl
 321  007b a430          	and	a,#48
 322  007d ca5410        	or	a,21520
 323  0080 c75410        	ld	21520,a
 324                     ; 160       LCD->RAM[LCD_RAMRegister_8] &= 0xFC;
 326  0083 c65414        	ld	a,21524
 327  0086 a4fc          	and	a,#252
 328  0088 c75414        	ld	21524,a
 329                     ; 161       LCD->RAM[LCD_RAMRegister_8] |= (uint8_t)(digit[2] & 0x03);// 1E 1C
 331  008b b602          	ld	a,_digit+2
 332  008d a403          	and	a,#3
 333  008f ca5414        	or	a,21524
 334  0092 c75414        	ld	21524,a
 335                     ; 163       LCD->RAM[LCD_RAMRegister_11] &= 0xCF;
 337  0095 c65417        	ld	a,21527
 338  0098 a4cf          	and	a,#207
 339  009a c75417        	ld	21527,a
 340                     ; 164       LCD->RAM[LCD_RAMRegister_11] |= (uint8_t)((digit[3]<<4) & 0x30);// 1P1 1D
 342  009d b603          	ld	a,_digit+3
 343  009f 97            	ld	xl,a
 344  00a0 a610          	ld	a,#16
 345  00a2 42            	mul	x,a
 346  00a3 9f            	ld	a,xl
 347  00a4 a430          	and	a,#48
 348  00a6 ca5417        	or	a,21527
 349  00a9 c75417        	ld	21527,a
 350                     ; 166       break;
 352  00ac acb401b4      	jpf	L111
 353  00b0               L52:
 354                     ; 169     case 1:
 354                     ; 170 			LCD->RAM[LCD_RAMRegister_1] &= 0xF3;
 356  00b0 c6540d        	ld	a,21517
 357  00b3 a4f3          	and	a,#243
 358  00b5 c7540d        	ld	21517,a
 359                     ; 171       LCD->RAM[LCD_RAMRegister_1] |= (uint8_t)((digit[0]<<2) & 0x0C);// 2A 2B
 361  00b8 b600          	ld	a,_digit
 362  00ba 48            	sll	a
 363  00bb 48            	sll	a
 364  00bc a40c          	and	a,#12
 365  00be ca540d        	or	a,21517
 366  00c1 c7540d        	ld	21517,a
 367                     ; 173       LCD->RAM[LCD_RAMRegister_4] &= 0x3F;
 369  00c4 c65410        	ld	a,21520
 370  00c7 a43f          	and	a,#63
 371  00c9 c75410        	ld	21520,a
 372                     ; 174       LCD->RAM[LCD_RAMRegister_4] |= (uint8_t)((digit[1]<<6) & 0xC0);// 2F 2G
 374  00cc b601          	ld	a,_digit+1
 375  00ce 97            	ld	xl,a
 376  00cf a640          	ld	a,#64
 377  00d1 42            	mul	x,a
 378  00d2 9f            	ld	a,xl
 379  00d3 a4c0          	and	a,#192
 380  00d5 ca5410        	or	a,21520
 381  00d8 c75410        	ld	21520,a
 382                     ; 176       LCD->RAM[LCD_RAMRegister_8] &= 0xF3;
 384  00db c65414        	ld	a,21524
 385  00de a4f3          	and	a,#243
 386  00e0 c75414        	ld	21524,a
 387                     ; 177       LCD->RAM[LCD_RAMRegister_8] |= (uint8_t)((digit[2]<<2) & 0x0C);// 2E 2C
 389  00e3 b602          	ld	a,_digit+2
 390  00e5 48            	sll	a
 391  00e6 48            	sll	a
 392  00e7 a40c          	and	a,#12
 393  00e9 ca5414        	or	a,21524
 394  00ec c75414        	ld	21524,a
 395                     ; 179       LCD->RAM[LCD_RAMRegister_11] &= 0x3F;
 397  00ef c65417        	ld	a,21527
 398  00f2 a43f          	and	a,#63
 399  00f4 c75417        	ld	21527,a
 400                     ; 180       LCD->RAM[LCD_RAMRegister_11] |= (uint8_t)((digit[3]<<6) & 0xC0);// 2P1 2D
 402  00f7 b603          	ld	a,_digit+3
 403  00f9 97            	ld	xl,a
 404  00fa a640          	ld	a,#64
 405  00fc 42            	mul	x,a
 406  00fd 9f            	ld	a,xl
 407  00fe a4c0          	and	a,#192
 408  0100 ca5417        	or	a,21527
 409  0103 c75417        	ld	21527,a
 410                     ; 182       break;
 412  0106 acb401b4      	jpf	L111
 413  010a               L72:
 414                     ; 185     case 2:
 414                     ; 186       LCD->RAM[LCD_RAMRegister_1] &= 0xCF;
 416  010a c6540d        	ld	a,21517
 417  010d a4cf          	and	a,#207
 418  010f c7540d        	ld	21517,a
 419                     ; 187       LCD->RAM[LCD_RAMRegister_1] |= (uint8_t)((digit[0]<<4) & 0x30);// 3A 3B
 421  0112 b600          	ld	a,_digit
 422  0114 97            	ld	xl,a
 423  0115 a610          	ld	a,#16
 424  0117 42            	mul	x,a
 425  0118 9f            	ld	a,xl
 426  0119 a430          	and	a,#48
 427  011b ca540d        	or	a,21517
 428  011e c7540d        	ld	21517,a
 429                     ; 189       LCD->RAM[LCD_RAMRegister_5] &= 0xFC;
 431  0121 c65411        	ld	a,21521
 432  0124 a4fc          	and	a,#252
 433  0126 c75411        	ld	21521,a
 434                     ; 190       LCD->RAM[LCD_RAMRegister_5] |= (uint8_t)(digit[1] & 0x03);// 3F 3G
 436  0129 b601          	ld	a,_digit+1
 437  012b a403          	and	a,#3
 438  012d ca5411        	or	a,21521
 439  0130 c75411        	ld	21521,a
 440                     ; 192       LCD->RAM[LCD_RAMRegister_8] &= 0xCF;
 442  0133 c65414        	ld	a,21524
 443  0136 a4cf          	and	a,#207
 444  0138 c75414        	ld	21524,a
 445                     ; 193       LCD->RAM[LCD_RAMRegister_8] |= (uint8_t)((digit[2]<<4) & 0x30);// 3E 3C
 447  013b b602          	ld	a,_digit+2
 448  013d 97            	ld	xl,a
 449  013e a610          	ld	a,#16
 450  0140 42            	mul	x,a
 451  0141 9f            	ld	a,xl
 452  0142 a430          	and	a,#48
 453  0144 ca5414        	or	a,21524
 454  0147 c75414        	ld	21524,a
 455                     ; 195       LCD->RAM[LCD_RAMRegister_12] &= 0xFC;
 457  014a c65418        	ld	a,21528
 458  014d a4fc          	and	a,#252
 459  014f c75418        	ld	21528,a
 460                     ; 196       LCD->RAM[LCD_RAMRegister_12] |= (uint8_t)(digit[3] & 0x03);// 3P1 3D
 462  0152 b603          	ld	a,_digit+3
 463  0154 a403          	and	a,#3
 464  0156 ca5418        	or	a,21528
 465  0159 c75418        	ld	21528,a
 466                     ; 197       break;
 468  015c 2056          	jra	L111
 469  015e               L13:
 470                     ; 200     case 3:
 470                     ; 201       LCD->RAM[LCD_RAMRegister_1] &= 0x3F;
 472  015e c6540d        	ld	a,21517
 473  0161 a43f          	and	a,#63
 474  0163 c7540d        	ld	21517,a
 475                     ; 202       LCD->RAM[LCD_RAMRegister_1] |= (uint8_t)((digit[0]<<6) & 0xC0);// 4A 4B
 477  0166 b600          	ld	a,_digit
 478  0168 97            	ld	xl,a
 479  0169 a640          	ld	a,#64
 480  016b 42            	mul	x,a
 481  016c 9f            	ld	a,xl
 482  016d a4c0          	and	a,#192
 483  016f ca540d        	or	a,21517
 484  0172 c7540d        	ld	21517,a
 485                     ; 204       LCD->RAM[LCD_RAMRegister_5] &= 0xF3;
 487  0175 c65411        	ld	a,21521
 488  0178 a4f3          	and	a,#243
 489  017a c75411        	ld	21521,a
 490                     ; 205       LCD->RAM[LCD_RAMRegister_5] |= (uint8_t)((digit[1]<<2) & 0x0C);// 4F 4G
 492  017d b601          	ld	a,_digit+1
 493  017f 48            	sll	a
 494  0180 48            	sll	a
 495  0181 a40c          	and	a,#12
 496  0183 ca5411        	or	a,21521
 497  0186 c75411        	ld	21521,a
 498                     ; 207       LCD->RAM[LCD_RAMRegister_8] &= 0x3F;
 500  0189 c65414        	ld	a,21524
 501  018c a43f          	and	a,#63
 502  018e c75414        	ld	21524,a
 503                     ; 208       LCD->RAM[LCD_RAMRegister_8] |= (uint8_t)((digit[2]<<6) & 0xC0);// 4E 4C
 505  0191 b602          	ld	a,_digit+2
 506  0193 97            	ld	xl,a
 507  0194 a640          	ld	a,#64
 508  0196 42            	mul	x,a
 509  0197 9f            	ld	a,xl
 510  0198 a4c0          	and	a,#192
 511  019a ca5414        	or	a,21524
 512  019d c75414        	ld	21524,a
 513                     ; 210       LCD->RAM[LCD_RAMRegister_12] &= 0xF3;
 515  01a0 c65418        	ld	a,21528
 516  01a3 a4f3          	and	a,#243
 517  01a5 c75418        	ld	21528,a
 518                     ; 211       LCD->RAM[LCD_RAMRegister_12] |= (uint8_t)((digit[3]<<2) & 0x0C);// 4P1 4D
 520  01a8 b603          	ld	a,_digit+3
 521  01aa 48            	sll	a
 522  01ab 48            	sll	a
 523  01ac a40c          	and	a,#12
 524  01ae ca5418        	or	a,21528
 525  01b1 c75418        	ld	21528,a
 526                     ; 213       break;
 528  01b4               L33:
 529                     ; 215     default:
 529                     ; 216       break;
 531  01b4               L111:
 532                     ; 218 }
 535  01b4 85            	popw	x
 536  01b5 81            	ret
 582                     ; 225 void LCD_GLASS_DisplayString(uint8_t* ptr)
 582                     ; 226 {
 583                     	switch	.text
 584  01b6               _LCD_GLASS_DisplayString:
 586  01b6 89            	pushw	x
 587  01b7 5203          	subw	sp,#3
 588       00000003      OFST:	set	3
 591                     ; 227   uint8_t i = 0x00;
 593  01b9 0f03          	clr	(OFST+0,sp)
 595  01bb 2017          	jra	L141
 596  01bd               L531:
 597                     ; 233     LCD_GLASS_WriteChar(ptr, POINT_OFF, APOSTROPHE_OFF, i);
 599  01bd 7b03          	ld	a,(OFST+0,sp)
 600  01bf 88            	push	a
 601  01c0 4b00          	push	#0
 602  01c2 4b00          	push	#0
 603  01c4 1e07          	ldw	x,(OFST+4,sp)
 604  01c6 cd0037        	call	_LCD_GLASS_WriteChar
 606  01c9 5b03          	addw	sp,#3
 607                     ; 236     ptr++;
 609  01cb 1e04          	ldw	x,(OFST+1,sp)
 610  01cd 1c0001        	addw	x,#1
 611  01d0 1f04          	ldw	(OFST+1,sp),x
 612                     ; 239     i++;
 614  01d2 0c03          	inc	(OFST+0,sp)
 615  01d4               L141:
 616                     ; 230   while ((*ptr != 0) & (i < 8))
 618  01d4 7b03          	ld	a,(OFST+0,sp)
 619  01d6 a108          	cp	a,#8
 620  01d8 2405          	jruge	L61
 621  01da ae0001        	ldw	x,#1
 622  01dd 2001          	jra	L02
 623  01df               L61:
 624  01df 5f            	clrw	x
 625  01e0               L02:
 626  01e0 1f01          	ldw	(OFST-2,sp),x
 627  01e2 1e04          	ldw	x,(OFST+1,sp)
 628  01e4 7d            	tnz	(x)
 629  01e5 2705          	jreq	L22
 630  01e7 ae0001        	ldw	x,#1
 631  01ea 2001          	jra	L42
 632  01ec               L22:
 633  01ec 5f            	clrw	x
 634  01ed               L42:
 635  01ed 01            	rrwa	x,a
 636  01ee 1402          	and	a,(OFST-1,sp)
 637  01f0 01            	rrwa	x,a
 638  01f1 1401          	and	a,(OFST-2,sp)
 639  01f3 01            	rrwa	x,a
 640  01f4 a30000        	cpw	x,#0
 641  01f7 26c4          	jrne	L531
 642                     ; 241 }
 645  01f9 5b05          	addw	sp,#5
 646  01fb 81            	ret
 681                     ; 248 void LCD_GLASS_ClearChar(uint8_t position)
 681                     ; 249 {
 682                     	switch	.text
 683  01fc               _LCD_GLASS_ClearChar:
 687                     ; 250   switch (position)
 690                     ; 284     default:
 690                     ; 285       break;
 691  01fc 4d            	tnz	a
 692  01fd 270d          	jreq	L541
 693  01ff 4a            	dec	a
 694  0200 272c          	jreq	L741
 695  0202 4a            	dec	a
 696  0203 274b          	jreq	L151
 697  0205 4a            	dec	a
 698  0206 276a          	jreq	L351
 699  0208 ac920292      	jpf	L771
 700  020c               L541:
 701                     ; 253     case 0:
 701                     ; 254       LCD->RAM[LCD_RAMRegister_1] &= 0xFC;
 703  020c c6540d        	ld	a,21517
 704  020f a4fc          	and	a,#252
 705  0211 c7540d        	ld	21517,a
 706                     ; 255       LCD->RAM[LCD_RAMRegister_4] &= 0xCF;
 708  0214 c65410        	ld	a,21520
 709  0217 a4cf          	and	a,#207
 710  0219 c75410        	ld	21520,a
 711                     ; 256       LCD->RAM[LCD_RAMRegister_8] &= 0xFC;
 713  021c c65414        	ld	a,21524
 714  021f a4fc          	and	a,#252
 715  0221 c75414        	ld	21524,a
 716                     ; 257       LCD->RAM[LCD_RAMRegister_11] &= 0xCF;
 718  0224 c65417        	ld	a,21527
 719  0227 a4cf          	and	a,#207
 720  0229 c75417        	ld	21527,a
 721                     ; 258       break;
 723  022c 2064          	jra	L771
 724  022e               L741:
 725                     ; 261     case 1:
 725                     ; 262       LCD->RAM[LCD_RAMRegister_1] &= 0xF3;
 727  022e c6540d        	ld	a,21517
 728  0231 a4f3          	and	a,#243
 729  0233 c7540d        	ld	21517,a
 730                     ; 263       LCD->RAM[LCD_RAMRegister_4] &= 0x3F;
 732  0236 c65410        	ld	a,21520
 733  0239 a43f          	and	a,#63
 734  023b c75410        	ld	21520,a
 735                     ; 264       LCD->RAM[LCD_RAMRegister_8] &= 0xF3;
 737  023e c65414        	ld	a,21524
 738  0241 a4f3          	and	a,#243
 739  0243 c75414        	ld	21524,a
 740                     ; 265       LCD->RAM[LCD_RAMRegister_11] &= 0x3F;
 742  0246 c65417        	ld	a,21527
 743  0249 a43f          	and	a,#63
 744  024b c75417        	ld	21527,a
 745                     ; 266       break;
 747  024e 2042          	jra	L771
 748  0250               L151:
 749                     ; 269     case 2:
 749                     ; 270       LCD->RAM[LCD_RAMRegister_1] &= 0xCF;
 751  0250 c6540d        	ld	a,21517
 752  0253 a4cf          	and	a,#207
 753  0255 c7540d        	ld	21517,a
 754                     ; 271       LCD->RAM[LCD_RAMRegister_5] &= 0xFC;
 756  0258 c65411        	ld	a,21521
 757  025b a4fc          	and	a,#252
 758  025d c75411        	ld	21521,a
 759                     ; 272       LCD->RAM[LCD_RAMRegister_8] &= 0xCF;
 761  0260 c65414        	ld	a,21524
 762  0263 a4cf          	and	a,#207
 763  0265 c75414        	ld	21524,a
 764                     ; 273       LCD->RAM[LCD_RAMRegister_12] &= 0xFC;
 766  0268 c65418        	ld	a,21528
 767  026b a4fc          	and	a,#252
 768  026d c75418        	ld	21528,a
 769                     ; 274       break;
 771  0270 2020          	jra	L771
 772  0272               L351:
 773                     ; 277     case 3:
 773                     ; 278       LCD->RAM[LCD_RAMRegister_1] &= 0x3F;
 775  0272 c6540d        	ld	a,21517
 776  0275 a43f          	and	a,#63
 777  0277 c7540d        	ld	21517,a
 778                     ; 279       LCD->RAM[LCD_RAMRegister_5] &= 0xF3;
 780  027a c65411        	ld	a,21521
 781  027d a4f3          	and	a,#243
 782  027f c75411        	ld	21521,a
 783                     ; 280       LCD->RAM[LCD_RAMRegister_8] &= 0x3F;
 785  0282 c65414        	ld	a,21524
 786  0285 a43f          	and	a,#63
 787  0287 c75414        	ld	21524,a
 788                     ; 281       LCD->RAM[LCD_RAMRegister_12] &= 0xF3;
 790  028a c65418        	ld	a,21528
 791  028d a4f3          	and	a,#243
 792  028f c75418        	ld	21528,a
 793                     ; 282       break;
 795  0292               L551:
 796                     ; 284     default:
 796                     ; 285       break;
 798  0292               L771:
 799                     ; 287 }
 802  0292 81            	ret
 836                     ; 294 void LCD_GLASS_Clear(void)
 836                     ; 295 {
 837                     	switch	.text
 838  0293               _LCD_GLASS_Clear:
 840  0293 88            	push	a
 841       00000001      OFST:	set	1
 844                     ; 296   uint8_t counter = 0;
 846                     ; 297   for (counter = 0;counter < 0x0C; counter++)
 848  0294 0f01          	clr	(OFST+0,sp)
 849  0296               L712:
 850                     ; 299     LCD->RAM[counter] =  LCD_RAM_RESET_VALUE;
 852  0296 7b01          	ld	a,(OFST+0,sp)
 853  0298 5f            	clrw	x
 854  0299 97            	ld	xl,a
 855  029a 724f540c      	clr	(21516,x)
 856                     ; 297   for (counter = 0;counter < 0x0C; counter++)
 858  029e 0c01          	inc	(OFST+0,sp)
 861  02a0 7b01          	ld	a,(OFST+0,sp)
 862  02a2 a10c          	cp	a,#12
 863  02a4 25f0          	jrult	L712
 864                     ; 301 }
 867  02a6 84            	pop	a
 868  02a7 81            	ret
 955                     ; 313 static void Convert(uint8_t* c, Point_Typedef point, Apostrophe_Typedef apostrophe)
 955                     ; 314 {
 956                     	switch	.text
 957  02a8               L3_Convert:
 959  02a8 89            	pushw	x
 960  02a9 5207          	subw	sp,#7
 961       00000007      OFST:	set	7
 964                     ; 315   uint16_t ch = 0 , tmp = 0;
 966  02ab 5f            	clrw	x
 967  02ac 1f05          	ldw	(OFST-2,sp),x
 970                     ; 319   if ((*c < 0x5B)&(*c > 0x40))
 972  02ae 1e08          	ldw	x,(OFST+1,sp)
 973  02b0 f6            	ld	a,(x)
 974  02b1 a141          	cp	a,#65
 975  02b3 2505          	jrult	L43
 976  02b5 ae0001        	ldw	x,#1
 977  02b8 2001          	jra	L63
 978  02ba               L43:
 979  02ba 5f            	clrw	x
 980  02bb               L63:
 981  02bb 1f01          	ldw	(OFST-6,sp),x
 982  02bd 1e08          	ldw	x,(OFST+1,sp)
 983  02bf f6            	ld	a,(x)
 984  02c0 a15b          	cp	a,#91
 985  02c2 2405          	jruge	L04
 986  02c4 ae0001        	ldw	x,#1
 987  02c7 2001          	jra	L24
 988  02c9               L04:
 989  02c9 5f            	clrw	x
 990  02ca               L24:
 991  02ca 01            	rrwa	x,a
 992  02cb 1402          	and	a,(OFST-5,sp)
 993  02cd 01            	rrwa	x,a
 994  02ce 1401          	and	a,(OFST-6,sp)
 995  02d0 01            	rrwa	x,a
 996  02d1 a30000        	cpw	x,#0
 997  02d4 270e          	jreq	L762
 998                     ; 321     ch = LetterMap[*c-0x41];
1000  02d6 1e08          	ldw	x,(OFST+1,sp)
1001  02d8 f6            	ld	a,(x)
1002  02d9 5f            	clrw	x
1003  02da 97            	ld	xl,a
1004  02db 58            	sllw	x
1005  02dc 1d0082        	subw	x,#130
1006  02df de000c        	ldw	x,(_LetterMap,x)
1007  02e2 1f05          	ldw	(OFST-2,sp),x
1008  02e4               L762:
1009                     ; 324   if ((*c < 0x3A)&(*c > 0x2F))
1011  02e4 1e08          	ldw	x,(OFST+1,sp)
1012  02e6 f6            	ld	a,(x)
1013  02e7 a130          	cp	a,#48
1014  02e9 2505          	jrult	L44
1015  02eb ae0001        	ldw	x,#1
1016  02ee 2001          	jra	L64
1017  02f0               L44:
1018  02f0 5f            	clrw	x
1019  02f1               L64:
1020  02f1 1f01          	ldw	(OFST-6,sp),x
1021  02f3 1e08          	ldw	x,(OFST+1,sp)
1022  02f5 f6            	ld	a,(x)
1023  02f6 a13a          	cp	a,#58
1024  02f8 2405          	jruge	L05
1025  02fa ae0001        	ldw	x,#1
1026  02fd 2001          	jra	L25
1027  02ff               L05:
1028  02ff 5f            	clrw	x
1029  0300               L25:
1030  0300 01            	rrwa	x,a
1031  0301 1402          	and	a,(OFST-5,sp)
1032  0303 01            	rrwa	x,a
1033  0304 1401          	and	a,(OFST-6,sp)
1034  0306 01            	rrwa	x,a
1035  0307 a30000        	cpw	x,#0
1036  030a 270e          	jreq	L172
1037                     ; 326     ch = NumberMap[*c-0x30];
1039  030c 1e08          	ldw	x,(OFST+1,sp)
1040  030e f6            	ld	a,(x)
1041  030f 5f            	clrw	x
1042  0310 97            	ld	xl,a
1043  0311 58            	sllw	x
1044  0312 1d0060        	subw	x,#96
1045  0315 de0040        	ldw	x,(_NumberMap,x)
1046  0318 1f05          	ldw	(OFST-2,sp),x
1047  031a               L172:
1048                     ; 329   if (*c == 0x20)
1050  031a 1e08          	ldw	x,(OFST+1,sp)
1051  031c f6            	ld	a,(x)
1052  031d a120          	cp	a,#32
1053  031f 2603          	jrne	L372
1054                     ; 331     ch = 0x00;
1056  0321 5f            	clrw	x
1057  0322 1f05          	ldw	(OFST-2,sp),x
1058  0324               L372:
1059                     ; 334   if (point == POINT_ON)
1061  0324 7b0c          	ld	a,(OFST+5,sp)
1062  0326 a101          	cp	a,#1
1063  0328 2606          	jrne	L572
1064                     ; 336     ch |= 0x0001;
1066  032a 7b06          	ld	a,(OFST-1,sp)
1067  032c aa01          	or	a,#1
1068  032e 6b06          	ld	(OFST-1,sp),a
1069  0330               L572:
1070                     ; 340   if (apostrophe == APOSTROPHE_ON)
1072  0330 7b0d          	ld	a,(OFST+6,sp)
1073  0332 a101          	cp	a,#1
1074  0334 2606          	jrne	L772
1075                     ; 342     ch |= 0x0001;
1077  0336 7b06          	ld	a,(OFST-1,sp)
1078  0338 aa01          	or	a,#1
1079  033a 6b06          	ld	(OFST-1,sp),a
1080  033c               L772:
1081                     ; 345   for (i = 0;i < 4; i++)
1083  033c 0f07          	clr	(OFST+0,sp)
1084  033e               L103:
1085                     ; 347     tmp = ch & mask[i];
1087  033e 7b07          	ld	a,(OFST+0,sp)
1088  0340 5f            	clrw	x
1089  0341 97            	ld	xl,a
1090  0342 58            	sllw	x
1091  0343 de0000        	ldw	x,(_mask,x)
1092  0346 01            	rrwa	x,a
1093  0347 1406          	and	a,(OFST-1,sp)
1094  0349 01            	rrwa	x,a
1095  034a 1405          	and	a,(OFST-2,sp)
1096  034c 01            	rrwa	x,a
1097  034d 1f03          	ldw	(OFST-4,sp),x
1098                     ; 348     digit[i] = (uint8_t)(tmp >> (uint8_t)shift[i]);
1100  034f 7b07          	ld	a,(OFST+0,sp)
1101  0351 5f            	clrw	x
1102  0352 97            	ld	xl,a
1103  0353 7b07          	ld	a,(OFST+0,sp)
1104  0355 905f          	clrw	y
1105  0357 9097          	ld	yl,a
1106  0359 90d60008      	ld	a,(_shift,y)
1107  035d 1603          	ldw	y,(OFST-4,sp)
1108  035f 4d            	tnz	a
1109  0360 2705          	jreq	L45
1110  0362               L65:
1111  0362 9054          	srlw	y
1112  0364 4a            	dec	a
1113  0365 26fb          	jrne	L65
1114  0367               L45:
1115  0367 909f          	ld	a,yl
1116  0369 e700          	ld	(_digit,x),a
1117                     ; 345   for (i = 0;i < 4; i++)
1119  036b 0c07          	inc	(OFST+0,sp)
1122  036d 7b07          	ld	a,(OFST+0,sp)
1123  036f a104          	cp	a,#4
1124  0371 25cb          	jrult	L103
1125                     ; 350 }
1128  0373 5b09          	addw	sp,#9
1129  0375 81            	ret
1194                     	xdef	_NumberMap
1195                     	xdef	_LetterMap
1196                     	switch	.ubsct
1197  0000               _digit:
1198  0000 00000000      	ds.b	4
1199                     	xdef	_digit
1200                     	xdef	_shift
1201                     	xdef	_mask
1202                     	xdef	_LCD_GLASS_Clear
1203                     	xdef	_LCD_GLASS_ClearChar
1204                     	xdef	_LCD_GLASS_DisplayString
1205                     	xdef	_LCD_GLASS_WriteChar
1206                     	xdef	_LCD_GLASS_Init
1207                     	xref	_LCD_ContrastConfig
1208                     	xref	_LCD_DeadTimeConfig
1209                     	xref	_LCD_PulseOnDurationConfig
1210                     	xref	_LCD_Cmd
1211                     	xref	_LCD_PortMaskConfig
1212                     	xref	_LCD_Init
1213                     	xref	_CLK_PeripheralClockConfig
1233                     	end
