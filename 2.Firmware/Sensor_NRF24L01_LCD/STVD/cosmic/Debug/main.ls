   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.8.32 - 23 Mar 2010
   3                     ; Generator V4.3.4 - 23 Mar 2010
  15                     	bsct
  16  0000               _ButtonPressed:
  17  0000 00            	dc.b	0
  18  0001               _LCD_String:
  19  0001 30            	dc.b	48
  20  0002 31            	dc.b	49
  21  0003 32            	dc.b	50
  22  0004 33            	dc.b	51
  23  0005               _txData:
  24  0005 00            	dc.b	0
  25  0006 00            	dc.b	0
  26  0007 00            	dc.b	0
  27  0008 00            	dc.b	0
  28  0009 00            	dc.b	0
  29  000a 00            	dc.b	0
  30  000b 00            	dc.b	0
 104                     ; 45 main()
 104                     ; 46 {
 106                     	switch	.text
 107  0000               _main:
 109  0000 5204          	subw	sp,#4
 110       00000004      OFST:	set	4
 113                     ; 52     CLK_Config();//系统时钟源配置
 115  0002 cd00f0        	call	L3_CLK_Config
 117                     ; 53 		Delay(1600);//1ms
 119  0005 ae0640        	ldw	x,#1600
 120  0008 cd00de        	call	_Delay
 122                     ; 54 		RTC_Config();//RTC配置
 124  000b cd0108        	call	L5_RTC_Config
 126                     ; 55 		Delay(1600);//1ms
 128  000e ae0640        	ldw	x,#1600
 129  0011 cd00de        	call	_Delay
 131                     ; 56 		GPIO_LowPower_Config();//IO口低功耗配置
 133  0014 cd0146        	call	_GPIO_LowPower_Config
 135                     ; 57 		Delay(1600);//1ms
 137  0017 ae0640        	ldw	x,#1600
 138  001a cd00de        	call	_Delay
 140                     ; 58 		LCD_GLASS_Init();//配置LCD的驱动IO口
 142  001d cd0000        	call	_LCD_GLASS_Init
 144                     ; 59 		Delay(1600);//1ms
 146  0020 ae0640        	ldw	x,#1600
 147  0023 cd00de        	call	_Delay
 149                     ; 62 	  DS18B20_Init();//DS18B20初始化
 151  0026 cd0000        	call	_DS18B20_Init
 153                     ; 63 		Delay(1600);//1ms
 155  0029 ae0640        	ldw	x,#1600
 156  002c cd00de        	call	_Delay
 158                     ; 64 		Init_NRF24L01();//初始化NRF24L01
 160  002f cd0000        	call	_Init_NRF24L01
 162                     ; 65 		Delay(1600);//1ms
 164  0032 ae0640        	ldw	x,#1600
 165  0035 cd00de        	call	_Delay
 167                     ; 67 		enableInterrupts();//开启全局中断
 170  0038 9a            rim
 172  0039               L34:
 173                     ; 75 		Temperature = DS18B20_ReadTemperature();
 175  0039 cd0000        	call	_DS18B20_ReadTemperature
 177  003c 1f03          	ldw	(OFST-1,sp),x
 178                     ; 76 		T_H = Temperature >> 8;
 180  003e 7b03          	ld	a,(OFST-1,sp)
 181  0040 6b01          	ld	(OFST-3,sp),a
 182                     ; 77 		T_L = Temperature;
 184  0042 7b04          	ld	a,(OFST+0,sp)
 185  0044 6b02          	ld	(OFST-2,sp),a
 186                     ; 79 		txData[0] = 0x05;
 188  0046 35050005      	mov	_txData,#5
 189                     ; 80 		txData[1] = T_H;
 191  004a 7b01          	ld	a,(OFST-3,sp)
 192  004c b706          	ld	_txData+1,a
 193                     ; 81 		txData[2] = T_L;
 195  004e 7b02          	ld	a,(OFST-2,sp)
 196  0050 b707          	ld	_txData+2,a
 197                     ; 83 		LCD_String[3] = 'C';
 199  0052 35430004      	mov	_LCD_String+3,#67
 200                     ; 84 		LCD_String[2] = (Temperature%10)+0x30;//小数部分
 202  0056 1e03          	ldw	x,(OFST-1,sp)
 203  0058 90ae000a      	ldw	y,#10
 204  005c 65            	divw	x,y
 205  005d 51            	exgw	x,y
 206  005e 1c0030        	addw	x,#48
 207  0061 01            	rrwa	x,a
 208  0062 b703          	ld	_LCD_String+2,a
 209  0064 02            	rlwa	x,a
 210                     ; 85 		Temperature = Temperature / 10;
 212  0065 1e03          	ldw	x,(OFST-1,sp)
 213  0067 90ae000a      	ldw	y,#10
 214  006b 65            	divw	x,y
 215  006c 1f03          	ldw	(OFST-1,sp),x
 216                     ; 86 		LCD_String[1] = (Temperature%10)+0x30;//整数个位
 218  006e 1e03          	ldw	x,(OFST-1,sp)
 219  0070 90ae000a      	ldw	y,#10
 220  0074 65            	divw	x,y
 221  0075 51            	exgw	x,y
 222  0076 1c0030        	addw	x,#48
 223  0079 01            	rrwa	x,a
 224  007a b702          	ld	_LCD_String+1,a
 225  007c 02            	rlwa	x,a
 226                     ; 87 		LCD_String[0] = (Temperature/10)+0x30;//整数十位 
 228  007d 1e03          	ldw	x,(OFST-1,sp)
 229  007f 90ae000a      	ldw	y,#10
 230  0083 65            	divw	x,y
 231  0084 1c0030        	addw	x,#48
 232  0087 01            	rrwa	x,a
 233  0088 b701          	ld	_LCD_String,a
 234  008a 02            	rlwa	x,a
 235                     ; 89 		LCD_GLASS_WriteChar(&LCD_String[0], POINT_OFF, APOSTROPHE_OFF, 0);
 237  008b 4b00          	push	#0
 238  008d 4b00          	push	#0
 239  008f 4b00          	push	#0
 240  0091 ae0001        	ldw	x,#_LCD_String
 241  0094 cd0000        	call	_LCD_GLASS_WriteChar
 243  0097 5b03          	addw	sp,#3
 244                     ; 90 		LCD_GLASS_WriteChar(&LCD_String[1], POINT_OFF, APOSTROPHE_OFF, 1);
 246  0099 4b01          	push	#1
 247  009b 4b00          	push	#0
 248  009d 4b00          	push	#0
 249  009f ae0002        	ldw	x,#_LCD_String+1
 250  00a2 cd0000        	call	_LCD_GLASS_WriteChar
 252  00a5 5b03          	addw	sp,#3
 253                     ; 91 		LCD_GLASS_WriteChar(&LCD_String[2], POINT_ON , APOSTROPHE_OFF, 2);
 255  00a7 4b02          	push	#2
 256  00a9 4b00          	push	#0
 257  00ab 4b01          	push	#1
 258  00ad ae0003        	ldw	x,#_LCD_String+2
 259  00b0 cd0000        	call	_LCD_GLASS_WriteChar
 261  00b3 5b03          	addw	sp,#3
 262                     ; 92 		LCD_GLASS_WriteChar(&LCD_String[3], POINT_OFF, APOSTROPHE_OFF, 3);
 264  00b5 4b03          	push	#3
 265  00b7 4b00          	push	#0
 266  00b9 4b00          	push	#0
 267  00bb ae0004        	ldw	x,#_LCD_String+3
 268  00be cd0000        	call	_LCD_GLASS_WriteChar
 270  00c1 5b03          	addw	sp,#3
 271                     ; 95 		SwitchToTxMode( );//切换到发送模式
 273  00c3 cd0000        	call	_SwitchToTxMode
 275                     ; 96 		Delay(2400);//1.5ms
 277  00c6 ae0960        	ldw	x,#2400
 278  00c9 ad13          	call	_Delay
 280                     ; 98 		if(NRF24L01_SendPacket(txData, 7) != 0)//发送字符串
 282  00cb 4b07          	push	#7
 283  00cd ae0005        	ldw	x,#_txData
 284  00d0 cd0000        	call	_NRF24L01_SendPacket
 286  00d3 5b01          	addw	sp,#1
 287  00d5 4d            	tnz	a
 288                     ; 105 		Power_off( );//进入掉电模式		
 290  00d6 cd0000        	call	_Power_off
 292                     ; 107 		halt();/*CPU in Active Halt mode */
 295  00d9 8e            halt
 299  00da ac390039      	jpf	L34
 333                     ; 121 void Delay(u16 nCount)
 333                     ; 122 {   //延时0.629us -- 16MHz
 334                     	switch	.text
 335  00de               _Delay:
 337  00de 89            	pushw	x
 338       00000000      OFST:	set	0
 341  00df               L17:
 342                     ; 123     while(nCount--);
 344  00df 1e01          	ldw	x,(OFST+1,sp)
 345  00e1 1d0001        	subw	x,#1
 346  00e4 1f01          	ldw	(OFST+1,sp),x
 347  00e6 1c0001        	addw	x,#1
 348  00e9 a30000        	cpw	x,#0
 349  00ec 26f1          	jrne	L17
 350                     ; 124 }
 353  00ee 85            	popw	x
 354  00ef 81            	ret
 381                     ; 134 static void CLK_Config(void)
 381                     ; 135 {
 382                     	switch	.text
 383  00f0               L3_CLK_Config:
 387                     ; 137 		CLK_SYSCLKSourceSwitchCmd(ENABLE);
 389  00f0 a601          	ld	a,#1
 390  00f2 cd0000        	call	_CLK_SYSCLKSourceSwitchCmd
 392                     ; 139 		CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);//HSI = 16M/Hz
 394  00f5 a601          	ld	a,#1
 395  00f7 cd0000        	call	_CLK_SYSCLKSourceConfig
 397                     ; 141     CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);//分频只会影响系统时钟源
 399  00fa 4f            	clr	a
 400  00fb cd0000        	call	_CLK_SYSCLKDivConfig
 402                     ; 143     CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_2);
 404  00fe ae0020        	ldw	x,#32
 405  0101 a604          	ld	a,#4
 406  0103 95            	ld	xh,a
 407  0104 cd0000        	call	_CLK_RTCClockConfig
 409                     ; 145 }
 412  0107 81            	ret
 440                     ; 154 static void RTC_Config(void)
 440                     ; 155 {
 441                     	switch	.text
 442  0108               L5_RTC_Config:
 446                     ; 157     CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
 448  0108 ae0001        	ldw	x,#1
 449  010b a612          	ld	a,#18
 450  010d 95            	ld	xh,a
 451  010e cd0000        	call	_CLK_PeripheralClockConfig
 453                     ; 160     RTC_WakeUpCmd(DISABLE);//配置唤醒单位前,必须先禁用才能设置!!!
 455  0111 4f            	clr	a
 456  0112 cd0000        	call	_RTC_WakeUpCmd
 458                     ; 164     RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
 460  0115 a604          	ld	a,#4
 461  0117 cd0000        	call	_RTC_WakeUpClockConfig
 463                     ; 167 	  RTC_SetWakeUpCounter(60);
 465  011a ae003c        	ldw	x,#60
 466  011d cd0000        	call	_RTC_SetWakeUpCounter
 468                     ; 169     RTC_ITConfig(RTC_IT_WUT, ENABLE);
 470  0120 4b01          	push	#1
 471  0122 ae0040        	ldw	x,#64
 472  0125 cd0000        	call	_RTC_ITConfig
 474  0128 84            	pop	a
 475                     ; 172 	  RTC_WakeUpCmd(ENABLE);
 477  0129 a601          	ld	a,#1
 478  012b cd0000        	call	_RTC_WakeUpCmd
 480                     ; 173 }
 483  012e 81            	ret
 507                     ; 182 void IO_Config(void)
 507                     ; 183 {
 508                     	switch	.text
 509  012f               _IO_Config:
 513                     ; 184 	  GPIO_Init(GPIOB, GPIO_Pin_1, GPIO_Mode_Out_PP_Low_Slow);//LED,PB.1
 515  012f 4bc0          	push	#192
 516  0131 4b02          	push	#2
 517  0133 ae5005        	ldw	x,#20485
 518  0136 cd0000        	call	_GPIO_Init
 520  0139 85            	popw	x
 521                     ; 185 		GPIO_Init(GPIOB, GPIO_Pin_2, GPIO_Mode_Out_PP_Low_Slow);//LED,PB.2
 523  013a 4bc0          	push	#192
 524  013c 4b04          	push	#4
 525  013e ae5005        	ldw	x,#20485
 526  0141 cd0000        	call	_GPIO_Init
 528  0144 85            	popw	x
 529                     ; 186 }
 532  0145 81            	ret
 557                     ; 195 void GPIO_LowPower_Config(void)
 557                     ; 196 {
 558                     	switch	.text
 559  0146               _GPIO_LowPower_Config:
 563                     ; 198     GPIO_Init(GPIOA, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
 565  0146 4bc0          	push	#192
 566  0148 4bff          	push	#255
 567  014a ae5000        	ldw	x,#20480
 568  014d cd0000        	call	_GPIO_Init
 570  0150 85            	popw	x
 571                     ; 200     GPIO_Init(GPIOB, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
 573  0151 4bc0          	push	#192
 574  0153 4bff          	push	#255
 575  0155 ae5005        	ldw	x,#20485
 576  0158 cd0000        	call	_GPIO_Init
 578  015b 85            	popw	x
 579                     ; 202     GPIO_Init(GPIOC, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
 581  015c 4bc0          	push	#192
 582  015e 4bff          	push	#255
 583  0160 ae500a        	ldw	x,#20490
 584  0163 cd0000        	call	_GPIO_Init
 586  0166 85            	popw	x
 587                     ; 204     GPIO_Init(GPIOD, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
 589  0167 4bc0          	push	#192
 590  0169 4bff          	push	#255
 591  016b ae500f        	ldw	x,#20495
 592  016e cd0000        	call	_GPIO_Init
 594  0171 85            	popw	x
 595                     ; 205 }
 598  0172 81            	ret
 663                     	xdef	_main
 664                     	xdef	_GPIO_LowPower_Config
 665                     	xdef	_Delay
 666                     	xdef	_IO_Config
 667                     	xdef	_txData
 668                     	xdef	_LCD_String
 669                     	xdef	_ButtonPressed
 670                     	xref	_LCD_GLASS_WriteChar
 671                     	xref	_LCD_GLASS_Init
 672                     	xref	_DS18B20_ReadTemperature
 673                     	xref	_DS18B20_Init
 674                     	xref	_SwitchToTxMode
 675                     	xref	_Power_off
 676                     	xref	_NRF24L01_SendPacket
 677                     	xref	_Init_NRF24L01
 678                     	xref	_RTC_ITConfig
 679                     	xref	_RTC_WakeUpCmd
 680                     	xref	_RTC_SetWakeUpCounter
 681                     	xref	_RTC_WakeUpClockConfig
 682                     	xref	_GPIO_Init
 683                     	xref	_CLK_PeripheralClockConfig
 684                     	xref	_CLK_RTCClockConfig
 685                     	xref	_CLK_SYSCLKSourceSwitchCmd
 686                     	xref	_CLK_SYSCLKDivConfig
 687                     	xref	_CLK_SYSCLKSourceConfig
 706                     	end
