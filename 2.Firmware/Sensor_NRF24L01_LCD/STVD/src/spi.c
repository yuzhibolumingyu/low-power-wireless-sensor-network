//============================================================================
//File    : SPI.c                                                            =
//Date    : 2014.07.15                                                       =
//Author  : zty                                                              =
//Diary   :                                                                  =
//============================================================================
#include "SPI.h"

//=====================================================================
//= Public functions------公---共---函---数-------------------------- =
//=====================================================================

//=====================================================================
//Function : SPI_RW( );                                               =
//Author   : zty                                                      =
//WriteData: 2014.07.15                                               =
//EditData : 2014.07.15                                               =
//Diary    : 1、写一个字节到NRF24L01并返回读取的字节;                 =
//           2、这里读引脚MISO返回的非零值并不是1，所以不能用:        =
//              "(NRF24L01_MISO_PIN) >> 7",编译器会报错误值;          =
//=====================================================================
BYTE SPI_RW(BYTE _byte)
{  
    BYTE bit_ctr;
    for(bit_ctr = 0;bit_ctr < 8; bit_ctr++)// output 8-bit
    {
        if (_byte & 0x80)// output 'byte', MSB to MOSI
					  {NRF24L01_MOSI_SET;}//置'1'
        else
					  {NRF24L01_MOSI_RESET;}//置'0'
   	     
   	    _byte = (_byte << 1);            // shift next bit into MSB..
   	    NRF24L01_SCK_SET;                // Set SCK high..
   	    NRF24L01_MISO_SET;
				if(NRF24L01_MISO_PIN)_byte |= 1; // capture current MISO bit                  
   	    NRF24L01_SCK_RESET;              // ..then set SCK low again
    }
    return(_byte);           	           // return read byte
}

//=====================================================================
//Function : SPI_Write_Reg( );                                        =
//Author   : zty                                                      =
//WriteData: 2014.07.15                                               =
//EditData : 2014.07.15                                               =
//Diary    : 1、把值'value'写到寄存器'reg'中;                         =
//=====================================================================
BYTE SPI_Write_Reg(BYTE reg, BYTE value)
{       
    BYTE status;
  	NRF24L01_CSN_RESET;  // CSN low, init SPI transaction
  	status = SPI_RW(reg);// select register
  	SPI_RW(value);       // ..and write value to it..
  	NRF24L01_CSN_SET;    // CSN high again
  	return(status);      // return nRF24L01 status byte
}

//=====================================================================
//Function : SPI_Read_Reg( );                                         =
//Author   : zty                                                      =
//WriteData: 2014.07.15                                               =
//EditData : 2014.07.15                                               =
//Diary    : 1、从寄存器'reg'中读取一个字节并返回;                    =
//=====================================================================
BYTE SPI_Read_Reg(BYTE reg)
{
	  BYTE value;
  	NRF24L01_CSN_RESET;// CSN low, initialize SPI communication...
  	SPI_RW(reg);       // Select register to read from..
  	value = SPI_RW(0); // ..then read registervalue
  	NRF24L01_CSN_SET;  // CSN high, terminate SPI communication
  	return(value);     // return register value
}

//=====================================================================
//Function : SPI_Write_Buf( );                                        =
//Author   : zty                                                      =
//WriteData: 2014.07.15                                               =
//EditData : 2014.07.15                                               =
//Diary    : 1、把pBuf缓冲区中的byte字节数据写入NRF24L01;             =
//=====================================================================
BYTE SPI_Write_Buf(BYTE reg, BYTE *pBuf, BYTE bytes)
{
	BYTE status, byte_ctr;
  	NRF24L01_CSN_RESET;      // Set CSN low, init SPI tranaction
  	status = SPI_RW(reg);    // Select register to write to and read status byte
  	for(byte_ctr = 0;byte_ctr < bytes; byte_ctr++)//then write all byte in buffer(*pBuf)
	{
	    SPI_RW(*pBuf++);
	}
   	NRF24L01_CSN_SET;        // Set CSN high again
  	return(status);          // return nRF24L01 status byte
}

//=====================================================================
//Function : SPI_Read_Buf( );                                         =
//Author   : zty                                                      =
//WriteData: 2014.07.15                                               =
//EditData : 2014.07.15                                               =
//Diary    : 1、从NRF24L01中读取bytes字节数据到pBuf缓冲区中;          =
//=====================================================================
BYTE SPI_Read_Buf(unsigned char reg, unsigned char *pBuf, unsigned char bytes)     
{                                                           
	BYTE status, byte_ctr;                              
    
  	NRF24L01_CSN_RESET;              // Set CSN l
  	status = SPI_RW(reg);            // Select re
    
  	for(byte_ctr = 0;byte_ctr < bytes; byte_ctr++)
        {	
    	    pBuf[byte_ctr] = SPI_RW(0);// Perform SPI_RW to 
        }
	       
  	NRF24L01_CSN_SET;                // Set CSN high again
  	return(status);                  // return BK2411 
}  
