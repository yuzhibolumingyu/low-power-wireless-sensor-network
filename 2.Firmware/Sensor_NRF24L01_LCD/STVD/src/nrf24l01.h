#ifndef _NRF24L01_H_
#define _NRF24L01_H_ 

//=====================================================================
//= Private variables------变---量---定---义------------------------- =
//=====================================================================
//=====================================================================
//= Private define-----------宏---定---义---------------Private macro =
//=====================================================================
//NRF24L01
#define TX_ADR_WIDTH    5   	// 5 uints TX address width
#define RX_ADR_WIDTH    5   	// 5 uints RX address width
#define TX_PLOAD_WIDTH  32  	// 20 uints TX payload
#define RX_PLOAD_WIDTH  32  	// 20 uints TX payload

//NRF24L01寄存器指令
#define READ_REG        0x00  	// 读寄存器指令
#define WRITE_REG       0x20 	  // 写寄存器指令
#define RD_RX_PLOAD     0x61  	// 读取接收数据指令
#define WR_TX_PLOAD     0xA0  	// 写待发数据指令
#define FLUSH_TX        0xE1 	  // 冲洗发送 FIFO指令
#define FLUSH_RX        0xE2  	// 冲洗接收 FIFO指令
#define REUSE_TX_PL     0xE3  	// 定义重复装载数据指令
#define NOP             0xFF  	// 保留 

#define W_TX_PAYLOAD_NOACK_CMD	0xb0
#define W_ACK_PAYLOAD_CMD	0xa8
#define ACTIVATE_CMD		0x50
#define R_RX_PL_WID_CMD		0x60

//SPI(nRF24L01)寄存器地址
#define CONFIG          0x00  // 配置收发状态，CRC校验模式以及收发状态响应方式
#define EN_AA           0x01  // 自动应答功能设置
#define EN_RXADDR       0x02  // 可用信道设置
#define SETUP_AW        0x03  // 收发地址宽度设置
#define SETUP_RETR      0x04  // 自动重发功能设置
#define RF_CH           0x05  // 工作频率设置
#define RF_SETUP        0x06  // 发射速率、功耗功能设置
#define STATUS          0x07  // 状态寄存器
#define OBSERVE_TX      0x08  // 发送监测功能
#define CD              0x09  // 地址检测           
#define RX_ADDR_P0      0x0A  // 频道0接收数据地址
#define RX_ADDR_P1      0x0B  // 频道1接收数据地址
#define RX_ADDR_P2      0x0C  // 频道2接收数据地址
#define RX_ADDR_P3      0x0D  // 频道3接收数据地址
#define RX_ADDR_P4      0x0E  // 频道4接收数据地址
#define RX_ADDR_P5      0x0F  // 频道5接收数据地址
#define TX_ADDR         0x10  // 发送地址寄存器
#define RX_PW_P0        0x11  // 接收频道0接收数据长度
#define RX_PW_P1        0x12  // 接收频道1接收数据长度
#define RX_PW_P2        0x13  // 接收频道2接收数据长度
#define RX_PW_P3        0x14  // 接收频道3接收数据长度
#define RX_PW_P4        0x15  // 接收频道4接收数据长度
#define RX_PW_P5        0x16  // 接收频道5接收数据长度
#define FIFO_STATUS     0x17  // FIFO栈入栈出状态寄存器设置

//中断状态
#define STATUS_RX_DR   0x40
#define STATUS_TX_DS   0x20
#define STATUS_MAX_RT  0x10
#define STATUS_TX_FULL 0x01

//入栈出栈状态,FIFO_STATUS
#define FIFO_STATUS_TX_REUSE 0x40
#define FIFO_STATUS_TX_FULL  0x20
#define FIFO_STATUS_TX_EMPTY 0x10

#define FIFO_STATUS_RX_FULL  0x02
#define FIFO_STATUS_RX_EMPTY 0x01

//ASK命令和寄存器, ASK COMMAND and REGISTER
#define REG2_BUFFER_COUNT   0x02
#define REG3_STATUS         0x03
#define ASK_Rx_Full       (1<<7)
#define ASK_Rx_Empty      (1<<6)
#define ASK_Tx_Full       (1<<5)
#define ASK_Tx_Empty      (1<<4)
#define ASK__Reserved     (1<<3)
#define ASK__Irqn_Tx_Err  (1<<2)
#define ASK_Irqn_Rx_Done  (1<<1)
#define ASK_Irqn_Tx_Done  (1<<0)
	
#define ASK_Irqn_ALL (ASK__Irqn_Tx_Err|ASK_Irqn_Rx_Done|ASK_Irqn_Tx_Done)


//=====================================================================
//= function prototypes---函---数---声---明-------------------------- =
//=====================================================================	
void Init_NRF24L01(void);//NRF24L01初始化函数 
unsigned char NRF24L01_SendPacket(unsigned char* buf,unsigned char len);//发送数据包
unsigned char NRF24L01_ReceivePacket(unsigned char* buf);//接收数据包 
void Power_off(void);//进入掉电模式
void SwitchToRxMode(void);//切换到接收模式 
void SwitchToTxMode(void);//切换到发送模式

#endif