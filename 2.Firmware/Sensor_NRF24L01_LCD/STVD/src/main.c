//============================================================================
//Project : Sensor_NRF24L01_LCD                                              =
//Version : V1.0                                                             =
//Date    : 2015.02.02                                                       =
//Author  : zty                                                              =
//Company : todo studio                                                      =
//                                                                           =
//Chip type           : STM8L152K4T6                                         =
//Clock frequency     : SYS=16/1=16MHz ,RTC=38K/Hz                           =
//Diary               :                                                      =
//============================================================================

//=====================================================================
//= Includes------------头---文---件---包---含----------------------- =
//=====================================================================
#include "stm8l15x.h"
#include "stm8l15x_clk.h"
#include "stm8l15x_rtc.h"
#include "stm8l15x_gpio.h"
#include "nrf24l01.h"
#include "ds18b20.h"
#include "clock12_glass_lcd.h"

//=====================================================================
//= Private variables------变---量---定---义------------------------- =
//=====================================================================
bool ButtonPressed = FALSE;

uint8_t LCD_String[4] = "0123";

u8 txData[7] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00};

//=====================================================================
//= function prototypes---函---数---声---明-------------------------- =
//=====================================================================
static void CLK_Config(void);
static void RTC_Config(void);
void IO_Config(void);
void Delay(u16 nCount);
void GPIO_LowPower_Config(void);

//=====================================================================
//= -------------------------主---函---数---------------------------- =
//=====================================================================
main()
{
	  //======================变量定义=========================//
		u16  Temperature;//温度值
		u8  T_H, T_L;
		
    //=====================初始化配置========================//
    CLK_Config();//系统时钟源配置
		Delay(1600);//1ms
		RTC_Config();//RTC配置
		Delay(1600);//1ms
		GPIO_LowPower_Config();//IO口低功耗配置
		Delay(1600);//1ms
		LCD_GLASS_Init();//配置LCD的驱动IO口
		Delay(1600);//1ms
	  //IO_Config();//IO口初始化
		//Delay(1600);//1ms
	  DS18B20_Init();//DS18B20初始化
		Delay(1600);//1ms
		Init_NRF24L01();//初始化NRF24L01
		Delay(1600);//1ms
		
		enableInterrupts();//开启全局中断
    
		/* Display "0123" string on LCD glass before entering in Halt mode*/
    //LCD_GLASS_DisplayString("0123");
		//LCD_GLASS_WriteChar('0', POINT_OFF, APOSTROPHE_OFF, 1);
		
while(1)
{		 
		Temperature = DS18B20_ReadTemperature();
		T_H = Temperature >> 8;
		T_L = Temperature;
		
		txData[0] = 0x05;
		txData[1] = T_H;
		txData[2] = T_L;
  	
		LCD_String[3] = 'C';
		LCD_String[2] = (Temperature%10)+0x30;//小数部分
		Temperature = Temperature / 10;
		LCD_String[1] = (Temperature%10)+0x30;//整数个位
		LCD_String[0] = (Temperature/10)+0x30;//整数十位 
		
		LCD_GLASS_WriteChar(&LCD_String[0], POINT_OFF, APOSTROPHE_OFF, 0);
		LCD_GLASS_WriteChar(&LCD_String[1], POINT_OFF, APOSTROPHE_OFF, 1);
		LCD_GLASS_WriteChar(&LCD_String[2], POINT_ON , APOSTROPHE_OFF, 2);
		LCD_GLASS_WriteChar(&LCD_String[3], POINT_OFF, APOSTROPHE_OFF, 3);
		
    /*将NRF24L01从掉电模式唤醒*/
		SwitchToTxMode( );//切换到发送模式
		Delay(2400);//1.5ms
		
		if(NRF24L01_SendPacket(txData, 7) != 0)//发送字符串
    {
        //GPIO_SetBits(GPIOB, GPIO_Pin_2);
		    //Delay(5000);
		    //GPIO_ResetBits(GPIOB, GPIO_Pin_2);
		    //Delay(5000); 
    }
		Power_off( );//进入掉电模式		
		
		halt();/*CPU in Active Halt mode */
};
}

//=====================================================================
//= Private functions---自---定---义---函---数----------------------- =
//=====================================================================
//=====================================================================
//Function : Delay( );                                                =
//Author   : zty                                                      =
//WriteData: 2014.08.04                                               =
//EditData : 2014.08.04                                               =
//Diary    : 1、系统延时;                                             =
//=====================================================================
void Delay(u16 nCount)
{   //延时0.629us -- 16MHz
    while(nCount--);
}

//=====================================================================
//Function : CLK_Config( );                                           =
//Author   : zty                                                      =
//WriteData: 2014.08.04                                               =
//EditData : 2014.08.04                                               =
//Diary    : 1、系统时钟源选择HSI(16M/hz);                            =
//           2、RTC时钟选择LSI(38K/hz);                               =
//=====================================================================
static void CLK_Config(void)
{
    /* Initialization of the clock */
		CLK_SYSCLKSourceSwitchCmd(ENABLE);
		/* Select HSI as system clock source */
		CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);//HSI = 16M/Hz
    /* Clock divider to HSI/1 */
    CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);//分频只会影响系统时钟源
		/* Enable RTC clock *///LSI = 38K/Hz
    CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_2);
		/* 分频会让下面的唤醒时间加倍 */
}

//=====================================================================
//Function : RTC_Config( );                                           =
//Author   : zty                                                      =
//WriteData: 2014.08.16                                               =
//EditData : 2014.08.16                                               =
//Diary    : 1、配置RTC外围;                                          =
//=====================================================================
static void RTC_Config(void)
{
	  /* Enable RTC clock */
    CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
	 
	  /* Disable WakeUp */
    RTC_WakeUpCmd(DISABLE);//配置唤醒单位前,必须先禁用才能设置!!!
   
    /* Configures the RTC */
    //RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);
    RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
	  
    /* RTC will wake-up from halt every 120s */	
	  RTC_SetWakeUpCounter(60);
	  /* Enable wake up unit Interrupt */
    RTC_ITConfig(RTC_IT_WUT, ENABLE);
	 
	  /* Enable WakeUp */
	  RTC_WakeUpCmd(ENABLE);
}

//=====================================================================
//Function : IO_Config( );                                            =
//Author   : zty                                                      =
//WriteData: 2014.08.09                                               =
//EditData : 2014.08.09                                               =
//Diary    : 1、配置IO口模式;                                         =
//=====================================================================
void IO_Config(void)
{
	  GPIO_Init(GPIOB, GPIO_Pin_1, GPIO_Mode_Out_PP_Low_Slow);//LED,PB.1
		GPIO_Init(GPIOB, GPIO_Pin_2, GPIO_Mode_Out_PP_Low_Slow);//LED,PB.2
}

//=====================================================================
//Function : GPIO_LowPower_Config( );                                 =
//Author   : zty                                                      =
//WriteData: 2015.05.10                                               =
//EditData : 2015.05.10                                               =
//Diary    : 1、IO口低功耗配置;                                       =
//=====================================================================
void GPIO_LowPower_Config(void)
{
    /* Port A in output push-pull 0 */
    GPIO_Init(GPIOA, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
    /* Port B in output push-pull 0 */
    GPIO_Init(GPIOB, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
    /* Port C in output push-pull 0 */
    GPIO_Init(GPIOC, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
    /* Port D in output push-pull 0 */
    GPIO_Init(GPIOD, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow);
}
