//============================================================================
//File    : clock12_glass_lcd.h                                              =
//Date    : 2015.03.02                                                       =
//Author  : zty                                                              =
//Diary   :                                                                  =
//============================================================================
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CLOCK12_GLASS_LCD_H
#define __CLOCK12_GLASS_LCD_H

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_lcd.h"

/* Private define ------------------------------------------------------------*/
typedef enum
{
  POINT_OFF = 0,
  POINT_ON = 1
}Point_Typedef;

typedef enum
{
  APOSTROPHE_OFF = 0,
  APOSTROPHE_ON = 1
}Apostrophe_Typedef;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void LCD_GLASS_Init(void);
void LCD_GLASS_WriteChar(uint8_t* ch, Point_Typedef point, Apostrophe_Typedef apostrophe, uint8_t position);
void LCD_GLASS_DisplayString(uint8_t* ptr);
void LCD_GLASS_ClearChar(uint8_t position);
void LCD_GLASS_Clear(void);

#endif
