//============================================================================
//File    : NRF24L01.c                                                       =
//Date    : 2014.07.15                                                       =
//Author  : zty                                                              =
//Diary   :                                                                  =
//============================================================================
#include "NRF24L01.h"
#include "SPI.h"

//=====================================================================
//= Private variables------变---量---定---义------------------------- =
//=====================================================================
//NRF24L01地址
unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB3,0x43,0x10,0x10,0x01};//发送地址(目标)
unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB3,0x43,0x10,0x10,0x01};//接收地址0(本地)

//=====================================================================
//= Private functions---自---定---义---函---数----------------------- =
//=====================================================================
//=====================================================================
//Function : delay_ms( );                                             =
//Author   : zty                                                      =
//WriteData: 2014.08.04                                               =
//EditData : 2014.08.04                                               =
//Diary    : 1、系统延时;                                             =
//=====================================================================
void delay_ms(u16 nCount)
 {//延时0.629us -- 16MHz
    nCount = (nCount*1600);
    while(nCount--);
 }
 
//=====================================================================
//= Public functions------公---共---函---数-------------------------- =
//=====================================================================

//=====================================================================
//Function : Init_NRF24L01( );                                        =
//Author   : zty                                                      =
//WriteData: 2014.07.15                                               =
//EditData : 2014.07.15                                               =
//Diary    : 1、NRF24L01初始化函数                                    =
//=====================================================================
void Init_NRF24L01(void)
{ 
    NRF24L01_CE_DDR;//输出
    NRF24L01_CE_SET;//置'1'
  
    NRF24L01_CSN_DDR;//输出	
    NRF24L01_CSN_SET;//置'1'	
  
    NRF24L01_MOSI_DDR;  //输出
    NRF24L01_MOSI_RESET;//置'0'	
   
    NRF24L01_IRQ_DDR;//输入
    NRF24L01_IRQ_SET;//置'1'
  
    NRF24L01_MISO_DDR;//输入
    NRF24L01_MISO_SET;//置'1'
  
    NRF24L01_SCK_DDR;  //输出	    
    NRF24L01_SCK_RESET;//置'0'
  
    NRF24L01_CE_RESET;//置'0'  
	  SPI_Write_Reg(WRITE_REG + CONFIG, 0x0d);//掉电
		delay_ms(1);
    SPI_Write_Reg(WRITE_REG + EN_AA, 0x01);      //频道0自动ACK应答允许	
    SPI_Write_Reg(WRITE_REG + EN_RXADDR, 0x01);  //允许接收地址只有频道0
		SPI_Write_Reg(WRITE_REG + SETUP_AW, 0x03);   //设置地址宽度为5字节
    SPI_Write_Reg(WRITE_REG + SETUP_RETR, 0x13); //自动重发延时500us + 86us,自动重发3次
    SPI_Write_Reg(WRITE_REG + RF_CH, 0x6f);      //设置信道工作为2.4GHZ，收发必须一致
    SPI_Write_Reg(WRITE_REG + RF_SETUP, 0x07);   //设置发射速率为1MHZ，发射功率为最大值0dB	
		
    SPI_Write_Reg(WRITE_REG + RX_PW_P0, RX_PLOAD_WIDTH);//设置接收数据长度，本次设置为32字节
    SPI_Write_Buf(WRITE_REG + TX_ADDR, TX_ADDRESS, TX_ADR_WIDTH);   //写发送地址
    SPI_Write_Buf(WRITE_REG + RX_ADDR_P0, RX_ADDRESS, RX_ADR_WIDTH);//写接收端地址0(本地)
		
    SPI_Write_Reg(WRITE_REG + CONFIG, 0x0e);   	 //IRQ收发完成中断响应，16位CRC，上电，发送模式
    
    delay_ms(1);
    NRF24L01_CE_SET;//置'1'
    //SwitchToRxMode( );//切换到接收模式 
    //SwitchToTxMode( );//切换到发送模式
    delay_ms(1);
}

//=====================================================================
//Function : SwitchToRxMode( );                                       =
//Author   : zty                                                      =
//WriteData: 2014.07.17                                               =
//EditData : 2014.07.17                                               =
//Diary    : 1、切换到接收模式                                        =
//=====================================================================
void SwitchToRxMode(void)
{
    NRF24L01_CE_RESET;//置'0'
    
    SPI_Write_Reg(WRITE_REG + CONFIG, 0x0f);//上电，16位CRC，接收模式
    SPI_Write_Reg(WRITE_REG+STATUS, 0xff);  //清除所有中断标志
		
    NRF24L01_CE_SET;//置'1'
}

//=====================================================================
//Function : SwitchToTxMode( );                                       =
//Author   : zty                                                      =
//WriteData: 2014.07.17                                               =
//EditData : 2014.07.17                                               =
//Diary    : 1、切换到发送模式                                        =
//=====================================================================
void SwitchToTxMode(void)
{  
    NRF24L01_CE_RESET;//置'0'
		
    SPI_Write_Reg(WRITE_REG + CONFIG, 0x0e);//IRQ收发完成中断响应，16位CRC，上电，发送模式 
    SPI_Write_Reg(WRITE_REG+STATUS, 0xff);  //清除所有中断标志
   
    NRF24L01_CE_SET;//置'1'
}

//=====================================================================
//Function : NRF24L01_SendPacket( );                                  =
//Author   : zty                                                      =
//WriteData: 2014.07.17                                               =
//EditData : 2014.07.17                                               =
//Diary    : 1、发送数据包                                            =
//=====================================================================
unsigned char NRF24L01_SendPacket(unsigned char* buf,unsigned char len)
{
    unsigned char temp;
   
    SPI_Write_Buf(WR_TX_PLOAD, buf, len);//写32字节数据
    do
    {
        temp = SPI_Read_Reg(STATUS);//读状态寄存器

    }while(!(temp & 0x70));//等待数据发送完成(检查接收数据中断、发送完成中断、重发次数溢出中断)

    if(SPI_Read_Reg(STATUS) & STATUS_TX_DS)//检查发送完成中断是否为'1'
    {
        SPI_Write_Reg(WRITE_REG + STATUS, 0xff);//清除所有中断标志
        SPI_Write_Reg(FLUSH_TX, 0x00);          //冲洗TX FIFO寄存器
        return 1;
    }
    else
    {
        SPI_Write_Reg(WRITE_REG + STATUS, 0xff);//清除所有中断标志
        SPI_Write_Reg(FLUSH_TX, 0x00);          //冲洗TX FIFO寄存器
        return 0;
    }    
}

//=====================================================================
//Function : NRF24L01_ReceivePacket( );                               =
//Author   : zty                                                      =
//WriteData: 2014.07.17                                               =
//EditData : 2014.07.17                                               =
//Diary    : 1、接收数据包                                            =
//=====================================================================
unsigned char NRF24L01_ReceivePacket(unsigned char* buf)
{   
    unsigned char temp;
		
    /*NRF24L01_IRQ_SET;
    if(NRF24L01_IRQ_PIN != RESET)//SET = !RESET(0),SET不一定为'1'
    return 0;*/
		
		temp = SPI_Read_Reg(STATUS);//读取状态寄存器
        
    if(temp & 0x40)//判断是否为接收数据中断
    {
        SPI_Read_Buf(RD_RX_PLOAD,buf, TX_PLOAD_WIDTH);//读32字节数据
        SPI_Write_Reg(WRITE_REG + STATUS, 0xff);      //清除所有中断标志
				SPI_Write_Reg(FLUSH_RX, 0x00);                //冲洗RX FIFO寄存器
        return temp;
    }
    else return 0;
    
}// 这个函数最好频繁调用或者放在中断里 

//=====================================================================
//Function : Power_off( );                                            =
//Author   : zty                                                      =
//WriteData: 2014.09.08                                               =
//EditData : 2014.09.08                                               =
//Diary    : 1、进入掉电模式;                                         =
//=====================================================================
void Power_off(void)
{
		NRF24L01_CE_RESET;//置'0'
		
    SPI_Write_Reg(WRITE_REG + CONFIG, 0x0d);//掉电
		
		NRF24L01_CE_SET;//置'1'
		
		delay_ms(1);
}