# 硬件制作说明

### 打样
- 嘉立创PCB，1.0mm厚度即可，太厚也不好看

### 关键BOM
- 幸好段码液晶、12864点阵液晶屏，这些没有因为年代久远，而采购不到(●'◡'●)
- [段码液晶](https://item.taobao.com/item.htm?spm=a1z10.1-c.w4004-4711780199.1.391e7f60mQr01c&id=39869072895)
- [12864点阵](https://item.taobao.com/item.htm?spm=a1z0d.7625083.1998302264.6.5c5f4e692VucTq&id=41560461551) ：注意驱动ST7567引脚14pin
- [直插nRF24L01](https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-21223910208.9.12dc6a4b4C5J0J&id=522572193384)：某宝选择比较多
- [贴片nRF24L01](https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-21223910208.13.12dc6a4b4C5J0J&id=522573574710)：某宝选择比较多

### 焊接
- 发送节点的LED和复位按键就都不要焊接了，用不上
- 接收机的32.768晶振、U1蓝牙模块、U3存储芯片，此部分用不上
- Sensor Nodes Nano24L.csv
- Sensor_NRF24L01.csv
- Sensor_NRF24L01_LCD.csv

![](../5.Docs/2.image/PCB.jpg)
