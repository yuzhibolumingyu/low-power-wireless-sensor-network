# 低能耗无线传感器
![](1.Hardware/Sensor_Nodes_Nano24L/image/03_Sensor_Nodes_Nano24L.PNG)

## 项目介绍
- 这是很久之前的一个项目（许是2015年），因为当时没有什么平台可以完整的展示它（许是自己井底之蛙了），于是搁置至今没有更新过，这次借助gitee和b站来发表，图也都是那时候拍的，留图很重要(●'◡'●)

- 项目描述的场景很简单：1台接收机对应6个发送节点，发送节点是电池供电可超低功耗休眠，定时每2分钟唤醒一次，然后读取温度传感器数值并发送给接收机，接收机收到后在屏幕上显示此节点温度值。

- 发送节点设计了两种外观，一种是无屏的，一种是带4位段码液晶的，电池用的是CR2032纽扣电池，并且两种外壳当时都做了透明不干胶贴，成本还非常低。

## 硬件相关

1. 无屏发送节点方案：Sensor_NRF24L01，MCU采用20脚STM8L101F3P6(*STM8L051F3P6也可*)，无线采用安信可直插nRF24L01模块，温度传感器采用直插DS18B20，安装CR2032纽扣电池，外壳选用巴尔BMD60012(比1元硬币稍大)质感和表面比3D打印效果好很多。我很喜欢无屏这个方案，小巧又好组装，用于别的项目也很nice
![](5.Docs/2.image/Sensor_NRF24L01.PNG)

2. 有屏发送节点方案：Sensor_NRF24L01_LCD，MCU采用32脚STM8L152K4T6(*STM8L152K6T6也可*)，无线采用安信可贴片nRF24L01模块，温度传感器采用直插DS18B20，安装CR2032纽扣电池，外壳本来打算选用巴尔BMD60025和BMW50024，但是开屏幕孔实在费劲(没有铣床用手抠效果一般)，后来就画了一个外壳3D打印来用。
![](/5.Docs/2.image/Sensor_NRF24L01_LCD.png)

3. 接收机硬件方案：Sensor_Nodes_Nano24L，MCU采用32脚STM8L151K4T6(*STM8L151K6T6也可*)，无线采用安信可直插带PA方大nRF24L01模块，AAAx2电池或自己找个USB转3.3V小板供电，外壳选用巴尔BMW50028，没来得及画一个3D打印，屏幕开孔是手抠的(因为当时想做一个就行)
![](/5.Docs/2.image/Sensor_Node_NRF24L01.png)

4. 组装完成效果：
![](/5.Docs/2.image/Sensor_NRF24L01-WC.png)
![](/5.Docs/2.image/Sensor_NRF24L01_LCD-WC.png)
![](/5.Docs/2.image/Sensor_1-5.jpg)

## 软件相关


1. 本项目的无线通信部分，实际上就是实现nRF24L01芯片可一对六接收的效果，即接收端设置六路接收通道地址，分别对应六个发送端地址，就能同时接收六路数据了。
`
(通信原理：一方接收地址和一方发射地址设置相同，双方再分别切换成接收模式和发送模式，才能收发通信。)
`
2. 所以6个发送节点的nRF24L01，工作时要处于发送模式，每个节点都设置自己单独的地址；而接收机的nRF24L01则一直处于接收模式，其6个接收地址与6个发送节点的发射地址相同。
`
(注意这里的6个发送节点是使用同一份程序[ \2.Firmware\Sensor_NRF24L01 ]，所以在给6个发送节点的板子烧写固件之前，要把地址按下面改好，再去编译下载。这样做其实是为了简单，后面考虑直接写在eeprom中。)
`

3. 地址设置：

```c
    //接收机地址设置：文件路径*[ \2.Firmware\Sensor_Nodes_Nano24L\STVD\src\nrf24l01.c ]*，发送地址用不到，所以不用改
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0x34,0x43,0x10,0x10,0x01};//发送地址
    unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0x34,0x43,0x10,0x10,0x01};//通道0接收地址
    unsigned char RX_ADDRESS1[RX_ADR_WIDTH]= {0xB0,0x43,0x10,0x10,0x01};//通道1接收地址
    unsigned char RX_ADDRESS2[1]= {0xB1};//通道2接收地址，实际地址{0xB1,0x43,0x10,0x10,0x01}
    unsigned char RX_ADDRESS3[1]= {0xB2};//通道3接收地址，实际地址{0xB2,0x43,0x10,0x10,0x01}
    unsigned char RX_ADDRESS4[1]= {0xB3};//通道4接收地址，实际地址{0xB3,0x43,0x10,0x10,0x01}
    unsigned char RX_ADDRESS5[1]= {0xB4};//通道5接收地址，实际地址{0xB4,0x43,0x10,0x10,0x01}

	//发送节点地址设置：文件路径*[ \2.Firmware\Sensor_NRF24L01\STVD\src\nrf24l01.c ]*
	//发送节点1，通道0是芯片Enhanced ShockBurst TM模式需要，不用计较
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0x34,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0x34,0x43,0x10,0x10,0x01};//通道0接收地址
	//发送节点2
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB0,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB0,0x43,0x10,0x10,0x01};//通道0接收地址
	//发送节点3
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB1,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB1,0x43,0x10,0x10,0x01};//通道0接收地址
	//发送节点4
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB2,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB2,0x43,0x10,0x10,0x01};//通道0接收地址
	//发送节点5
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB3,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB3,0x43,0x10,0x10,0x01};//通道0接收地址
	//发送节点6
    unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xB4,0x43,0x10,0x10,0x01};//发送地址
	unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xB4,0x43,0x10,0x10,0x01};//通道0接收地址
```

4. 固件编译下载工具：打开工程编译下载即可。
`
(采用ST官方推荐的STVD+Cosmic组合，安装好两个软件后，打开工程编译下载即可。开发环境的详细安装配置网上很多教程，这里不再赘述。)
`
![](/3.Software/STVD+Cosmic.png)

## 低功耗说明

1. 发送节点在发送完成后，就进入低功耗，此时STM8进入活跃停机模式，数据手册显示其功耗大约0.8uA；nRF24L01进入掉电模式，数据手册显示其功耗900nA；DS18B20也进入待机模式，数据手册显示其功耗<3uA；STM8驱动段码液晶也会稍微增加一些功耗。
`
(当时某宝买的CR2032电池几毛钱一片，发送节点就分布在屋子的各个角落，后来接收机上不显示了，就说明没电了。于是根据开始日期计算了一下时间，带段码液晶的大概3个月就没电了，无屏的有4个月没电的，有6个月才没电的。)
`
2. 发送节点功耗示意图：
![](/5.Docs/2.image/PW_ew.png)

3. 发送节点功耗实际测试效果：
![](/5.Docs/2.image/Sensor_NRF24L01_LCD_PW.png)
![](/5.Docs/2.image/Sensor_NRF24L01_PW.png)

## 关键BOM链接
* [段码液晶](https://item.taobao.com/item.htm?spm=a1z10.1-c.w4004-4711780199.1.391e7f60mQr01c&id=39869072895)
* [12864点阵](https://item.taobao.com/item.htm?spm=a1z0d.7625083.1998302264.6.5c5f4e692VucTq&id=41560461551)
* [直插nRF24L01](https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-21223910208.9.12dc6a4b4C5J0J&id=522572193384)
* [贴片nRF24L01](https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-21223910208.13.12dc6a4b4C5J0J&id=522573574710)

## 早期迭代靓图(●'◡'●)
![](/5.Docs/2.image/03Wireless.jpg)
![](/5.Docs/2.image/01Wireless_Control_mega128.jpg)

